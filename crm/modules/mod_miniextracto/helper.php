<?php
//error_reporting(E_ALL);
/**
* @version    $Id: helper.php 14401 2010-01-26 14:10:00Z louis $
* @package    Inc Socios
* @copyright  Copyright (C) 2005 - 2010 Open Source Matters. All rights reserved.
* @license    GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// no direct access
defined('_JEXEC') or die('Restricted access'); 

class modMiniExtractoDocumentosHelper
{
  
  
    function getUltimomesliquidado($user)
        {
          $dbo = JFactory::getDBO();

          $sql = "SELECT per.nombre AS ultPeriodoLiq
                  FROM #__periodos AS per
                  WHERE per.id = (SELECT MAX( periodo )
                                      FROM #__liquidaciones
                                      WHERE participante = ".$user.")
                                          ";
          $dbo->setQuery($sql); 
          $result = $dbo->loadResult();
          if (!$result){
              $result = "No Disponible";
          }
          return $result;
        }
        
         function getSaldoAnterior($user)
        {
          $dbo = JFactory::getDBO();
          $sql = "SELECT IFNULL(SUM(puntos),0)
                    FROM #__liquidaciones
                    WHERE participante = ".$user."
                    AND periodo < (SELECT MAX( periodo )
                                      FROM #__cuotas
                                      WHERE participante = ".$user.") ";
          $dbo->setQuery($sql);
          $result = $dbo->loadResult();
          return $result;
        }
       function getPuntosMes($user)
        {
          $dbo = JFactory::getDBO();
         // $fechaSis = date('Y-m-01');
                  
          $sql = "SELECT IFNULL(SUM(puntos),0)
                    FROM #__liquidaciones
                    WHERE participante = ".$user."
                    AND periodo = (SELECT MAX( periodo )
                                      FROM #__cuotas
                                      WHERE participante = ".$user.") ";
         // echo $sql; 
          
          $dbo->setQuery($sql);
          $result = $dbo->loadResult();
          return $result;
        }
        
        function getRedimidos($user)
        {
          $dbo = JFactory::getDBO();
          $sql = "SELECT IFNULL(SUM(product_subtotal_with_tax),0) as redimidos
                    FROM  jfb_virtuemart_order_items 
                    WHERE created_by = ".$user." AND order_status != 'X'";
          $dbo->setQuery($sql);
          $result = $dbo->loadResult();
          return $result;
        }
        
          function getSaldoaFecha($user) 
        {
          $dbo = JFactory::getDBO(); 
          $sql = "SELECT SUM(puntos)
                    FROM #__liquidaciones
                    WHERE participante = ".$user." ";  
          
          $dbo->setQuery($sql);
          $totalPuntos = $dbo->loadResult();
          
          $sql = "SELECT IFNULL(SUM(product_subtotal_with_tax),0) as redimidos
                    FROM  jfb_virtuemart_order_items 
                    WHERE created_by = ".$user." AND order_status != 'X'";
          
          $dbo->setQuery($sql);
          $redimidos = $dbo->loadResult();
          $saldo = $totalPuntos - $redimidos;
          return $saldo;
        }
        function acumulados($user){
            
          $dbo = JFactory::getDBO(); 
          $sql = "SELECT SUM(puntos)
                    FROM #__liquidaciones
                    WHERE participante = ".$user." ";  
          
          $dbo->setQuery($sql);
          $totalPuntos = $dbo->loadResult();
          
          return $totalPuntos;
        }
}
