<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>CONSULTA HOJA DE VIDA</title>
        <link rel="stylesheet" href="templates/unidos/css/aganar.css" type="text/css" />
        <link rel="stylesheet" href="templates/unidos/css/template.css" type="text/css" />
    </head>       
    <body>
  <center><h2>HOJA DE VIDA CLIENTE</h2></center>
      <div id="datosbasic" >
        <br>
        <table class="tabla" border ="0">
            <tr>
                <td>Tipo identificación </td>
                <td><input readonly type="text" id="tpid" name="tpid" value="<?php echo $this->items[0]->tp_id; ?>"/></td>
                <td>Número identificación </td>
                <td><input readonly type="text" id="numidclien" name="numidcliente" value="<?php echo $this->items[0]->documento; ?>"/></td>
            </tr>  
            <tr>
                <td >Nombre cliente </td>
                <td colspan="3">
                    <input readonly type="text" id="nombreclien"  name="nombreclien" title="Ingrese el nombre del cliente" value="<?php echo $this->items[0]->nombre; ?>"/>
                </td>

            </tr>   
            <tr>
                <td>Gerente FB: </td>
                <td><input readonly type="text" id="comercial" name="comercial" value="<?php echo $this->items[0]->gerenFB; ?>" /></td>
                <td>Segmento: </td>
                <td>
                    <input readonly type="text" id="segclien" name="segclien" value="<?php echo $this->items[0]->segmento; ?>">
                </td>
            </tr>   
            <tr>
                <td>Gerente BB: </td>
                <td><input readonly type="text" id="gerenteBB" name="gerenteBB" value="<?php echo $this->items[0]->gerenBB; ?>" /></td>
                <td>Sector: </td>
                <td>
                    <input readonly type="text" id="sectorclien" name="sectorclien" value="<?php echo strtoupper($this->items[0]->sector); ?>"/>
                </td>
            </tr>   
            <tr>
                <td>Fecha Visita: </td> 
                <td><input readonly type="text" id="fechavisita" name="fechavisita" value="<?php echo $this->items[0]->fecha_ult_vis; ?>" /></td>
                <td>Regional: </td>
                <td><input readonly type="text" id="regional" name="regional" value="<?php echo $this->items[0]->regional; ?>"/> 

                </td>
            </tr>   
            <tr>
                <td>Dirección: </td>
                <td><input readonly type="text" id="direcclien" name="direcclien"  value="<?php echo $this->items[0]->direccion; ?>" title="Digite la direccion del cliente Carrera Cra, Calle Cll. Ejemplo Cra 55 No. 35-28 "/></td>
                <td>Tipo de negocio: </td>
                <td>
                    <input readonly type="text" id="tpnegocio" name="tpnegocio" value="<?php echo $this->items[0]->tpnego; ?>" />
                </td>
            </tr>  
            <td>Asistio</td>
            <td>
                <input readonly type="text" id="tpnegocio" name="tpnegocio" value="<?php echo $this->items[0]->asistio; ?>" /> 
            </td>
            <tr> 
          
        </table>
               <br/>
    </div>
        
<h4>PRODUCTOS ACTUALES DEL CLIENTE</h4>
    <div id="financieros" >
        <br>
        <div id="financieros">
            <table class="tabla">
                <tr>
                    <td colspan="4"><h5>Productos Bancarios</h5></td>
                </tr>
                <?php
                $banco = $this->bancos;
                foreach ($banco as $value) {
                    ?>
                    <tr>
                        <td>Banco :</td>
                        <td><input readonly type="text"  value="<?php echo $value->banco; ?>" /> </td>
                        <td>Producto :</td>
                        <td>
                            <input readonly type="text"  value="<?php echo $value->producto; ?>" /> 
                        </td>

                    </tr> 
                <?php } ?>
                <tr>
                   
                </tr>
            </table>
            <br/>
        </div>
        <div id="financieros">
            <table class="tabla">
                <tr>
                    <td colspan="4"><h5>Productos Fiduciarias</h5></td>
                </tr>
                <?php
                $fidu = $this->fidu;
                foreach ($fidu as $value) {
                    ?>
                    <tr>
                        <td>Fiduciaria :</td>
                        <td><input readonly type="text"  value="<?php echo $value->fidu; ?>" /> </td>
                        <td>Producto :</td>
                        <td>
                            <input readonly type="text"  value="<?php echo $value->producto; ?>" /> 
                        </td>

                    </tr> 
                <?php } ?>
                
            </table>
            <br/>
        </div>
        <div id="financieros">
            <table class="tabla">
                <tr>
                    <td colspan="4">
                        <h5>Productos Comisionistas</h5></td>
                </tr>
                <?php
                $comi = $this->comi;
                foreach ($comi as $value) {
                    ?>
                    <tr>
                        <td>Comisionista :</td>
                        <td><input readonly type="text"  value="<?php echo $value->comi; ?>" /> </td>
                        <td>Producto :</td>
                        <td>
                            <input readonly type="text"  value="<?php echo $value->producto; ?>" />
                        </td>

                    </tr> 
                <?php } ?>
            </table>
            <br/>
        </div>
        <div id="financieros">
            <br/>
                <?php $Infoadicional = $this->Infoadicional[0];
                // print_r($Infoadicional);
                ?>
            <table class="tabla" >
                <tr>
                    <td style="width: 70px ">Otros Financieros:</td>
                </tr>
                <tr>
                    <td>
                        <textarea readonly rows="2" cols="50" ><?php echo $Infoadicional->otros_Finan; ?></textarea>
                    </td>
                </tr>    
                
            </table>
            <br/>
        </div>
        <div id="financieros">
            <br/>
            <table class="tabla" >
                <tr>
                    <td style="width: 70px ">Proyectos actuales: </td>
                </tr>
                <tr>
                    <td>
                        <textarea readonly rows="2" cols="50" ><?php echo $Infoadicional->proyec_actuales; ?></textarea>
                    </td>
               
            </table>
        <br/>
        </div>
       
    </div>
 <h4>INFORMACIÓN DE CONTACTOS</h4>
    <div id="contactos" >
        <table class="tabla">
            <tr>
                <td colspan="4"><h5>Contactos</h5></td>
            </tr>

            <?php
            $contactos = $this->contactos;
            foreach ($contactos as $value) {
                ?>
                <tr>
                    <td>Nombre:</td>
                    <td><input readonly type="text" id="nombrecont" name="nombrecont" value="<?php echo $value->nombre; ?>" /></td>
                    <td>Producto :</td>
                    <td>Cargo:</td>
                    <td><input readonly type="text" id="cargocont" name="cargocont" value="<?php echo $value->cargo; ?>" /></td>
                   
                </tr> 
<?php } ?>
            <tr>
            </tr> 
                </table>
         <br/>
    </div>
    <h4>SEGUIMIENTO DE VISITAS</h4> 
    <div id="datosbasic" >
        <br/>
        <table class="tabla" >
            <?php
            $seguimientos = $this->seguimientos;
            foreach ($seguimientos as $value) {
                ?>
                <tr>
                    <td style="width: 30px ">fecha:</td>
                    <td style="width: 70px "><input readonly type="text"  value="<?php echo $value->fecha; ?>" /></td>
                    <td style="width: 70px ">Seguimiento:</td>
                    <td>
                        <textarea readonly rows="2" cols="50" ><?php echo $value->segui; ?></textarea>
                    </td>
                </tr>    
<?php } ?>
          
        </table>
         <br/>
    </div>
    <h4>NOTICIAS</h4>
    <div id="datosbasic" >
        <br/>
        <table class="tabla">
            <?php
            $noticias = $this->noticias;
            foreach ($noticias as $value) {
                ?>
                <tr>
                    <td style="width: 30px ">fecha:</td>
                    <td style="width: 70px "><input readonly type="text"  value="<?php echo $value->fecha; ?>" /></td>
                    <td style="width: 70px ">Seguimiento:</td>
                    <td>
                        <textarea readonly rows="2" cols="50" ><?php echo $value->noti; ?></textarea>
                    </td>
                </tr>    
<?php } ?>


        </table>
         <br/>
    </div>

    <h4>SEGUIMIENTO DEL CLIENTE</h4>
    <div id="situacionactual" >
        <h4 style="text-align: center">Situación Actual </h4>

        <div id="st_act_interno">
<?php $situacion = $this->situacionact[0]; ?>  
            <br />
            <table class="tablaint" >
                <tr>
                    <td>Fecha última visita:</td>
                    <td><input readonly type="text"  value="<?php echo $situacion->ult_visita; ?>" /></td>
                    <td>Resultado Visita:</td>
                    <td><input readonly type="text"  value="<?php echo $situacion->result_visita; ?>" /></td>
                </tr>
                <tr>
                    <td>Compromiso:</td>
                    <td><input readonly type="text" value="<?php echo $situacion->compromiso; ?>" /></td>
                    <td>Producto:</td>
                    <td><input readonly type="text"  value="<?php echo $situacion->producto; ?>" /></td>
                </tr>
                <tr>
                    <td>Antiguedad:</td>
                    <td><input readonly type="text"  value="<?php echo $situacion->antiguedad; ?>" /></td>
                    <td>Saldo promedio mes:</td>
                    <td><input readonly type="text"  value="<?php echo number_format($situacion->saldo_prom_mes); ?>" /></td>
                </tr>
                <tr>
                    <td>Aportes promedio mes(# operaciones):</td>
                    <td><input readonly type="text"  value="<?php echo number_format($situacion->aport_prom_mes); ?>" /></td>
                    <td>Número de Pagos Prom. Mes:</td>
                    <td><input readonly type="text"  value="<?php echo number_format($situacion->n_pagosprom_mes); ?>" /></td>
                </tr>
                <tr>
                    <td>Fiducia Admón; tipo de negocio:</td>
                    <td><input readonly type="text"  value="<?php echo $situacion->fid_adm_tpneg; ?>" /></td>
                    <td>Recursos Administrados:</td>
                    <td><input readonly type="text"  value="<?php echo number_format($situacion->recursos_adm); ?>" /></td>
                </tr>
                <tr>
                    <td>Director Asignado/Vic de Gestion:</td>
                    <td><input readonly type="text"  value="<?php echo $situacion->dir_asig_vic_gest; ?>" /></td>
                    <td>Ejecutivo/Vic de Gestion:</td>
                    <td><input readonly type="text"  value="<?php echo $situacion->ejec_vic_gest; ?>" /></td>
                </tr> 
                <tr>
                    <td >Vencimiento del contrato:</td>
                    <td colspan="2"><input readonly type="text"  value="<?php echo $situacion->vence_contrato; ?>" /></td>

                </tr>
                            </table>
 <br/>
        </div>
        <h4 style="text-align: center">Información Financiera </h4>
        <?php
        $infofinan = $this->infofinan[0];
        //echo "<pre>".print_r($this,1);
        ?>
        <div id="st_act_interno">
            <br />
            <table class="tablaint">
                <tr>
                    <td>Ventas anuales ($MM):</td>
                    <td><input readonly title="Esta sección no es editable" type="text"  value="<?php echo number_format($infofinan->ventas_anuales); ?>" /></td>
                    <td>Ingresos mensuales ($MM):</td>
                    <td><input readonly title="Esta sección no es editable" type="text" value="<?php echo number_format($infofinan->ingresos_men); ?>" /></td>
                </tr>
                <tr>
                    <td>Gastos mensuales ($MM):</td>
                    <td><input readonly  title="Esta sección no es editable" type="text"  value="<?php echo number_format($infofinan->gastos_men); ?>" /></td>
                    <td>Activos Totales ($MM):</td>
                    <td><input readonly  title="Esta sección no es editable" type="text"  value="<?php echo number_format($infofinan->activos_tot); ?>" /></td>
                </tr>
                <tr>
                    <td>Pasivos Totales ($MM):</td>
                    <td><input readonly title="Esta sección no es editable" type="text"  value="<?php echo number_format($infofinan->pasivos_tot); ?>" /></td>
                    <td>Sucursales:</td>
                    <td><input readonly title="Esta sección no es editable" type="text" value="<?php echo $infofinan->sucursales; ?>" /></td>
                </tr>
                <tr>
                    <td>Número Empleados:</td>
                    <td><input readonly title="Esta sección no es editable" type="text"  value="<?php echo number_format($infofinan->empleados); ?>" /></td>
                    <td>Operación en el Exterior:</td>
                    <td><input readonly  title="Esta sección no es editable" type="text"  value="<?php echo $infofinan->oper_exterior; ?>" /></td>
                </tr>
                <tr>
                    <td>Portafolio de Inversiones:</td>
                    <td><input readonly title="Esta sección no es editable" type="text"  value="<?php echo $infofinan->portafolio_inver; ?>" /></td>

                </tr>
                
            </table>
 <br/>
        </div>
        <br />
    </div>
    <div id="infoadicional">
        <br/>
        <table class="tabla" >
            <tr>
                <td style="width: 70px ">Generación nuevos negocios: </td>
            </tr>  <tr> 
                <td>
                    <textarea readonly rows="2" cols="50" ><?php echo $Infoadicional->genera_new_nego; ?></textarea>
                </td>
            </tr>    
            
        </table>
         <br/>
    </div>
    <div id="infoadicional">
        <br/>
        <table class="tabla" >
            <tr>
                <td style="width: 70px ">Negocios en curso: </td>
            </tr>  <tr> 
                <td>
                    <textarea readonly rows="2" cols="50" ><?php echo $Infoadicional->Nego_en_curso; ?></textarea>
                </td>
            </tr>    
           
        </table>
         <br/>
    </div>
    <div id="infoadicional">
        <br/>
        <table class="tabla" >
            <tr>
                <td style="width: 70px ">Propósito visita: </td>
            </tr> 
            <tr>   
                <td>
                    <textarea readonly rows="2" cols="50" ><?php echo $Infoadicional->Propo_visita; ?></textarea>
                </td>
            </tr>    
           
        </table>
         <br/>
    </div>
    <div id="infoadicional">
        <br/>
        <table class="tabla" >
            <tr>
                <td style="width: 70px ">Beneficio: El Beneficio de esta reunion sera: </td>
            </tr> 
            <tr>
                <td>
                    <textarea readonly rows="2" cols="50" ><?php echo $Infoadicional->Beneficio_visi; ?></textarea>
                </td>
            </tr>    
           
        </table>
         <br/>
    </div>
        
    </body>
</html>
