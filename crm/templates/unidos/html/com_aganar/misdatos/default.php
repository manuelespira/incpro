<?php
defined('_JEXEC') or die('Restricted access');
$sesion = JFactory::getSession();
$db = JFactory::getDbo();
$link = $_SERVER['REQUEST_URI'];
$ciudad = $sesion->get('ciudad'); 
//echo "<pre>".print_r(JFactory::getUser(),1); 
//echo "<pre>".print_r($this->items,1);
?>
<html>
    <head>
        <script type="text/javascript" src="templates/unidos/html/fancybox/custom/jquery-1.7.2.min.js"></script>
        <link rel="stylesheet" href="/templates/unidos/html/script/thickbox.css" type="text/css" />
        <link rel="stylesheet" href="/templates/unidos/css/aganar.css" type="text/css" />
        <script type="text/javascript" src="/templates/unidos/html/script/thickbox.js"></script>
        <title>.::Mis Datos::.</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!--        <style type="text/css">
        td {
        padding-bottom: 10PX;
                }
        </style>-->
        <?php
        ob_start();
        ?>
        <script type="text/javascript">
            $(function() {
                $("#departamento").change(function(event) {
                    var id = $("#departamento").find(':selected').val();
                    $("#ciudad").load('filtros.php?id=' + id + '&task=ciudad');
                });
            });

            function valida(f) {
               
                var errores = "";
                var opciones2;
                var seleccionado2 = false;
                e = f.correo.value;
                if ((e.indexOf(".") > 2) && (e.indexOf("@") > 0) && (e.indexOf(".") < (e.length - 2)))
                    errores = errores; //correcto
                else
                    errores += 'Debe introducir un email v\u00E1lido\r\n';
                if (!/^[0-9]{10}$/.test(f.celular.value))
                    errores += "El número de celular debe tener 10 dígitos.\r\n";
                if (!/^[0-9]{5,7}$/.test(f.telefono.value))
                    errores += "El número de teléfono debe tener entre 5 y 7 dígitos.\r\n";
                if (f.departamento.value == 0 || f.ciudad.value == 0)
                    errores += 'Debe indicar el departamento y la ciudad de ubicaci\u00f3n\r\n';
                if (f.direccion.value.length < 5)
                    errores += 'Debe introducir una dirección\r\n';
                if (f.barrio.value == '')
                    errores += 'Debe especificar un barrio\r\n';
                if (isNaN(f.telefono.value)) 
                     errores += "el campo telefono debe tener sólo números\r\n";
                if(f.pass2.value!=''|| f.pass.value!='' )
                     if (!/^.*(?=.{8,20})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[|@#$%&\^\+\*\_\:\;\,\.\-\=\/\!]).*$/.test(f.pass.value))
                       errores+='El password no es muy fuerte por favor verifiquelo';
                else if(f.pass2.value!=f.pass.value) errores+='La confirmaci\u00F3n y el password deben ser iguales\r\n';
                 
                opciones2 = document.getElementsByName("sms");
                 for (var i = 0; i < opciones2.length; i++) {
                          if (opciones2[i].checked) {
                              seleccionado2 = true;
                              break;
                          }
                 }

                 if (!seleccionado2) {
                      errores+='Autoriza el envío de mensajes de texto al celular?\r\n';
                 }
                  opciones2 = document.getElementsByName("mail");
                  seleccionado2 = false;
                  for (var i = 0; i < opciones2.length; i++) {
                          if (opciones2[i].checked) {
                              seleccionado2 = true;
                              break;
                          }
                 }
                 if (!seleccionado2) {
                      errores+='Autoriza el envío de mensajes de correo?\r\n';
                 } 
                 
                if (errores == "")
                    f.submit();
                else
                    alert(errores);
            }
        </script>
        <?php
        $js = ob_get_contents();
        ob_clean();
        ?>
    </head>
    <body bgcolor="#FFFFFF"  marginwidth="0" marginheight="0">
        <?php
        $mainframe = JFactory::getApplication();
        $mainframe->triggerEvent('onJsLoad', array(JURI::root() . '/templates/unidos/html/script/jquery-1.3.2.min.js', null));
        $mainframe->triggerEvent('onJsLoad', array(null, '<script type="text/javascript" >jQuery.noConflict();</script>'));
        $mainframe->triggerEvent('onJsLoad', array(JURI::root() . '/templates/unidos/html/script/jquery.min.js', null));
        $mainframe->triggerEvent('onJsLoad', array(JURI::root() . '/templates/unidos/html/script/thickbox.js', null));
        $mainframe->triggerEvent('onJsLoad', array(null, $js));
        //print_r($_POST);
        ?>    
        <!-- Save for Web Slices (mis_datos.psd) -->
        <form action="index.php" method="post" name="Form2">
            <input type="hidden" name="option" value="com_aganar" />
            <input type="hidden" name="task" value="reiniciapass" />
        </form>
        <form action="index.php" method="post" name="Form">
            <table id="Tabla_01" border="0" cellpadding="0" cellspacing="0" style="margin: 0 auto;">
               <tr><!-- nombres-->
                    <td><img src="images/imagesmisdatos/mis_datos_01.jpg" ></td>
                    <td width="447" height="31"><input class="noeditable" disabled=""  type="text" size="50" value="<?php echo $this->items[0]->nombres . " " . $this->items[0]->apellidos; ?>" ></td>
                </tr>
                <tr> <!-- cedula-->
                    <td><img src="images/imagesmisdatos/mis_datos_06.jpg" ></td>
                    <td width="447" height="32"><input class="noeditable" disabled="" type="text" size="50"  value="<?php echo $this->items[0]->cedula; ?>"></td>
                </tr>
                <tr> <!--correo-->
                    <td><img src="images/imagesmisdatos/mis_datos_10.jpg" ></td>
                    <td width="447" height="32"><input name="correo" type="text" size="50"  value="<?php echo $this->items[0]->correo; ?>"></td>
                </tr>
                <tr> <!--celular-->
                    <td><img src="images/imagesmisdatos/mis_datos_14.jpg" ></td>
                    <td width="447" height="31"><input name="celular" type="text" size="50" maxlength="10" value="<?php echo $this->items[0]->celular; ?>"></td>
                </tr>
                <tr><!--telefono-->
                    <td><img src="images/imagesmisdatos/mis_datos_18.jpg"></td>
                    <td width="447" height="31"><input name="telefono" type="text" size="50" maxlength="7" value="<?php echo $this->items[0]->telefono; ?>"></td>
                </tr>
                <tr><!--direccion-->
                    <td><img src="images/imagesmisdatos/mis_datos_22.jpg" ></td>
                    <td width="447" height="31"><input name="direccion" type="text" size="50" value="<?php echo $this->items[0]->direccion; ?>"  ></td>
                </tr>
                <tr> <!--barrio-->
                    <td><img src="images/imagesmisdatos/mis_datos_26.jpg" ></td>
                    <td width="447" height="31"><input name="barrio" type="text" size="50"  value="<?php echo $this->items[0]->barrio; ?>" ></td>
                </tr>
                <tr> 
                    <td><img src="images/imagesmisdatos/mis_datos_30.jpg" ></td>
                    <td width="447" height="31"><input class="noeditable" disabled="" type="text" size="50" maxlength="15" value="<?php echo ($this->items[0]->genero == 1) ? "Maculino" : "Femenino"; ?>"></td>
                </tr>
                <tr><!--departamento-->
                    <td>
                        <img src="images/imagesmisdatos/depto.jpg" width="162" height="31" style="margin-left: 13px">
                    </td>
                    <td width="447" height="31">
                        <select name="departamento" id="departamento" >
                            <?php echo $this->depts; ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><img src="images/imagesmisdatos/mis_datos_34.jpg" ></td>
                    <td width="447" height="31">
                        <?php
                        if ($ciudad != 0) {
                            ?>        
                            <select name="ciudad" id="ciudad">
                                <option value ="0">Seleccione...</option>
                                <?php echo $this->ciudad; ?>
                            </select>
                            <?php
                        } else {
                            ?>
                            <select name="ciudad" id="ciudad">
                                <option value ="0">Seleccione...</option>
                            </select>
                        <?php } ?>

                    </td>
                </tr>
                <tr>
                    <td><img src="images/imagesmisdatos/mis_datos_38.jpg" ></td>
                    <td width="447" height="31"><input class="noeditable" disabled="" type="text" size="50" maxlength="15" value="<?php echo ($this->items[0]->estuser == 0) ? "Activo" : "Inactivo"; ?>"></td>
                </tr>
                <tr>
                    <td><img src="images/imagesmisdatos/mis_datos_42.jpg" ></td>
                    <td width="447" height="32"><input class="noeditable" disabled="" type="text" size="50" maxlength="15" value="<?php echo $this->items[0]->registro; ?>"></td>
                </tr>
                <tr> 
                    <td><img src="images/imagesmisdatos/mis_datos_46.jpg" ></td>
                    <td width="447" height="31"><input class="noeditable" disabled="" type="text" size="50" maxlength="15" value="<?php echo $this->items[0]->retiro; ?>"></td>
                </tr>
  <?php if (!$sesion->get('idpart')){?>
                <tr>
                    <td><img src="images/imagesmisdatos/mis_datos_50.jpg" ></td>
                    <td>
                        <a href="#" class="Ntooltip">
                         <input  type="password" size="50" maxlength="20" name="pass" id="pass" onload="this.value=''"><span>Su password debe contener mínimo 8 caracteres, una letra mayúscula, una letra minúscula, al menos un número, y un caracter especial.</span></a>
                    </td>
                    </tr>
                    <tr>
                        <td><img src="images/imagesmisdatos/mis_datos_54.jpg" height="35px" width="186px" ></td>
                        <td><a href="#" class="Ntooltip">
                                <input   type="password" size="50" maxlength="20" name="pass2" id="pass2" value=""  >
                                <span>Su password debe contener mínimo 8 caracteres, una letra mayúscula, una letra minúscula, al menos un número, y un caracter especial.</span></a>
                                </td>
                </tr>
 <?php }else{?>
                <tr>
                  <td colspan="3" style="text-align: center;">
                        <a class="button art-button" href="javascript:document.Form2.submit();" >Restaurar Contraseña</a>
                          <input type="hidden" name="pass" value="" />
                          <input type="hidden" name="pass2" value="" />
                  </td>
                  </tr>
 <?php $sesion->set('callEditdatos',3); 
 }?>
                  <tr>  
                      <td colspan="2">
                          <table>
                              <tr>
                                  <td>
                                      <br/>
                                      ¿Autoriza el envío de mensajes a su correo electrónico? 
                                  </td>
                                  <td>
                                      SI<input type="radio"   name="mail" id="mail" value="1" <?php echo $this->items[0]->envio_email == '1' ? 'checked="true"' : ''; ?> />
                                      &nbsp;&nbsp;NO<input type="radio" name="mail" id="mail"  value="0" <?php echo $this->items[0]->envio_email == '0' ? 'checked="true"' : ''; ?>/>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      <br/>
                                      ¿Autoriza el envío de mensajes de texto al celular? 
                                  </td>
                                  <td>     
                                      SI<input type="radio"  name="sms" id="sms"  value="1" <?php echo $this->items[0]->envio_sms == '1' ? 'checked="true"' : ''; ?>/>
                                      &nbsp;&nbsp;NO<input type="radio" name="sms" id="sms" value="0" <?php echo $this->items[0]->envio_sms == '0' ? 'checked="true"' : ''; ?>/>
                                  </td>
                              </tr>
                          </table>
                      </td>  
                    </tr>  
                <tr>
                    <td style="text-align: center" colspan="2">
                        <br />
                        <input type="hidden" name="option" value="com_aganar" />
                        <input type="hidden" name="view" value="misdatos" />
                        <input type="hidden" name="task" value="guardar" />
                        <input class="art-button" type="button" value="Guardar" onclick="javascript:valida(document.forms['Form'])">
                    </td>
                </tr>
            </table>
        </form>       
        <br/> 
        <div style="text-align: center;width: 700px;height: 100px"  >NOTA:
Si tus datos no nos correctos y el campo no esditable por favor utiliza la opción contátanos e informanos tu novedad.                
</div>

        <!-- End Save for Web Slices -->
    </body>
</html> 