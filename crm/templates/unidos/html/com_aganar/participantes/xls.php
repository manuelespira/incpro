<?php
$session = JFactory::getSession();
ob_end_clean();
?> 
<h2>Participantes</h2>
 <table class="tablaliq" border="1" cellspacing="3" cellpadding="2" >
                    <tr> 
                        <th style="text-transform: uppercase;text-align: center;background-color: #000066;color: white;">Participante</th>
                        <th style="text-transform: uppercase;text-align: center;background-color: #000066;color: white;">Cédula</th>
                        <th style="text-transform: uppercase;text-align: center;background-color: #000066;color: white;"><? echo JRequest::getVar('accion')?></th>
                        <th style="text-transform: uppercase;text-align: center;background-color: #000066;color: white;">cargo</th>
                        <th style="text-transform: uppercase;text-align: center;background-color: #000066;color: white;">Correo</th>
                        <th style="text-transform: uppercase;text-align: center;background-color: #000066;color: white;">Teléfono</th>
                        <th style="text-transform: uppercase;text-align: center;background-color: #000066;color: white;">Celular</th>
                        <th style="text-transform: uppercase;text-align: center;background-color: #000066;color: white;">Barrio</th>
                        <th style="text-transform: uppercase;text-align: center;background-color: #000066;color: white;">Ciudad</th>
                        <th style="text-transform: uppercase;text-align: center;background-color: #000066;color: white;">Departamento</th>
                        <th style="text-transform: uppercase;text-align: center;background-color: #000066;color: white;">Estado</th>
                        <th style="text-transform: uppercase;text-align: center;background-color: #000066;color: white;">Envio SMS</th>
                        <th style="text-transform: uppercase;text-align: center;background-color: #000066;color: white;">Envio E-mail</th>
                        <th style="text-transform: uppercase;text-align: center;background-color: #000066;color: white;">Autorizo</th>
                    </tr>
                    <?php
                    $datos = $this->data;
                    if ($datos) {
                        $i = 0;
                        foreach ($datos as $value) {
                            ?>
                            <tr style="height: 25px;" <?= (($i % 2) != 0 ? '' : 'style="background-color: #DDDDDD;"'); ?>>
                                <td><?php echo utf8_decode($value->participante); ?></td>
                                <td><?php echo $value->cedula; ?></td>
                                <td><?php echo $value->campo; ?></td>
                                <td><?php echo $value->cargo; ?></td>
                                <td><?php echo $value->correo; ?></td>
                                <td><?php echo $value->telefono; ?></td>
                                <td><?php echo $value->celular; ?></td>
                                <td><?php echo $value->barrio; ?></td>
                                <td><?php echo $value->ciudad; ?></td>
                                <td><?php echo $value->departamento; ?></td>
                                <td style="text-align: center"><?php echo $value->estado; ?></td>
                                <td style="text-align: center"><?php echo $value->sms; ?></td>
                                <td style="text-align: center"><?php echo $value->email; ?></td>
                                <td style="text-align: center"><?php echo $value->autoriza; ?></td>
                            </tr>
                            <?php
                            $i++;
                        }
                    }
                    ?>
                </table>
<?php exit(0); ?>
