<?php
defined('_JEXEC') or die('Restricted access');
ob_end_clean();

$opc = JRequest::getVar('opcion');

switch ($opc):
    case 1:
        $title = "Participante";
        break;
    case 2 :
        $title = "Regional";
        break;
    case 3:
        $title = "Segmento";
        break;
    default :
         $title = "Opción";
endswitch;
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--<html xmlns="http://www.w3.org/1999/xhtml">-->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>EXTRACTO</title>
        <!--<script type="text/javascript" src="templates/unidos/html/fancybox/custom/jquery-1.7.2.min.js"></script>-->
    </head>
    <?php ob_end_flush();?>
<body>
        <strong style="font-size: 26px"><?php echo JText::_('COM_AGANAR_TITTLE_REP_RANKING')." ".$title; ?></strong>
        <br />
        <div id="datosbasic">  
                <br />
                <table  class="tabla" cellspacing="3" cellpadding="3" style="text-align: center" >
                    <tr style="background-color: #000066;color: white" >
                        <th>Puesto</th>
                        <th><?php echo $title; ?></th>
                        <th>Puntos</th>
                        <th>Puntos Posibles</th>
                       <th>Cumpliento</th>
                    </tr>
            

                    <?php
                    if ($this->items) {
                        for ($i = 0; $i < count($this->items); $i++) {
                            ?>
                            <tr style="height: 25px;" <?= (($i % 2) != 0 ? ' class="trcolor"' : ''); ?>>
                                <td><?php echo $i+1;?> </td>
                                <td style="text-align: left;height: 25px;"><?php echo $this->items[$i]->nombre; ?></td>     
                                <td style="height: 25px;"><?php echo $this->items[$i]->puntos; ?></td>
                                <td style="height: 25px;"><?php echo $this->items[$i]->posibles; ?></td>
                                <td style="height: 25px;"><?php echo $this->items[$i]->cumplimiento;?>%</td>
                            </tr>
                        <?php
                        }
                    } else {
                        ?>
                        <td>Sin Resultado</td>
                        <td>0</td>
                        <td>0</td>
<!--                        <td>0</td>
                        <td>0</td>
                        <td>0</td>-->
<?php } ?>
                </table>
                <br />
            </div>
    </body>
<!--</html>-->

