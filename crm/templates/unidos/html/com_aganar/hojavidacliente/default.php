<?php

defined('_JEXEC') or die('Restricted access');
$sesion = JFactory::getSession();
$db = JFactory::getDbo();
$Oculseleclien = JRequest::getVar('bandera');
$user = JFactory::getUser();
$app = JFactory::getApplication();
if ($Oculseleclien):
    $sesion->set('Oculseleclien', $Oculseleclien);
endif;
//echo $link;
if($user->id == 0){
    $app->redirect('index.php');
    $app->close();
}
?>
<script type="text/javascript" src="templates/unidos/html/fancybox/custom/jquery-1.7.2.min.js"></script>
<link rel="stylesheet" href="/templates/unidos/css/aganar.css" type="text/css" />

<?php
ob_start();
?>
<script type="text/javascript">
    $(function() {
        $("#departamento").change(function(event) {
            var id = $("#departamento").find(':selected').val();
            $("#ciudad").load('filtros.php?id=' + id + '&task=ciudad');
        });
    });

</script>
<?php
$js = ob_get_contents();
ob_clean();
?>
<body>
    <?php
    $mainframe = JFactory::getApplication();
    $mainframe->triggerEvent('onJsLoad', array(JURI::root() . '/templates/unidos/html/script/jquery-1.3.2.min.js', null));
    $mainframe->triggerEvent('onJsLoad', array(null, '<script type="text/javascript" >jQuery.noConflict();</script>'));
    $mainframe->triggerEvent('onJsLoad', array(JURI::root() . '/templates/unidos/html/script/jquery.min.js', null));
    $mainframe->triggerEvent('onJsLoad', array(JURI::root() . '/templates/unidos/html/script/thickbox.js', null));
    $mainframe->triggerEvent('onJsLoad', array(null, $js));
    //print_r($_POST);
    ?>    

    <div id="encabezado">
        <div id ="titulo">
            <h2>HOJA DE VIDA CLIENTE</h2></div>
        <div id="clientes">
            <?php if (!$sesion->get('Oculseleclien')) { ?>
                <form action="index.php" method="post" name="Form">
                    <select id="clientebuscado" name="clientebuscado" onchange="javascript:document.Form.submit()">  
                        <!--<option value ="0">Seleccione...</option>-->
                        <?php echo $this->clientes; ?>
                    </select>
                    <input type="hidden" name="option" value="com_aganar" />
                    <input type="hidden" name="view" value="hojavidacliente" />
                    <?php
                    if ($sesion->get('clientebuscado')):
                        echo '<input type="hidden" name="band" value="1" />';
                    endif;
                    ?>
                </form>
            <?php }else { ?>
            <form action="<?php echo $sesion->get('regresoHVcliente') ;?>" method="post" name="Form">  
                    <input class="button art-button" type='submit' name='cerrarvh'  id='cerrarvh' value='Cerrar Hoja'/>
                </form>
            <?php } ?>
        </div>



    </div>
    <div id="datosbasic" >
        <br>
        <table class="tabla" border ="0">
            <tr>
                <td>Tipo identificación </td>
                <td><input readonly type="text" id="tpid" name="tpid" value="<?php echo $this->items[0]->tp_id; ?>"/></td>
                <td>Número identificación </td>
                <td><input readonly type="text" id="numidclien" name="numidcliente" value="<?php echo $this->items[0]->documento; ?>"/></td>
            </tr>  
            <tr>
                <td >Nombre cliente </td>
                <td colspan="3">
                    <input readonly type="text" id="nombreclien"  name="nombreclien" title="Ingrese el nombre del cliente" value="<?php echo $this->items[0]->nombre; ?>"/>
                </td>

            </tr>   
            <tr>
                <td>Gerente FB: </td>
                <td><input readonly type="text" id="comercial" name="comercial" value="<?php echo $this->items[0]->gerenFB; ?>" /></td>
                <td>Segmento: </td>
                <td>
                    <input readonly type="text" id="segclien" name="segclien" value="<?php echo $this->items[0]->segmento; ?>">
                </td>
            </tr>   
            <tr>
                <td>Gerente BB: </td>
                <td><input readonly type="text" id="gerenteBB" name="gerenteBB" value="<?php echo $this->items[0]->gerenBB; ?>" /></td>
                <td>Sector: </td>
                <td>
                    <input readonly type="text" id="sectorclien" name="sectorclien" value="<?php echo strtoupper($this->items[0]->sector); ?>"/>
                </td>
            </tr>   
            <tr>
                <td>Fecha Visita: </td> 
                <td><input readonly type="text" id="fechavisita" name="fechavisita" value="<?php echo $this->items[0]->fecha_ult_vis; ?>" /></td>
                <td>Regional: </td>
                <td><input readonly type="text" id="regional" name="regional" value="<?php echo $this->items[0]->regional; ?>"/> 

                </td>
            </tr>   
            <tr>
                <td>Dirección: </td>
                <td><input readonly type="text" id="direcclien" name="direcclien"  value="<?php echo $this->items[0]->direccion; ?>" title="Digite la direccion del cliente Carrera Cra, Calle Cll. Ejemplo Cra 55 No. 35-28 "/></td>
                <td>Tipo de negocio: </td>
                <td>
                    <input readonly type="text" id="tpnegocio" name="tpnegocio" value="<?php echo $this->items[0]->tpnego; ?>" />
                </td>
            </tr>  
            <td>Asistio</td>
            <td>
                <input readonly type="text" id="tpnegocio" name="tpnegocio" value="<?php echo $this->items[0]->asistio; ?>" /> 
            </td>
            <tr> 
            <tr>
                <td></td>
                <td style="text-align: center;"><a class="button art-button" href="javascript:window.open('templates/accion/cliente_nuev_edit.php?&task=new&user=<?php echo $user->id; ?>','','width=800,height=400,left=50,top=50,toolbar=yes');void 0">Agregar Cliente</a></td>
                <td style="text-align: center;"><a class="button art-button" href="javascript:window.open('templates/accion/cliente_nuev_edit.php?cliente=<?php echo $sesion->get('clientebuscado'); ?>&task=edit','','width=800,height=400,left=50,top=50,toolbar=yes');void 0">Editar Cliente</a></td>  
                <td></td>
            </tr>
        </table>
    </div>
   
    <h4>PRODUCTOS ACTUALES DEL CLIENTE</h4>
    <div id="financieros" >
        <br>
        <div id="financieros">
            <table class="tabla">
                <tr>
                    <td colspan="4"><h5>Productos Bancarios</h5></td>
                </tr>
                <?php
                $banco = $this->bancos;
                foreach ($banco as $value) {
                    ?>
                    <tr>
                        <td>Banco :</td>
                        <td><input readonly type="text"  value="<?php echo $value->banco; ?>" /> </td>
                        <td>Producto :</td>
                        <td>
                            <input readonly type="text"  value="<?php echo $value->producto; ?>" /> 
                        </td>

                    </tr> 
                <?php } ?>
                <tr>
                    <td colspan="4" style="text-align: right"> 
                        <a  class="button art-button" href="javascript:window.open('templates/accion/agregarbanco.php?cliente=<?php echo $sesion->get('clientebuscado'); ?>','','width=300,height=200,left=50,top=50,toolbar=yes');void 0">Agregar</a>
                    </td>
                </tr>
            </table>
        </div>
        <div id="financieros">
            <table class="tabla">
                <tr>
                    <td colspan="4"><h5>Productos Fiduciarias</h5></td>
                </tr>
                <?php
                $fidu = $this->fidu;
                foreach ($fidu as $value) {
                    ?>
                    <tr>
                        <td>Fiduciaria :</td>
                        <td><input readonly type="text"  value="<?php echo $value->fidu; ?>" /> </td>
                        <td>Producto :</td>
                        <td>
                            <input readonly type="text"  value="<?php echo $value->producto; ?>" /> 
                        </td>

                    </tr> 
                <?php } ?>
                <tr>
                    <td colspan="4" style="text-align: right"> 
                       <a class="button art-button" href="javascript:window.open('templates/accion/agregarfidu.php?cliente=<?php echo $sesion->get('clientebuscado'); ?>','','width=400,height=200,left=50,top=50,toolbar=yes');void 0">Agregar</a>
                    </td>
                </tr>
            </table>
        </div>
        <div id="financieros">
            <table class="tabla">
                <tr>
                    <td colspan="4">
                        <h5>Productos Comisionistas</h5></td>
                </tr>
                <?php
                $comi = $this->comi;
                foreach ($comi as $value) {
                    ?>
                    <tr>
                        <td>Comisionista :</td>
                        <td><input readonly type="text"  value="<?php echo $value->comi; ?>" /> </td>
                        <td>Producto :</td>
                        <td>
                            <input readonly type="text"  value="<?php echo $value->producto; ?>" />
                        </td>

                    </tr> 
                <?php } ?>
                <tr>
                    <td colspan="4" style="text-align: right"> 
                       <a class="button art-button" href="javascript:window.open('templates/accion/agregarcomi.php?cliente=<?php echo $sesion->get('clientebuscado'); ?>','','width=300,height=200,left=50,top=50,toolbar=yes');void 0">Agregar</a>
                    </td>
                </tr>
            </table>
        </div>
        <div id="financieros">
            <br/><?php $Infoadicional = $this->Infoadicional[0];
                // print_r($Infoadicional);
                ?>
            <table class="tabla" >
                <tr>
                    <td style="width: 70px ">Otros Financieros:</td>
                </tr>
                <tr>
                    <td>
                        <textarea readonly rows="2" cols="50" ><?php echo $Infoadicional->otros_Finan; ?></textarea>
                    </td>
                </tr>    
                <td colspan="4" style="text-align: right"> 
                   <a class="button art-button" href="javascript:window.open('templates/accion/agregarInfoAdicinal.php?cliente=<?php echo $sesion->get('clientebuscado'); ?>&task=Otros Financieros','','width=800,height=250,left=50,top=50,toolbar=yes');void 0">Editar</a>
                </td>
            </table>
        </div>
        <div id="financieros">
            <br/>
            <table class="tabla" >
                <tr>
                    <td style="width: 70px ">Proyectos actuales: </td>
                </tr>
                <tr>
                    <td>
                        <textarea readonly rows="2" cols="50" ><?php echo $Infoadicional->proyec_actuales; ?></textarea>
                    </td>
                </tr>    
                <td colspan="4" style="text-align: right"> 
                   <a class="button art-button" href="javascript:window.open('templates/accion/agregarInfoAdicinal.php?cliente=<?php echo $sesion->get('clientebuscado'); ?>&task=Proyectos actuales','','width=800,height=250,left=50,top=50,toolbar=yes');void 0">Editar</a>
                </td>
            </table>
        </div>
        <br/>
    </div>
    <h4>INFORMACIÓN DE CONTACTOS</h4>
    <div id="contactos" >
        <table class="tabla">
            <tr>
                <td colspan="4"><h5>Contactos</h5></td>
            </tr>

            <?php
            $contactos = $this->contactos;
            foreach ($contactos as $value) {
                ?>
                <tr>
                    <td>Nombre:</td>
                    <td><input readonly type="text" id="nombrecont" name="nombrecont" value="<?php echo $value->nombre; ?>" /></td>
                    <td>Producto :</td>
                    <td>Cargo:</td>
                    <td><input readonly type="text" id="cargocont" name="cargocont" value="<?php echo $value->cargo; ?>" /></td>
                    <td style="text-align: right">
                       <a class="button art-button" href="javascript:window.open('templates/accion/detallecont.php?conta=<?php echo $value->id; ?>','','width=800,height=300,left=50,top=50,toolbar=yes');void 0">Detalle</a>
                    </td>
                </tr> 
<?php } ?>
            <tr>
            </tr> 
            <tr>
                <td colspan="6" style="text-align: right"> 
                   <a class="button art-button" href="javascript:window.open('templates/accion/agregarcont.php?cliente=<?php echo $sesion->get('clientebuscado'); ?>','','width=800,height=250,left=50,top=50,toolbar=yes');void 0">Agregar</a>
                </td>
            </tr>
        </table>
    </div>
    <h4>SEGUIMIENTO DE VISITAS</h4> 
    <div id="datosbasic" >
        <br/>
        <table class="tabla" >
            <?php
            $seguimientos = $this->seguimientos;
            foreach ($seguimientos as $value) {
                ?>
                <tr>
                    <td style="width: 30px ">fecha:</td>
                    <td style="width: 70px "><input readonly type="text"  value="<?php echo $value->fecha; ?>" /></td>
                    <td style="width: 70px ">Seguimiento:</td>
                    <td>
                        <textarea readonly rows="2" cols="50" ><?php echo $value->segui; ?></textarea>
                    </td>
                </tr>    
<?php } ?>
            <td colspan="4" style="text-align: right"> 
               <a class="button art-button" href="javascript:window.open('templates/accion/agregarsegimiento.php?cliente=<?php echo $sesion->get('clientebuscado'); ?>&task=Seguimiento','','width=800,height=600,left=50,top=50,toolbar=yes');void 0">Agregar</a>
            </td>
        </table>
    </div>
    <h4>NOTICIAS</h4>
    <div id="datosbasic" >
        <br/>
        <table class="tabla">
            <?php
            $noticias = $this->noticias;
            foreach ($noticias as $value) {
                ?>
                <tr>
                    <td style="width: 30px ">fecha:</td>
                    <td style="width: 70px "><input readonly type="text"  value="<?php echo $value->fecha; ?>" /></td>
                    <td style="width: 70px ">Seguimiento:</td>
                    <td>
                        <textarea readonly rows="2" cols="50" ><?php echo $value->noti; ?></textarea>
                    </td>
                </tr>    
<?php } ?>


            <td colspan="4" style="text-align: right"> 
               <a class="button art-button" href="javascript:window.open('templates/accion/agregarsegimiento.php?cliente=<?php echo $sesion->get('clientebuscado'); ?>&task=Noticia','','width=800,height=600,left=50,top=50,toolbar=yes');void 0">Agregar</a>
            </td>
        </table>
    </div>

    <h4>SEGUIMIENTO DEL CLIENTE</h4>
    <div id="situacionactual" >
        <h4 style="text-align: center">Situación Actual </h4>

        <div id="st_act_interno">
<?php $situacion = $this->situacionact[0]; ?>  
            <br />
            <table class="tablaint" >
                <tr>
                    <td>Fecha última visita:</td>
                    <td><input readonly type="text"  value="<?php echo $situacion->ult_visita; ?>" /></td>
                    <td>Resultado Visita:</td>
                    <td><input readonly type="text"  value="<?php echo $situacion->result_visita; ?>" /></td>
                </tr>
                <tr>
                    <td>Compromiso:</td>
                    <td><input readonly type="text" value="<?php echo $situacion->compromiso; ?>" /></td>
                    <td>Producto:</td>
                    <td><input readonly type="text"  value="<?php echo $situacion->producto; ?>" /></td>
                </tr>
                <tr>
                    <td>Antiguedad:</td>
                    <td><input readonly type="text"  value="<?php echo $situacion->antiguedad; ?>" /></td>
                    <td>Saldo promedio mes:</td>
                    <td><input readonly type="text"  value="<?php echo number_format($situacion->saldo_prom_mes); ?>" /></td>
                </tr>
                <tr>
                    <td>Aportes promedio mes(# operaciones):</td>
                    <td><input readonly type="text"  value="<?php echo number_format($situacion->aport_prom_mes); ?>" /></td>
                    <td>Número de Pagos Prom. Mes:</td>
                    <td><input readonly type="text"  value="<?php echo number_format($situacion->n_pagosprom_mes); ?>" /></td>
                </tr>
                <tr>
                    <td>Fiducia Admón; tipo de negocio:</td>
                    <td><input readonly type="text"  value="<?php echo $situacion->fid_adm_tpneg; ?>" /></td>
                    <td>Recursos Administrados:</td>
                    <td><input readonly type="text"  value="<?php echo number_format($situacion->recursos_adm); ?>" /></td>
                </tr>
                <tr>
                    <td>Director Asignado/Vic de Gestion:</td>
                    <td><input readonly type="text"  value="<?php echo $situacion->dir_asig_vic_gest; ?>" /></td>
                    <td>Ejecutivo/Vic de Gestion:</td>
                    <td><input readonly type="text"  value="<?php echo $situacion->ejec_vic_gest; ?>" /></td>
                </tr> 
                <tr>
                    <td >Vencimiento del contrato:</td>
                    <td colspan="2"><input readonly type="text"  value="<?php echo $situacion->vence_contrato; ?>" /></td>

                </tr>
                <tr>
                    <td colspan="4" style="text-align: right">
                       <a class="button art-button" href="javascript:window.open('templates/accion/editsituacion.php?cliente=<?php echo $sesion->get('clientebuscado'); ?>','','width=800,height=600,left=50,top=50,toolbar=yes');void 0">Editar</a>
                    </td>
                </tr>
            </table>

        </div>
        <h4 style="text-align: center">Información Financiera </h4>
        <?php
        $infofinan = $this->infofinan[0];
        //echo "<pre>".print_r($this,1);
        ?>
        <div id="st_act_interno">
            <br />
            <table class="tablaint">
                <tr>
                    <td>Ventas anuales ($MM):</td>
                    <td><input readonly title="Esta sección no es editable" type="text"  value="<?php echo number_format($infofinan->ventas_anuales); ?>" /></td>
                    <td>Ingresos mensuales ($MM):</td>
                    <td><input readonly title="Esta sección no es editable" type="text" value="<?php echo number_format($infofinan->ingresos_men); ?>" /></td>
                </tr>
                <tr>
                    <td>Gastos mensuales ($MM):</td>
                    <td><input readonly  title="Esta sección no es editable" type="text"  value="<?php echo number_format($infofinan->gastos_men); ?>" /></td>
                    <td>Activos Totales ($MM):</td>
                    <td><input readonly  title="Esta sección no es editable" type="text"  value="<?php echo number_format($infofinan->activos_tot); ?>" /></td>
                </tr>
                <tr>
                    <td>Pasivos Totales ($MM):</td>
                    <td><input readonly title="Esta sección no es editable" type="text"  value="<?php echo number_format($infofinan->pasivos_tot); ?>" /></td>
                    <td>Sucursales:</td>
                    <td><input readonly title="Esta sección no es editable" type="text" value="<?php echo $infofinan->sucursales; ?>" /></td>
                </tr>
                <tr>
                    <td>Número Empleados:</td>
                    <td><input readonly title="Esta sección no es editable" type="text"  value="<?php echo number_format($infofinan->empleados); ?>" /></td>
                    <td>Operación en el Exterior:</td>
                    <td><input readonly  title="Esta sección no es editable" type="text"  value="<?php echo $infofinan->oper_exterior; ?>" /></td>
                </tr>
                <tr>
                    <td>Portafolio de Inversiones:</td>
                    <td><input readonly title="Esta sección no es editable" type="text"  value="<?php echo $infofinan->portafolio_inver; ?>" /></td>

                </tr>
                <tr>
                    <td colspan="4" style="text-align: right">
                        <!--este href es paraeditar los campos de la info financiera del cliente-->
                       <!--<a href="javascript:window.open('templates/accion/editInfoFinan.php?cliente=<?php echo $sesion->get('clientebuscado'); ?>','','width=800,height=300,left=50,top=50,toolbar=yes');void 0">Editar</a>-->
                    </td>
                </tr>
            </table>

        </div>
        <br />
    </div>
    <div id="infoadicional">
        <br/>
        <table class="tabla" >
            <tr>
                <td style="width: 70px ">Generación nuevos negocios: </td>
            </tr>  <tr> 
                <td>
                    <textarea readonly rows="2" cols="50" ><?php echo $Infoadicional->genera_new_nego; ?></textarea>
                </td>
            </tr>    
            <td colspan="4" style="text-align: right"> 
               <a class="button art-button" href="javascript:window.open('templates/accion/agregarInfoAdicinal.php?cliente=<?php echo $sesion->get('clientebuscado'); ?>&task=Generación nuevos negocios','','width=800,height=250,left=50,top=50,toolbar=yes');void 0">Editar</a>
            </td>
        </table>
    </div>
    <div id="infoadicional">
        <br/>
        <table class="tabla" >
            <tr>
                <td style="width: 70px ">Negocios en curso: </td>
            </tr>  <tr> 
                <td>
                    <textarea readonly rows="2" cols="50" ><?php echo $Infoadicional->Nego_en_curso; ?></textarea>
                </td>
            </tr>    
            <td colspan="4" style="text-align: right"> 
               <a class="button art-button" href="javascript:window.open('templates/accion/agregarInfoAdicinal.php?cliente=<?php echo $sesion->get('clientebuscado'); ?>&task=Negocios en curso','','width=800,height=250,left=50,top=50,toolbar=yes');void 0">Editar</a>
            </td>
        </table>
    </div>
    <div id="infoadicional">
        <br/>
        <table class="tabla" >
            <tr>
                <td style="width: 70px ">Propósito visita: </td>
            </tr> 
            <tr>   
                <td>
                    <textarea readonly rows="2" cols="50" ><?php echo $Infoadicional->Propo_visita; ?></textarea>
                </td>
            </tr>    
            <td colspan="4" style="text-align: right"> 
               <a class="button art-button" href="javascript:window.open('templates/accion/agregarInfoAdicinal.php?cliente=<?php echo $sesion->get('clientebuscado'); ?>&task=Propósito','','width=800,height=250,left=50,top=50,toolbar=yes');void 0">Editar</a>
            </td>
        </table>
    </div>
    <div id="infoadicional">
        <br/>
        <table class="tabla" >
            <tr>
                <td style="width: 70px ">Beneficio: El Beneficio de esta reunion sera: </td>
            </tr> 
            <tr>
                <td>
                    <textarea readonly rows="2" cols="50" ><?php echo $Infoadicional->Beneficio_visi; ?></textarea>
                </td>
            </tr>    
            <td colspan="4" style="text-align: right"> 
               <a class="button art-button" href="javascript:window.open('templates/accion/agregarInfoAdicinal.php?cliente=<?php echo $sesion->get('clientebuscado'); ?>&task=Beneficio','','width=800,height=250,left=50,top=50,toolbar=yes');void 0">Editar</a>
            </td>
        </table>
    </div>
</body>

