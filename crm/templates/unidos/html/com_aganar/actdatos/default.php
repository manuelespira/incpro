    <?php defined('_JEXEC') or die('Restricted access');
$sesion = JFactory::getSession();
$_link = $_SERVER['REQUEST_URI']."&ingreso=1";
$ciudad = $sesion->get('ciudad');
?>
<script type="text/javascript" src="templates/unidos/html/fancybox/custom/jquery-1.7.2.min.js"></script>
<link rel="stylesheet" href="/templates/unidos/html/script/thickbox.css" type="text/css" />
<link rel="stylesheet" href="/templates/unidos/css/aganar.css" type="text/css" />
<script type="text/javascript" src="/templates/unidos/html/script/thickbox.js"></script>

<style type="text/css">
    
    input[type="text"], input[type="password"], input[type="email"], input[type="url"], textarea ,select{
        width:90%
    }
    
</style>
<?php
//ob_start();
?>
<script type="text/javascript">                     
            $(function() {
                $("#departamento").change(function(event){
                    var id = $("#departamento").find(':selected').val();
                    $("#ciudad").load('filtros.php?id='+id+'&task=ciudad');
                });
            });
            
            function valida(){
               // alert("aqui");
                var f = document.Form;
                var errores=""; 
                var opciones2 = document.getElementsByName("sms");
                var seleccionado2 = false;
                
                
                e=f.correo.value;
                if ((e.indexOf(".") > 2) && (e.indexOf("@") > 0) && (e.indexOf(".") < (e.length-2))) errores=errores; //correcto
                else errores+='Debe introducir un email v\u00E1lido\r\n';
                if(!/^[0-9]{10}$/.test(f.celular.value))errores += "El número de celular debe tener 10 dígitos.\r\n";
                if(!/^[0-9]{5,7}$/.test(f.telefono.value))errores += "El número de teléfono debe tener entre 5 y 7 dígitos.\r\n";
                if(f.departamento.value==0|| f.ciudad.value==0) errores+='Debe indicar el departamento y la ciudad de ubicaci\u00f3n\r\n';
                if(f.direccion.value.length<5) errores+='Debe introducir una dirección\r\n';
                if(f.barrio.value=='')errores+='Debe especificar un barrio\r\n';
                if(f.genero.value==0) errores+='Debe especificar un g\u00e9nero\r\n';
                if(f.terminos.checked) errores+='';
                else errores+='Debe aceptar los terminos y condiciones\r\n';
                if(f.pass2.value==''|| f.pass.value=='' ) errores+='Debe debe ingresar su password en las dos casillas\r\n';
                else if (!/^.*(?=.{8,20})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[|@#$%&\^\+\*\_\:\;\,\.\-\=\/\!]).*$/.test(f.pass.value))
                       errores+='El password no es muy fuerte por favor verifiquelo';
                else if(f.pass2.value!=f.pass.value) errores+='La confirmaci\u00F3n y el password deben ser iguales\r\n'; 
        
                for (var i = 0; i < opciones2.length; i++) {
                          if (opciones2[i].checked) {
                              seleccionado2 = true;
                              break;
                          }
                 }

                 if (!seleccionado2) {
                      errores+='Autoriza el envío de mensajes de texto al celular?\r\n';
                 }
                  opciones2 = document.getElementsByName("mail");
                  seleccionado2 = false;
                  for (var i = 0; i < opciones2.length; i++) {
                          if (opciones2[i].checked) {
                              seleccionado2 = true;
                              break;
                          }
                 }
                 if (!seleccionado2) {
                      errores+='Autoriza el envío de mensajes de correo?\r\n';
                 }
                      if(errores=="") f.submit();
                      else alert(errores);
                  }   
           
</script>
<?php
//$js = ob_get_contents();
//ob_clean();
?>
<body>
<?php
//
  ?>    
    
    <form action="index.php" method="post" name="Form" autocomplete="false">
        <br>
        <div id="formulario" align="center" >
             <h1>Formulario actualización datos</h1><br />
            <table style="width: 90%;height: 90%" border="0">
                 <tbody>
                    <tr>
                        <td>Cedula: </td>
                        <td>
                           <input type="text" value="<?php echo $this->items[0]->cedula; ?>" disabled>
                        </td>
                         <td> Nombre: </td>
                        <td>
                           <input type="text" value="<?php echo ucwords(strtolower($this->items[0]->nombres." ".$this->items[0]->apellidos)); ?>" disabled> 
                        </td>
                    </tr>
                    
                    <tr>
                        <td>*Género:</td>
                        <td>
                            <select id="genero" name="genero">
                                        <option value="0">seleccione...</option>
                                        <option value="1">Masculino</option>
                                        <option value="2">Femenino</option>
                                    </select>
                        </td>
                        <td>*Correo:</td>
                        <td>
                          <input type="text" name="correo" id="correo" value="<?php echo $this->items[0]->correo; ?>"> 
                        </td>
                    </tr>
                    <tr>
                        <td> *Telefono fijo:</td>
                        <td>
                         <input type="text" name="telefono" id="telefono" value="<?php echo $this->items[0]->telefono; ?>"> 
                        </td>
                         <td> Extensión: </td>
                        <td>
                         <input type="text" name="extension" id="extension" value="<?php echo $this->items[0]->extension; ?>" >
                        </td>
                    </tr>
                    <tr>
                        <td> *Celular: </td>
                        <td> <input type="text" name="celular" id="celular" value="<?php echo $this->items[0]->celular; ?>"></td>
                        <td> *Dirección: </td>
                        <td> <input type="text" name="direccion" id="direccion" value="<?php echo $this->items[0]->direccion; ?>"></td>
                     </tr>
                     <tr>
                        <td> *Barrio: </td>
                        <td> <input type="text" name="barrio" id="barrio" value="<?php echo $this->items[0]->barrio; ?>"></td>
                        <td> *Departamento: </td>
                        <td> <select name="departamento" id="departamento">
                        <option value ="0">Seleccione...</option>
                        <?php echo $this->depts;?>
                        </select>
                        </td>
                     </tr>
                     <tr>
                          <td > *Ciudad: </td>
                         <td >
                            <?php
                    if($ciudad != 0){
                         ?>        
                         <select name="ciudad" id="ciudad">
                    <option value ="0">Seleccione...</option>
                    <?php echo $this->ciudad;?>
                </select>
            <?php
            }else{
            ?>
                <select name="ciudad" id="ciudad">
                    <option value ="0">Seleccione...</option>
                </select>
            <?php }?>
                     </td>   
               </tr>
                    <tr>
                        <td> *Contraseña: </td>
                        <td>
                            <a href="#" class="Ntooltip"><input type="password" name="pass" id="pass"><span>Su password debe contener mínimo 8 caracteres, una letra mayúscula, una letra minúscula, al menos un número, y un caracter especial.</span></a>
                        </td>
                         <td><label for='pass2'>*Confirmar contraseña:</label></td>
                        <td>
                            <a href="#" class="Ntooltip"><input type="password" name="pass2" id="pass2"><span>Su password debe contener mínimo 8 caracteres, una letra mayúscula, una letra minúscula, al menos un número, y un caracter especial. </span></a>
                                </td>
                        
                    </tr>
                    <tr>
                        <td colspan="2"> 
                            <br/>
                            ¿Autoriza el envío de mensajes a su correo electrónico? 
                        </td>
                        <td>
                            SI<input type="radio"   name="mail" id="mail" value="1" <?php echo $this->socio->envio_email=='1'?'checked="true"':'';?> />
                            &nbsp;&nbsp;NO<input type="radio" name="mail" id="mail"  value="0" <?php echo $this->socio->envio_email=='0'?'checked="true"':'';?>/>
                        </td>
                      
                    </tr>
                    <tr>
                        <td colspan="2">
                            <br/>
                             ¿Autoriza el envío de mensajes de texto al celular? 
                         </td>
                        <td>     
                        SI<input type="radio"  name="sms" id="sms"  value="1" <?php echo $this->socio->envio_sms=='1'?'checked="true"':'';?>/>
                        &nbsp;&nbsp;NO<input type="radio" name="sms" id="sms" value="0" <?php echo $this->socio->envio_sms=='0'?'checked="true"':'';?>/>
                        </td>
                        
                    </tr>
                    
                    
                </tbody>
            </table>
             <br/> 
           *<input type="checkbox" name="terminos" id="terminos" vaule="1"> Acepto terminos y condiciones&nbsp;&nbsp;<a href="images/terminos/terminosycondiciones.pdf" target="_blank">Ver aquí</a>
            <h5>*Campos obligatorios</h5><br>
            <input type="button" value="Guardar" onclick="javascript:valida()"><br><br>
        </div>

    <input type="hidden" name="option" value="com_aganar" />
    <input type="hidden" name="view" value="actdatos" />
    <input type="hidden" name="band" value="1" />
    <input type="hidden" name="task" value="infoparticipante">

    </form>
</body>


