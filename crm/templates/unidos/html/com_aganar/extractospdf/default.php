<?php

defined('_JEXEC') or die('Restricted access');
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html;  charset=utf-8" />
<title>EXTRACTO</title>
<script type="text/javascript" src="templates/unidos/html/fancybox/custom/jquery-1.7.2.min.js"></script>
<link rel="stylesheet" href="/templates/unidos/html/script/thickbox.css" type="text/css" />
<link rel="stylesheet" href="/templates/unidos/css/aganar.css" type="text/css" />
<script type="text/javascript" src="/templates/unidos/html/script/thickbox.js"></script>

    <script type="text/javascript">
    function valida(f) {
     if(f.regional.selectedIndex < 1){
         alert('Debe seleccionar una regional\r\n') ;
     }else if(f.periodo.selectedIndex < 1){
       alert('Debe seleccionar un periodo\r\n') ;
      }else if(f.noticia.value=="" || f.noticia.value=="No Existe noticia" || f.noticia.value=="Cargando Noticia..."){
           alert('Debe ingresar un a noticia\r\n') ;
      }else{

          f.submit();
      }
    }
    
            
   
    //funcion que limpia los demas campos
    function LimpiarInput(destino,destino2)
    {
        destino.value="";   
    }
    //funcion que llena los datos 
    function LlenarDatos(text,destino)
    {
        //alert(''+text);
        var datos = text.split('|'); //dividimos los datos para colocarlos en el lugar correcto     
        destino.value = datos[0];
    }
    
    //fucion con la cual obtenemos  los datos 
    function obten_datos(arrastre,destino)
    {
        //var destino = "noticia";
        //alert("arrastre: "+arrastre+" destino: "+destino);
        destino = document.getElementById(destino);
        LimpiarInput(destino);
        if(arrastre.options[arrastre.selectedIndex].value != 0)
        {
            arrastre.disabled = true;
            destino.disabled = true;
            destino.value = 'Cargando Noticia...';
           
            $.ajax({
                type: 'get',
                dataType: 'text',
                url: 'templates/unidos/html/com_aganar/extractospdf/obtendatos.php',
                data: {valor: arrastre.options[arrastre.selectedIndex].value},
                success: function(text){
                    LlenarDatos(text,destino);
                    arrastre.disabled = false;
                    destino.disabled = false;
                    //destino.readOnly = true;
                 }
            });     
        }
    }
    
    
    </script>
</head>

<body><!--onload="javascript:muestraObj(2)"-->
        <h2 style="text-align: center">Extractos PDF</h2>
        <br />
        <div id="datosbasic">
            <br />
            
                <table class="tablaliq" border="0" cellspacing="7" cellpadding="3" >
                    <tbody>
                        <form name="formpdf" action="#" method="POST" target="_blank"> 
                            <tr>
                                <td align="right" style="text-align: right;" >Regional : </td> 
                                <td><?php echo JHTML::_('aganar.regional', 'regional', JRequest::getVar('regional'), "", 1, 'selectBox'); ?> </td>
                                 <td rowspan="2"  style="text-align: right">Noticia:</td>
                                   <td rowspan="2"><textarea rows="4" cols="50" id="noticia" name="noticia"  maxlength="540" style="resize: none;"><?php echo $this->noticia; ?></textarea></td>
                             </tr>
                            <tr>
                                <td style="text-align: right">Periodo : </td>
                                <td><?php
                                $campo = "'noticia'";
                                echo JHTML::_('aganar.periodos_liq2', 'periodo', JRequest::getVar('periodo'),'onchange="javascript:obten_datos(this,'.$campo.')"', 1, 'selectBox'); ?></td>
                            </tr>
                            <tr>
                              
                            </tr>
                        <tr>
                            <td colspan="4" style="text-align: center">
                                    <input name="enviar" type="hidden" value='Enviar'/>
                                   <a class="button" onclick="valida(document.forms['formpdf'])" href="#">Enviar</a>
                          </form>
                            </td>
                        </tr>
                    </tbody>
                </table>	
           
            <br />
        </div>
    </body>
</html>
