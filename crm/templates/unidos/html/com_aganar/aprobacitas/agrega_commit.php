<?php
$db = JFactory::getDbo();
$visitanum = JRequest::getVar('visitanum');
$sql = 'SELECT extra_info
            FROM jfb_jevents_vevdetail
            WHERE evdet_id = ' . $visitanum . '  ';
$db->setQuery($sql);
$observacion = $db->loadResult();

$band = JRequest::getVar('band');
if (isset($band) && $band == 1) {

    $sql = "UPDATE jfb_jevents_vevdetail SET extra_info = '" . JRequest::getVar('observacion') . "' 
        WHERE evdet_id = " . $visitanum;
    $db->setQuery($sql);
    if ($db->query()) {
        echo "<script languaje='javascript' type='text/javascript'>
            alert('Ingreso exitoso');
            window.close();
            </script>";
    } else {
        echo "<script languaje='javascript' type='text/javascript'>
            alert('Lo sentimos ocurrio un error en la inserción, por favor intente nuevamente!');
            </script>";
    }
}
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="templates/unidos/css/aganar.css" type="text/css" />
        <link rel="stylesheet" href="templates/unidos/css/template.css" type="text/css" />
        <script>

            function valida() {
                if (document.FormAdmin.observacion.value == "") {
                    alert('Por favor ingrese una observación antes de guardar.');
                } else {
                    document.FormAdmin.submit();
                }
            }
        </script>
    </head>
    <body>

<?php if ($observacion == "") { ?>

            <form action="#" name="FormAdmin" method="post" >
                <div id="cuadro1" style ="width:500px" >
                    <h4>Observaciones</h4> 
                    <br />
                    <table class="tablaint" border="0" >
                        <tr>
                            <td colspan="2">
                                <textarea name="observacion" rows="4" cols="70"></textarea>
                            </td>
                        </tr>
                        <tr><td><br></td></tr>
                        <tr>  
                            <td style="text-align: center;width:225px " >
                                <a class="button art-button" href="javascript:window.close()">Cerrar</a></td>
                            <td style="text-align: center;width:225px">
                                <a class="button art-button" href="javascript:valida()">Guardar</a></td>
                            </td>
                        </tr>
                        <tr><td><br></td></tr>
                        <tr><td colspan="2" style="color:red;text-align:center" ><strong>NOTA: La observación solo se agrega una véz.</strong></td></tr>
                    </table>
                    <br />
                </div>
                <input type="hidden" name="band" id="band" value="1">
            </form>
<?php } else { ?>
            <div id="cuadro1" style ="width:500px;margin-top: 30px" > 
                <h4>Observaciones</h4> 
                <br />
                <table class="tablaint" border="0" >
                    <tr>
                        <td colspan="2">
                            <textarea readonly="readonly"name="observa" rows="4" cols="70"><?php echo $observacion; ?></textarea>
                        </td>
                    </tr>
                     <tr><td><br></td></tr>
                        <tr><td colspan="2" style="color:red;text-align:center" ><strong>Observación no editable!</strong></td></tr>
                </table>
                <br />
            </div>
<?php } ?>
    </body>