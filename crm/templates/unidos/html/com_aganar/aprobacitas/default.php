<?php
defined('_JEXEC') or die('Restricted access');
$sesion = JFactory::getSession();
$user = JFactory::getUser();
$app = JFactory::getApplication();  
//if($user->id == 0){
//    $app->redirect('index.php');
//    $app->close();
//}
$vistacomercial = JRequest::getVar('comercial');

//print_r($_POST);
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="templates/unidos/css/aganar.css" type="text/css" />
        <link rel="stylesheet" href="templates/unidos/css/template.css" type="text/css" />
        <link rel="stylesheet" href="templates/datapicker/development-bundle/themes/base/jquery.ui.all.css">
        <script src="templates/datapicker/development-bundle/jquery-1.8.2.js"></script>
        <script src="templates/datapicker/development-bundle/ui/jquery.ui.core.js"></script>
        <script src="templates/datapicker/development-bundle/ui/jquery.ui.widget.js"></script>
        <script src="templates/datapicker/development-bundle/ui/jquery.ui.datepicker.js"></script>
        <link rel="stylesheet" href="templates/datapicker/development-bundle/demos/demos.css" />
<script type="text/javascript">

$(function() {
    $("#desde").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        changeYear: false,
        numberOfMonths: 1,
        dateFormat: "yy-mm-dd",
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        onSelect: function(selectedDate) {
            $("#to").datepicker("option", "minDate", selectedDate);
        }
    });
    $("#hasta").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        changeYear: false,
        numberOfMonths: 1,
        dateFormat: "yy-mm-dd",
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        onSelect: function(selectedDate) {
            $("#from").datepicker("option", "maxDate", selectedDate);
        }
    });
});
 function all_redencion(){
        if (document.forms['FormAutoriza'].elements['todas'].checked == 1){
            $('input[name^="visitas"]').attr('checked','true');
        }
        else{
            $('input[name^="visitas"]').removeAttr('checked');	
        }
    }

</script>
<script language="JavaScript">
                  //ejecuta una accion determinada deacuerdo a la opcion especificada
  function sendAction(){

        if (document.Formcomercial.desde.value=="" && document.Formcomercial.hasta.value=="") {
                alert('Debe especificar el periodo');
        } else {
                if (document.Formcomercial.desde.value!="" && document.Formcomercial.hasta.value=="") {
                        alert('Debe ingresar la Fecha Inicial');
                } else {
                   if (document.Formcomercial.desde.value=="" && document.Formcomercial.hasta.value!=""){
                        alert('Debe ingresar la Fecha final');
                     }else{
                        if (document.Formcomercial.desde.value > document.Formcomercial.hasta.value) {
                                  alert('La fecha inicial es mayor a la fecha final, ingrese las fechas correctamente');
                          } else {
                                 document.Formcomercial.submit();
                          }
                    }

                }
            }
         }
</script>

    </head>
    <body>
        <form name="Formcomercial" method="post" action="index.php">
            <table class="tabla">
                <tr>
                    <td><h5>Comercial</h5></td>
                    <td> <select name="comercial" style="width: 180px;height: 19px">
                            <option value="-1" >Todos </option>
                                <?php echo $this->comerciales; ?>
                        </select>
                    </td>
                    <!--</tr><tr>-->
                    <td><h5>Desde</h5> </td>
                    <td><input readonly type="text" id="desde" name="desde" value="<?php echo $sesion->get('desde')?>"/></td>
                    <td><h5>Hasta</h5></td>
                    <td><input readonly type="text" id="hasta" name="hasta" value="<?php echo $sesion->get('hasta')?>"/></td>
                    <td><a class="button art-button" href="javascript:sendAction()">Consultar</a></td> 
                </tr>
                <tr>
                  <input type="hidden" name="option" value="com_aganar" />
                  <input type="hidden" name="view" value="aprobacitas" />
                </tr>
            </table>
        </form>
        <?php $citas = $this->items;
        $comerciales = $this->listacomerciales;
        //print_r($comerciales);
        ?>
        <div style="overflow-x: scroll;width: auto;height:auto ">
        <form name="FormAutoriza" method="post" action="index.php">
        <table class="tabla3" >
            <thead>
            <tr>
                <?php if($vistacomercial == -1)
                     echo "<th>Comercial</th>";
                    ?>
                <th >Cliente</th>
                <th>Fecha Visita</th>
                <th>Inicio</th>
                <th>Fin</th>
                <th>Contacto</th>
                <th>Localización</th>
                <th>Actividad</th>
                <th>Estado</th>
                <th>Comentario</th>
                <th colspan="3">Todas<input type="checkbox" name="todas" id="todas" onclick="all_redencion()" value="true"></th>
                
            </tr>  
            </thead>
            <tbody>
                <?php $i = 0;
                while ($i< count($citas)){ ?>
                 
                 <tr<?=(($i%2)!=0?' class="trcolor"':'');?>>
                 <?php if($vistacomercial == -1)
                     //echo "aquidentro";exit;
                     foreach ($comerciales as $key => $value) {
                        if($key == $citas[$i]->created_by){
                            echo "<td style='width: 100px'>".ucwords(strtolower($value))."</td>";
                     } 
                  }
                    ?>
                <td style="width: 150px"><?php echo ucwords(strtolower($citas[$i]->nomcliente));?></td>
                <td style="width: 80px"><?php echo substr($citas[$i]->inicio,0,10);?></td>
                <td><?php echo substr($citas[$i]->inicio,11,-3);?></td>
                <td><?php echo substr($citas[$i]->fin,11,-3);?></td>
                <td><?php echo ucwords(strtolower($citas[$i]->contact));?></td>
                <td><?php echo strtolower($citas[$i]->location);?></td>
                <td> <a href="javascript:window.open('templates/accion/detallevisita.php?evento=<?php echo $citas[$i]->ev_id; ?>','','width=500,height=400,left=50,top=50,toolbar=yes');void 0">Detalle</a></a></td>
                <!--<td><?php echo $citas[$i]->summary;?></td>-->
                <td><?php echo $citas[$i]->estado;?></td>
                <td style="text-align: center">
                    <a href="#" onClick="window.open('index.php?option=com_aganar&view=aprobacitas&Itemid=1168&visitanum=<?php echo $citas[$i]->ev_id; ?>&layout=agrega_commit&cond=popup','popup','width=560px,height=300px,scrollbars=1')">
                        <?php if($citas[$i]->obser == ""){
                                echo "Agregar";
                            }else{
                                echo "Ver"; 
                            }
                            ?>
                    
                    </a>            </td>
                <td style="text-align: center"> 
                    <?php if($citas[$i]->est==1){?>
                    <input type="checkbox" name="visitas[]" id="visitas[]"  value="<?php echo $citas[$i]->ev_id; ?>">
                    <?php }?>
                </td>
 
            </tr>
                <?php
                $i++;}?>
                </tbody>
        </table>
            </div>
        <table style="width: 100%;">
            <tr>
                <td style="text-align: right"><input class="button art-button" type="submit" name="Autorizar" value="Autorizar" ></td>
                <td style="text-align: left"><input class="button art-button" type="submit" name="Rechazar" value="Rechazar" ></td>
            </tr>
      </table>
            <input type="hidden" name="option" value="com_aganar" />
            <input type="hidden" name="view" value="aprobacitas" />
            <input type="hidden" name="comercial" value="<?php echo $sesion->get('comercial')?>" />
            <input type="hidden" name="hasta" value="<?php echo $sesion->get('hasta')?>" /> 
            <input type="hidden" name="desde" value="<?php echo $sesion->get('desde')?>" />
             
            </form>      
    </body>

</html>

