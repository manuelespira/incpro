<?php
$session = JFactory::getSession();
$per = intval(JRequest::getVar('periodo'));
$var = intval(JRequest::getVar('variable'));
?> 
<h2>Liquidaciones Participantes <?php echo $this->data[0]->variable; ?></h2>
 <table class="tablaliq" border="1" cellspacing="3" cellpadding="2" >
                    <tr> 
                        <th style="text-transform: uppercase;text-align: center;background-color: #000066;color: white;">Cédula</th>
                        <th style="text-transform: uppercase;text-align: center;background-color: #000066;color: white;">Participante</th>
                        <th style="text-transform: uppercase;text-align: center;background-color: #000066;color: white;">Cargo</th>
                        <th style="text-transform: uppercase;text-align: center;background-color: #000066;color: white;">Regional</th>
                        <th style="text-transform: uppercase;text-align: center;background-color: #000066;color: white;">Segmento</th>
                        <th style="text-transform: uppercase;text-align: center;background-color: #000066;color: white;">Estado</th>
                        <th style="text-transform: uppercase;text-align: center;background-color: #000066;color: white;">Acumulados</th>
                        <?php if ($per > 0 && $var >0){?>
                            <th style="text-transform: uppercase;text-align: center;background-color: #228811;color: #000066;">Objetivo</th>
                            <th style="text-transform: uppercase;text-align: center;background-color: #228811;color: #000066;">Resultado</th>
                            <th style="text-transform: uppercase;text-align: center;background-color: #228811;color: #000066;">Cumplimiento</th>
                            <th style="text-transform: uppercase;text-align: center;background-color: #228811;color: #000066;">Puntos Mes</th>
                        <?php }?>
                    </tr>
                    <?php
                    $datos = $this->data;
                    if ($datos) {
                        $i = 0;
                        foreach ($datos as $value) {
                            ?>
                            <tr style="height: 25px;" <?= (($i % 2) != 0 ? '' : 'style="background-color: #DDDDDD;"'); ?>>
                                <td><?php echo $value->cedula; ?></td>
                                
                                <td><?php echo utf8_decode(strtoupper($value->participante)); ?></td>
                                <td><?php echo $value->cargo; ?></td>
                                <td><?php echo $value->regional; ?></td>
                                <td><?php echo $value->segmento; ?></td>
                                <td><?php echo $value->estado; ?></td>
                                <td><?php echo $value->acumulado; ?></td>
                                <?php if ($per > 0 && $var >0){
                                         if($value->id_var <= 2) {?>
                                              <td><?php echo $value->objetivo==''? '0' :$value->objetivo ; ?></td>
                                              <td><?php echo $value->resultado==''? '0' :$value->resultado ; ; ?></td>
                                         <?php }else{ ?>
                                              <td><?php echo $value->objetivo==0||$value->objetivo==''? 'NO':'SI'; ?></td>
                                              <td><?php echo $value->resultado==0||$value->resultado==''? 'NO':'SI';?></td>
                                        <?php  }   ?>
                                <td><?php echo $value->cumplimiento==''?'0%':strtr($value->cumplimiento,'.',',').'%'; ?></td>
                                <td><?php echo $value->puntos==''?'0':$value->puntos; ?></td>
                                
                                 <?php }?>
                            </tr>
                            <?php
                            $i++;
                        }
                    }
                    ?>
                             <?php if ($per > 0 && $var >0){ ?>
                            <tr>
                                <td colspan="11">
                                    <font style="color:red" > NOTA: &nbsp;</font> Los registros que se encuentran en cero son usuarios que no se evaluaron en el mes y variable seleccionados
                                </td>
                            </tr>
                                 <?php  }   ?>
                </table>

<?php exit(0); ?>
