jQuery(document).ready( function() {
    // binds form submission and fields to the validation engine
    jQuery("#frmActualizarDatos").validationEngine();
    jQuery("#frmSubirArchivo").validationEngine();    
    jQuery("#frmOpciones").validationEngine();
    jQuery("#frmFiltroLog").validationEngine();
    jQuery("#frmRedencion").validationEngine();
});
