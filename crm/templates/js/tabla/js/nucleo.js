$.validator.setDefaults({
	highlight: function(input) {
		$(input).addClass("ui-state-error");
	},
	unhighlight: function(input) {
		$(input).removeClass("ui-state-error");
	}
});
$(document).ready(function(){
    
    $.datepicker.regional['es'] = {
			changeMonth: true,
			changeYear: true,
			yearRange: "-105:+10",
			closeText: 'Cerrar',
			prevText: '&#x3c;Ant',
			nextText: 'Sig&#x3e;',
			currentText: 'Hoy',
			monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
			'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
			monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
			'Jul','Ago','Sep','Oct','Nov','Dic'],
			dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
			dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
			dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
			weekHeader: 'Sm',
			dateFormat: 'yy-mm-dd',
			firstDay: 1,
			isRTL: false,
			showMonthAfterYear: false,
			showAnim: 'clip',
			yearSuffix: ''};
		$.datepicker.setDefaults($.datepicker.regional['es']);
	
        $('#FechaNacimiento').datepicker();
        
	/*Validacion de todos los formularios*/
	$(document).ready(function(){$("#form_mobil").validate();});
 
       
});




/*function enviar(){
     if(!$('#form_mobil').valid()){
		return false;
	}else{
            $('#form_mobil').submit(function(event) {
                event.preventDefault();
                var datos = $(this).serialize(); 
                $.ajax({
                    url: 'nucleo.php',
                    data: datos,
                    type: 'post',
                    dataType: 'html',
                    beforeSend: function() {
                        $('#guardar').html('almacenando');
                    },
                    success: function(datos){
                        $('#guardar').html(datos).show();
                    }

                }); 
            }); 
        } 
        
}*/