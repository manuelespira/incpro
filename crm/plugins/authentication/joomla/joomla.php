<?php

/**
 * @copyright  Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Joomla Authentication plugin
 *
 * @package    Joomla.Plugin
 * @subpackage  Authentication.joomla
 * @since 1.5
 */
class plgAuthenticationJoomla extends JPlugin {

    /**
     * This method should handle any authentication and report back to the subject
     *
     * @access  public
     * @param  array  Array holding the user credentials
     * @param  array  Array of extra options
     * @param  object  Authentication response object
     * @return  boolean
     * @since 1.5
     */
    function onUserAuthenticate($credentials, $options, &$response) {


        /* Iniciando las variables de sesion en blanco */
        $sesion = JFactory::getSession();
        //$sesion->clear('authenticate');
        //$sesion->clear('bloqueado');
        /* Iniciando las variables de sesion en blanco */

        $response->type = 'Joomla';
        // Joomla does not like blank passwords
        if (empty($credentials['password'])) {
            $response->status = JAuthentication::STATUS_FAILURE;
            $response->error_message = JText::_('JGLOBAL_AUTH_EMPTY_PASS_NOT_ALLOWED');
            return false;
        }

        // Initialise variables.
        $conditions = '';

        // Get a database object
        $db = JFactory::getDbo();
//$query = $db->getQuery(true);

       // $query->select('select id, password, block,estado,intentos');
        //$query->from('#__users');
       // $query->where('username=' . $db->Quote($credentials['username']));
       // echo
        $query = 'select id, password, block,estado,intentos
            from #__users
            where username like'.$db->Quote($credentials['username']);
        //exit;
        
        $db->setQuery($query);
        $result = $db->loadObject();
        //print_r($result);
        if ($result){
            $parts = explode(':',$result->password);
            $crypt = $parts[0]; 
            $salt = @$parts[1];
            $testcrypt = JUserHelper::getCryptedPassword($credentials['password'], $salt);
            if ($result->block == 1) {
                $sesion->set('authenticate', 'invalid');
                $sesion->set('bloqueado', 'Usuario Bloqueado');
                $response->status = JAuthentication::STATUS_FAILURE;
                $response->error_message = JText::_('JGLOBAL_AUTH_INVALID_PASS');
            }elseif ($result->estado == 1){
                $sesion->set('authenticate', 'invalid');
                $sesion->set('inactivo', 'Su usuario se encuentra inactivo');
                $response->status = JAuthentication::STATUS_FAILURE;
                $response->error_message = JText::_('JGLOBAL_AUTH_INVALID_PASS');
           }else{
                if ($crypt == $testcrypt) {
                    $user = JUser::getInstance($result->id); // Bring this in line with the rest of the system
                    $response->email = $user->email;
                    $response->fullname = $user->name;
                    if (JFactory::getApplication()->isAdmin()) {
                        $response->language = $user->getParam('admin_language');
                    } else {
                        $response->language = $user->getParam('language');
                    }
                    //actuliza el nomero de intentos a cero cuando hay un logeo exitoso
                    $sql = 'UPDATE #__users SET intentos = 0 WHERE username=' . $db->Quote($credentials["username"]).'';
                    $db->setQuery($sql);
                    $db->query();
                        //Inserta registro del usuario que se loguea con fecha y hora
                        $sql = "INSERT INTO #__logs (id_user,ingreso)VALUES(".$result->id.",NOW())"; 
                        $db->setQuery($sql);
                        $db->query();
                    $response->status = JAuthentication::STATUS_SUCCESS;
                    $response->error_message = '';
                    $sesion->clear('authenticate');
                    /* para enviar mensaje de usuario inactivo */
                } else {
                    
                    if($result->intentos == 4){
                        $sql = 'UPDATE #__users SET block = 1,intentos = 0 WHERE username=' . $db->Quote($credentials["username"]).''; 
                        $db->setQuery($sql);
                        $db->query();
                        $message = "Su cuenta ha sido bloqueada, por superar el número de intentos fallidos";
                    }else{
                        $sql = 'UPDATE #__users SET intentos = (intentos + 1) WHERE username=' . $db->Quote($credentials["username"]).'';
                        $db->setQuery($sql);
                        $db->query();
                        $message = 'Recuerde que despues de 5 intentos fallidos su cuenta será Bloqueada.<br /> Intentos Fallidos: '.($result->intentos + 1).'';
                        //$mensage .= (""); 
                    }
                    
                    $response->status = JAuthentication::STATUS_FAILURE;
                   // echo JText::_('JGLOBAL_AUTH_INVALID_PASS'); exit;
                    $response->error_message = JText::_('JGLOBAL_AUTH_INVALID_PASS'); 
                    $sesion->set('authenticate', 'invalid');
                    $sesion->set('nopass',$message);
                }
            }
        } else {
            $response->status = JAuthentication::STATUS_FAILURE;
           // echo JText::_('JGLOBAL_AUTH_NO_USER');exit;
            $response->error_message = JText::_('JGLOBAL_AUTH_NO_USER');
            $sesion->set('authenticate', 'invalid');
        }
    }

}
