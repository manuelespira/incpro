<?php
/**
 * @version     1.0.0
 * @package     com_aganar
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Jhon Rengifo <jrengifo@grupo-inc.com> - http://
 */
 
// No direct access
defined('_JEXEC') or die;
define("CARGOSCOMERCIALES","4,7");
/**
 * Aganar helper. 
 */
class JHTMLaganar 
{
	/**
	 * Configure the Linkbar.
	 */
	public static function addSubmenu($vName = ''){	}
        
    function areas_inc($name = 'areas_inc', $activo = 0, $javascript = '', $size = 1, $clase = 'inputbox'){
         
        $dbo = JFactory::getDBO();
        $user = JFactory::getUser();
        //$ejecutivo = JRequest::getVar('ejecutivos');
        $lista = array();
        $elem = new JObject();
        $elem->id = 0;
        $elem->nombre = 'Seleccione...';
        $lista[] = $elem;
       
        $query = "SELECT id,nombre 
                    FROM jfb_callcenter_areas_inc"; 
        $dbo->setQuery($query);
        $periodo = $dbo->loadObjectList();
        $i = 0;
        while ($i < count($periodo)) {
            $elem = new JObject();
            $elem->id = $periodo[$i]->id;
            $elem->nombre = $periodo[$i]->nombre;  
            $lista[] = $elem;
            $i++;
        }
	 
        return JHTML::_('select.genericlist', $lista, $name, 'class="' . $clase . '" size="' . $size . '" ' . $javascript, 'id', 'nombre', intval($activo));
        //return $salida;
    }
    
     function tipificacasocall($name = 'tp_caso', $activo = 0, $javascript = '', $size = 1, $clase = 'inputbox'){
         
        $dbo = JFactory::getDBO();
        $user = JFactory::getUser();
        //$ejecutivo = JRequest::getVar('ejecutivos');
        $lista = array();
        $elem = new JObject();
        $elem->id = 0;
        $elem->nombre = 'Seleccione...' ;
        $lista[] = $elem;
       
        $query = "SELECT id,nombre as nombre 
                    FROM jfb_callcenter_tp_caso"; 
        $dbo->setQuery($query);
        $periodo = $dbo->loadObjectList();
        $i = 0;
        while ($i < count($periodo)) {
            $elem = new JObject();
            $elem->id = $periodo[$i]->id;
            $elem->nombre = $periodo[$i]->nombre;
            $lista[] = $elem;
            $i++;
        }
	 
        return JHTML::_('select.genericlist', $lista, $name, 'class="' . $clase . '" size="' . $size . '" ' . $javascript, 'id', 'nombre', intval($activo));
        //return $salida;
    }
        
        
    function acciones_call($name = 'acc_call', $activo = 0, $javascript = '', $size = 1, $clase = 'inputbox'){
            $dbo = JFactory::getDBO();
            $lista = array();
            $elem = new JObject();
            $elem->id = 0;
            $elem->nombre = 'Seleccione';
            $lista[] = $elem;
            //echo 
            $query = "SELECT  id,descripcion 
                      FROM #__callcenter_acciones";
            //$query .= " ORDER BY 1 ASC";
            $dbo->setQuery($query);
            $elements = $dbo->loadObjectList();
            //print_r($elements);
          
            $i = 0;
            while($i < count($elements)){
              $elem = new JObject();
              $elem->id = $elements[$i]->id;
              $elem->nombre = $elements[$i]->descripcion;
              $lista[] = $elem;
              $i++;
            }

            return JHTML::_('select.genericlist', $lista, $name, 'class="'.$clase.'" size="' . $size . '" ' . $javascript,'id', 'nombre',intval($activo) );

            return $salida;
  }
        
        function idtarea($name = 'idtarea', $activo = 0, $javascript = '', $size = 1, $clase = 'inputbox'){
            $dbo = JFactory::getDBO();
            $lista = array();
            $elem = new JObject();
            $elem->id = 0;
            $elem->nombre = 'Seleccione';
            $lista[] = $elem;
            $query = "SELECT  *
                      FROM #__callcenter_semaforo";
            $query .= " ORDER BY nombre ASC";
            $dbo->setQuery($query);
            $productos = $dbo->loadObjectList();
            $i = 0;
            while($i < count($productos)){
              $elem = new JObject();
              $elem->id = $productos[$i]->id;
              $elem->nombre = $productos[$i]->nombre;
              $lista[] = $elem;
              $i++;
            }

            return JHTML::_('select.genericlist', $lista, $name, 'class="'.$clase.'" size="' . $size . '" ' . $javascript,'id', 'nombre',intval($activo) );

            return $salida;
  }
        
        
        
        
        function estadoreden($name = 'estadoreden', $activo = 0, $javascript = '', $size = 1, $clase ="inputbox"){
            $user = JFactory::getUser();
            $dbo = JFactory::getDBO();
            $vista = JRequest::getVar("view");           
            $lista = array(); 
            $elem = new JObject();
            $elem->id = "Z";
            $elem->nombre = "Todos...";
            $lista[] = $elem;
            $elem = new JObject();
            $elem->id = "C";
            $elem->nombre = "Pendiente";
            $lista[] = $elem;
            $elem = new JObject();
            $elem->id = "U";
            $elem->nombre = "Autorizado";
            $lista[] = $elem;
            $elem = new JObject();
            $elem->id = "X";
            $elem->nombre = "Rechazado";
            $lista[] = $elem;
            
            return JHTML::_('select.genericlist', $lista, $name, 'class="'.$clase.'" size="' . $size . '" ' . $javascript,'id', 'nombre',$activo );
       }
        
    function user_clientes2 ($name = 'user_clientes2', $activo = 0, $javascript = '', $size = 1, $clase ="inputbox"){
            $dbo = JFactory::getDBO();
            $comercial = JRequest::getVar("comerciales"); 
            $lista = array(); 
            $elem = new JObject();
            $elem->id = 0;
            $elem->nombre = "Seleccione...";
            $lista[] = $elem;
            if ($comercial > 0){
                $query = "SELECT a.id,upper(a.nombre) AS nombre
                                FROM jfb_user_cliente AS b INNER JOIN jfb_clientes as a ON a.id=b.cliente
                                WHERE user = ".$comercial."
                                ORDER BY nombre ASC" ;
                $dbo->setQuery($query);
                $comerciales = $dbo->loadObjectList();
                $i = 0;
                while($i < count($comerciales)){
                   $pull_value_short =  substr($comerciales[$i]->nombre, 0, 40);
                    if($pull_value_short == $comerciales[$i]->nombre) :
                        $nombrecliente = $comerciales[$i]->nombre;
                    else:
                        $nombrecliente = $pull_value_short."[...]";
                    endif; 
                    
                  $elem = new JObject();
                  $elem->id = $comerciales[$i]->id;
                  $elem->nombre = $nombrecliente;
                  $lista[] = $elem;
                  $i++;
                }
            }
            return JHTML::_('select.genericlist', $lista, $name, 'class="'.$clase.'" size="' . $size . '" ' . $javascript,'id', 'nombre',intval($activo) );
       }
        
        
         function comerciales($name = 'comerciales', $activo = 0, $javascript = '', $size = 1, $clase ="inputbox"){
            $user = JFactory::getUser();
            $dbo = JFactory::getDBO();
            $vista = JRequest::getVar("view");           
            $lista = array(); 
            $elem = new JObject();
            $elem->id = 0;
            $elem->nombre = "Todos..."; 
            $lista[] = $elem;
            
            $query = "SELECT id_user AS id, CONCAT(nombres,' ',apellidos) AS nombre 
                        FROM jfb_participantes 
                        WHERE cargo IN (".CARGOSCOMERCIALES.") AND id_user != 344
                        ORDER BY nombre ASC";
            $dbo->setQuery($query);
            $comerciales = $dbo->loadObjectList();
            $i = 0;
            while($i < count($comerciales)){ 
                
                 $pull_value_short =  substr($comerciales[$i]->nombre, 0, 30);
                    if($pull_value_short == $comerciales[$i]->nombre) :
                        $nombrecliente = $comerciales[$i]->nombre;
                    else:
                        $nombrecliente = $pull_value_short."[...]";
                    endif; 
                
              $elem = new JObject();
              $elem->id = $comerciales[$i]->id;
              $elem->nombre = $nombrecliente;
              $lista[] = $elem;
              $i++;
            }
            return JHTML::_('select.genericlist', $lista, $name, 'class="'.$clase.'" size="' . $size . '" ' . $javascript,'id', 'nombre',intval($activo) );
       }
        
        
        function user_clientes ($name = 'user_clientes', $activo = 0, $javascript = '', $size = 1, $clase ="inputbox"){
                
            $dbo = JFactory::getDBO();
            $comercial = JRequest::getVar("comercialesregional");
            $lista = array(); 
            $elem = new JObject();
            $elem->id = 0;
            $elem->nombre = "Seleccione...";
            $lista[] = $elem;
            if ($comercial > 0){
                $query = "SELECT a.id,upper(a.nombre) AS nombre
                                FROM jfb_user_cliente AS b INNER JOIN jfb_clientes as a ON a.id=b.cliente
                                WHERE user = ".$comercial."
                                ORDER BY nombre ASC" ;
                $dbo->setQuery($query);
                $comerciales = $dbo->loadObjectList();
                $i = 0;
                while($i < count($comerciales)){
                   $pull_value_short =  substr($comerciales[$i]->nombre, 0, 40);
                    if($pull_value_short == $comerciales[$i]->nombre) :
                        $nombrecliente = $comerciales[$i]->nombre;
                    else:
                        $nombrecliente = $pull_value_short."[...]";
                    endif; 
                    
                  $elem = new JObject();
                  $elem->id = $comerciales[$i]->id;
                  $elem->nombre = $nombrecliente;
                  $lista[] = $elem;
                  $i++;
                }
            }
            return JHTML::_('select.genericlist', $lista, $name, 'class="'.$clase.'" size="' . $size . '" ' . $javascript,'id', 'nombre',intval($activo) );
       }

       
       function comercialesregional ($name = 'comercialesregional', $activo = 0, $javascript = '', $size = 1, $clase ="inputbox"){
            $user = JFactory::getUser();
            $dbo = JFactory::getDBO();
            $vista = JRequest::getVar("view");           
            $lista = array(); 
            $elem = new JObject();
            $elem->id = 0;
            $elem->nombre = "Seleccione...";
            $lista[] = $elem;
            $query = "SELECT id 
                        FROM jfb_regionales 
                        WHERE id_user_director = ".$user->id;
            $dbo->setQuery($query);
            $id_regional = $dbo->loadResult();
            $query = "SELECT id_user AS id, CONCAT(nombres,' ',apellidos) AS nombre 
                        FROM jfb_participantes 
                        WHERE cargo IN (".CARGOSCOMERCIALES.")AND regional = ".$id_regional."
                        ORDER BY nombre ASC";
            $dbo->setQuery($query);
            $comerciales = $dbo->loadObjectList();
            $i = 0;
            while($i < count($comerciales)){ 
                
                 $pull_value_short =  substr($comerciales[$i]->nombre, 0, 30);
                    if($pull_value_short == $comerciales[$i]->nombre) :
                        $nombrecliente = $comerciales[$i]->nombre;
                    else:
                        $nombrecliente = $pull_value_short."[...]";
                    endif; 
                
              $elem = new JObject();
              $elem->id = $comerciales[$i]->id;
              $elem->nombre = $nombrecliente;
              $lista[] = $elem;
              $i++;
            }
            return JHTML::_('select.genericlist', $lista, $name, 'class="'.$clase.'" size="' . $size . '" ' . $javascript,'id', 'nombre',intval($activo) );
       }
        
        function regional($name = 'regionales', $activo = 0, $javascript = '', $size = 1, $clase ="inputbox"){
            $user = JFactory::getUser();
            $dbo = JFactory::getDBO();
            $vista = JRequest::getVar("view");           
            $lista = array(); 
            $elem = new JObject();
            $elem->id = 0;
            $elem->nombre = "seleccione...";
            $lista[] = $elem;
            $query = "SELECT id, descripcion as nombre
                        FROM #__regionales 
                        WHERE id > 0";
            $dbo->setQuery($query);
            $periodos = $dbo->loadObjectList();
            $i = 0;
            while($i < count($periodos)){
              $elem = new JObject();
              $elem->id = $periodos[$i]->id;
              $elem->nombre = $periodos[$i]->nombre;
              $lista[] = $elem;
              $i++;
            }
            return JHTML::_('select.genericlist', $lista, $name, 'class="'.$clase.'" size="' . $size . '" ' . $javascript,'id', 'nombre',intval($activo) );
       }
       function regionales($name = 'regionales', $activo = 0, $javascript = '', $size = 1, $clase ="inputbox"){
            $user = JFactory::getUser();
            $dbo = JFactory::getDBO();
            $vista = JRequest::getVar("view");           
            $lista = array(); 
            $elem = new JObject();
            $elem->id = 0;
            $elem->nombre = "Todos...";
            $lista[] = $elem;
            $query = "SELECT id, descripcion as nombre
                        FROM #__regionales 
                        WHERE id > 0";
            $dbo->setQuery($query);
            $periodos = $dbo->loadObjectList();
            $i = 0;
            while($i < count($periodos)){
              $elem = new JObject();
              $elem->id = $periodos[$i]->id;
              $elem->nombre = $periodos[$i]->nombre;
              $lista[] = $elem;
              $i++;
            }
            return JHTML::_('select.genericlist', $lista, $name, 'class="'.$clase.'" size="' . $size . '" ' . $javascript,'id', 'nombre',intval($activo) );
       }
         function segmentos($name = 'segmentos', $activo = 0, $javascript = '', $size = 1, $clase ="inputbox"){
             
            $user = JFactory::getUser();
            $dbo = JFactory::getDBO();
            $vista = JRequest::getVar("view");           
            $lista = array(); 
            $elem = new JObject();
            $elem->id = 0;
            $elem->nombre = "Seleccione...";
            $lista[] = $elem;
            $query = "SELECT id, descripcion as nombre
                        FROM #__segmentos
                        WHERE id > 0";
            $dbo->setQuery($query);
            $periodos = $dbo->loadObjectList();
            $i = 0;
            while($i < count($periodos)){
              $elem = new JObject();
              $elem->id = $periodos[$i]->id;
              $elem->nombre = $periodos[$i]->nombre;
              $lista[] = $elem;
              $i++;
            }
            return JHTML::_('select.genericlist', $lista, $name, 'class="'.$clase.'" size="' . $size . '" ' . $javascript,'id', 'nombre',intval($activo) );
       }
        
           function periodoReport($name = 'periodoReport', $activo = 0, $javascript = '', $size = 1, $clase ="inputbox"){
            $user = JFactory::getUser();
            $dbo = JFactory::getDBO();
            $vista = JRequest::getVar("view");           
            $lista = array();
            $query = "SELECT id,nombre
                      FROM  #__periodos  
                      WHERE id IN (SELECT DISTINCT periodo
                                      FROM  `jfb_cuotas`)
                      ORDER   BY id ASC";
            $dbo->setQuery($query);
            $periodos = $dbo->loadObjectList();
            $i = 0;
            while($i < count($periodos)){
              $elem = new JObject();
              $elem->id = $periodos[$i]->id;
              $elem->nombre = $periodos[$i]->nombre;
              $lista[] = $elem;
              $i++;
            }
            return JHTML::_('select.genericlist', $lista, $name, 'class="'.$clase.'" size="' . $size . '" ' . $javascript,'id', 'nombre',intval($activo) );
       }
       
       
       function periodos_liq($name = 'periodoReport', $activo = 0, $javascript = '', $size = 1, $clase ="inputbox"){
            $user = JFactory::getUser();
            $dbo = JFactory::getDBO();
            $vista = JRequest::getVar("view");           
//            $lista = array();
//            $elem = new JObject();
//            $elem->id = 0;
//            $elem->nombre = "Seleccione...";
//            $lista[] = $elem;
            $query = "SELECT id,nombre
                      FROM  #__periodos  
                      WHERE id IN (SELECT DISTINCT periodo
                                      FROM  `jfb_liquidaciones`)
                      ORDER   BY id ASC";
            $dbo->setQuery($query);
            $periodos = $dbo->loadObjectList();
            $i = 0;
            while($i < count($periodos)){
              $elem = new JObject();
              $elem->id = $periodos[$i]->id;
              $elem->nombre = $periodos[$i]->nombre;
              $lista[] = $elem;
              $i++; 
            }
            return JHTML::_('select.genericlist', $lista, $name, 'class="'.$clase.'" size="' . $size . '" ' . $javascript,'id', 'nombre',intval($activo) );
       }
       
       function periodos_liq2($name = 'periodoReport', $activo = 0, $javascript = '', $size = 1, $clase ="inputbox"){
            $user = JFactory::getUser();
            $dbo = JFactory::getDBO();
            $vista = JRequest::getVar("view");           
            $lista = array();
            $elem = new JObject();
            $elem->id = 0;
            $elem->nombre = "seleccione...";
            $lista[] = $elem;
            $query = "SELECT id,nombre
                      FROM  #__periodos  
                      WHERE id IN (SELECT DISTINCT periodo
                                      FROM  `jfb_liquidaciones`)
                      ORDER   BY id DESC";
            $dbo->setQuery($query);
            $periodos = $dbo->loadObjectList();
            $i = 0;
            while($i < count($periodos)){
              $elem = new JObject();
              $elem->id = $periodos[$i]->id;
              $elem->nombre = $periodos[$i]->nombre;
              $lista[] = $elem;
              $i++; 
            }
            return JHTML::_('select.genericlist', $lista, $name, 'class="'.$clase.'" size="' . $size . '" ' . $javascript,'id', 'nombre',intval($activo) );
       }
       
       
       function opcion($name = 'opcion', $activo = 0, $javascript = '', $size = 1, $clase ="inputbox"){ 
           
           $user = JFactory::getUser();
           $lista = array();
           $elem = new JObject();
           $elem->id = 0;
           $elem->nombre = "Seleccione...";
           $lista[] = $elem;
            
           $elem = new JObject();
           $elem->id = 1;
           $elem->nombre = "Participantes";
           $lista[] = $elem;
           if($user->id_cargo != "2"){
                $elem = new JObject();
                $elem->id = 2;
                $elem->nombre = "Regionales";
                $lista[] = $elem;
            }
           $elem = new JObject();
           $elem->id = 3;
           $elem->nombre = "Segmentos";
           $lista[] = $elem;
            
            return JHTML::_('select.genericlist', $lista, $name, 'class="'.$clase.'" size="' . $size . '" ' . $javascript,'id', 'nombre',intval($activo) );
       }
       
        
      function variables($name = 'variables', $activo = 0, $javascript = '', $size = 1, $clase ="inputbox"){
            
                $user = JFactory::getUser();
                $dbo = JFactory::getDBO();
                $vista = JRequest::getVar("view");             
                $lista = array();
                $elem = new JObject();
                $elem->id = 0;
                $elem->nombre = "Seleccione...";
                $lista[] = $elem;
                $elem = new JObject();
                $elem->id = 3;
                $elem->nombre = "Otras Variables";
                $lista[] = $elem; 
                $query = "SELECT  distinct id,descripcion
                          FROM  #__variables
                          WHERE id IN(1,2,6) 
                          ORDER   BY id ASC";
                $dbo->setQuery($query);
                $periodos = $dbo->loadObjectList();
                $i = 0;
                while($i < count($periodos)){
                  $elem = new JObject();
                  $elem->id = $periodos[$i]->id;
                  $elem->nombre = $periodos[$i]->descripcion;
                  $lista[] = $elem;
                  $i++;
                }
                return JHTML::_('select.genericlist', $lista, $name, 'class="'.$clase.'" size="' . $size . '" ' . $javascript,'id', 'nombre',intval($activo) );
       }
       
       
       function variables_tabla($name = 'variables', $activo = 0, $javascript = '', $size = 1, $clase ="inputbox"){
            $user = JFactory::getUser();
                $dbo = JFactory::getDBO();
                $vista = JRequest::getVar("view");             
                $lista = array();
                $elem = new JObject();
                $elem->id = 0;
                $elem->nombre = "Seleccione...";
                $lista[] = $elem;
//                $elem = new JObject();
//                $elem->id = 3;
//                $elem->nombre = "Otro";
//                $lista[] = $elem; 
                $query = "SELECT  distinct id,descripcion
                          FROM  #__variables
                          ORDER   BY id DESC";
                $dbo->setQuery($query);
                $periodos = $dbo->loadObjectList();
                $i = 0;
                while($i < count($periodos)){
                  $elem = new JObject();
                  $elem->id = $periodos[$i]->id;
                  $elem->nombre = $periodos[$i]->descripcion;
                  $lista[] = $elem;
                  $i++;
                }
                return JHTML::_('select.genericlist', $lista, $name, 'class="'.$clase.'" size="' . $size . '" ' . $javascript,'id', 'nombre',intval($activo) );
       }
        
        function periLiquidar($name = 'periLiquidar', $activo = 0, $javascript = '', $size = 1, $clase ="inputbox"){
            
            $user = JFactory::getUser();
            $dbo = JFactory::getDBO();
            $vista = JRequest::getVar("view");           
            $lista = array();
            $elem = new JObject();
            $elem->id = 0;
            $elem->nombre = "Seleccione...";
            $lista[] = $elem;
            $query = "SELECT  distinct id,nombre
                      FROM  #__periodos  
                      WHERE id in (SELECT distinct periodo FROM #__cuotas WHERE liquidacion = 0)
                      ORDER   BY id ASC";
            $dbo->setQuery($query);
            $periodos = $dbo->loadObjectList();
            $i = 0;
            while($i < count($periodos)){
              $elem = new JObject();
              $elem->id = $periodos[$i]->id;
              $elem->nombre = $periodos[$i]->nombre;
              $lista[] = $elem;
              $i++;
            }
            return JHTML::_('select.genericlist', $lista, $name, 'class="'.$clase.'" size="' . $size . '" ' . $javascript,'id', 'nombre',intval($activo) );
        }
        
        function periodos($name = 'periodos', $activo = 0, $javascript = '', $size = 1, $clase ="inputbox"){
            
            $user = JFactory::getUser();
            $dbo = JFactory::getDBO();
            $vista = JRequest::getVar("view");           
            $sesion = JFactory::getSession();
            $idpart = $sesion->get('idpart');
                if (isset($idpart) || $idpart != '' ){
                    $userBuscado = $idpart;
                }else{
                    $userBuscado = $user->id;
                }
            $lista = array();
            $query = "SELECT  distinct per.id,per.nombre
                      FROM    #__cuotas cuo INNER JOIN #__periodos per ON per.id = cuo.periodo 
                      WHERE   cuo.participante = ".$userBuscado."
                      ORDER   BY per.id DESC";;
            $dbo->setQuery($query);
            $periodos = $dbo->loadObjectList();
            $i = 0;
            while($i < count($periodos)){
              $elem = new JObject();
              $elem->id = $periodos[$i]->id;
              $elem->nombre = $periodos[$i]->nombre;
              $lista[] = $elem;
              $i++;
            }
            return JHTML::_('select.genericlist', $lista, $name, 'class="'.$clase.'" size="' . $size . '" ' . $javascript,'id', 'nombre',intval($activo) );
       }
        
	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @return	JObject
	 * @since	1.6 
	 */
	public static function getActions()
	{
		$user	= JFactory::getUser();
		$result	= new JObject;

		$assetName = 'com_aganar';

		$actions = array(
			'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.own', 'core.edit.state', 'core.delete'
		);

		foreach ($actions as $action) {
			$result->set($action, $user->authorise($action, $assetName));
		}

		return $result;
	}
}
