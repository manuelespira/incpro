<?php
/**
 * Joomla! 1.5 component brinsamania
 *
 * @version $Id: brinsamania.php 2013-02-08 01:06:17 svn $
 * @author Arley Romero
 * @package Joomla
 * @subpackage brinsamania
 * @license GNU/GPL
 *
 * componente para controlar el programa Brinsamania 2013
 *
 * This component file was created using the Joomla Component Creator by Not Web Design
 * http://www.notwebdesign.com/joomla_component_creator/
 *
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

// Include library dependencies
jimport('joomla.filter.input');

/**
* Table class
*
* @package          Joomla
* @subpackage		brinsamania
*/
class TableItem extends JTable {

	/**
	 * Primary Key
	 *
	 * @var int
	 */
	var $id = null;


    /**
	 * Constructor
	 *
	 * @param object Database connector object
	 * @since 1.0
	 */
	function __construct(& $db) {
		parent::__construct('#__aganar', 'id', $db);
                parent::__construct('jfb_users', 'id', $db);
	}

	/**
	 * Overloaded check method to ensure data integrity
	 *
	 * @access public
	 * @return boolean True on success
	 */
	function check() {
		return true;
	}

}
?>