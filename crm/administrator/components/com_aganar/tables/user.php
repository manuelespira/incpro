<?php
/**
 * Joomla! 1.5 component brinsamania
 *
 * @version $Id: brinsamania.php 2013-02-08 01:06:17 svn $
 * @author Arley Romero
 * @package Joomla
 * @subpackage brinsamania
 * @license GNU/GPL 
 *
 * componente para controlar el programa Brinsamania 2013
 *
 * This component file was created using the Joomla Component Creator by Not Web Design
 * http://www.notwebdesign.com/joomla_component_creator/
 *
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

// Include library dependencies
jimport('joomla.filter.input');

/**
* Table class
*
* @package          Joomla
* @subpackage		brinsamania
*/
class Tableuser extends JTable {

        var $id = null;                 //Primary Key  - @var int(11)
        
        /**
	 * Constructor
	 * @param object Database connector object
	 * @since 1.0
	 */
	function __construct(& $db) {
		parent::__construct('jos_users', 'id', $db);
                
	}

	/**
	 * Overloaded check method to ensure data integrity
	 *
	 * @access public
	 * @return boolean True on success
	 */
    function check() {
     
        $db = & JFactory::getDBO();

        $query = "SELECT  count(*)
                FROM    jos_users " ;

        //echo $query; exit();
        $db->setQuery($query);
        $conteo = intval($db->loadResult());
        if ($conteo > 0) {
            $this->setError(JText::_('MSJ_ERRORUSUARIO'));
            return false;
        }
        return true;
    }
  
}
?>


