<?php 
/**
 * @version $Id: view.html.php 875 2012-07-01 18:01:03Z kaktuspalme $
 * @package    aganar
 * @link http://www.kramgroup.com
 * @license    GNU/GPL
 *
 * Administraci&oacute;n aganar 
 *  Limitada, Colombia
 * @license    GNU/GPL
 * Project Page: http://www.kramgroup.com
 */

// Disallow direct access to this file
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.model');

/**
 * @package Joomla 
 * @subpackage Inc Group Socios
 * @since 1.0
 */
class aganarModelUsers extends JModel
{
  var $_pagination = null; 
  var $_id = null;
  var $_data = null;

  /**
   * Constructor
   **/
  function __construct()
  {
    parent::__construct();
    $mainframe = JFactory::getApplication();
    global $option;

    $limit    = $mainframe->getUserStateFromRequest( $option.'.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
    $limitstart = $mainframe->getUserStateFromRequest( $option.'.limitstart', 'limitstart', 0, 'int' );

    $this->setState('limit', $limit);
    $this->setState('limitstart', $limitstart);

    $array = JRequest::getVar('cid',  0, '', 'array');
    $this->setId((int)$array[0]);

  }

  /**
   * Method to set the identifier
   **/
  function setId($id)
  {
    // Set id and wipe data
    $this->_id   = $id;
    $this->_data = null;
  }

  /**
   * Method to get data
   **/
  function getData()
  {
    $mainframe = JFactory::getApplication();
    
    // Lets load the files if it doesn't already exist
    if (empty($this->_data))
    {
      $query = $this->_buildQuery();
      $limit    = $mainframe->getUserStateFromRequest( $option.'.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
      $limitstart = $mainframe->getUserStateFromRequest( $option.'.limitstart', 'limitstart', 0, 'int' );
      $this->_data = $this->_getList($query, $limitstart, $limit);
    }
     
    return $this->_data;
  }

  /**
   * Method to get the total
   **/
  function getTotal()
  {
    // Lets load the files if it doesn't already exist
    if (empty($this->_total))
    {
      $query = $this->_buildQuery();
      $this->_total = $this->_getListCount($query);
    }
    return $this->_total;
  }

  /**
   * Method to get a pagination object
   **/
  function getPagination()
  {
    $mainframe = JFactory::getApplication();
    
    // Lets load the files if it doesn't already exist
    if (empty($this->_pagination))
    {
      jimport('joomla.html.pagination');
      $limit    = $mainframe->getUserStateFromRequest( $option.'.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
      $limitstart = $mainframe->getUserStateFromRequest( $option.'.limitstart', 'limitstart', 0, 'int' );

      $this->_pagination = new JPagination( $this->getTotal(), $limitstart, $limit );
    }
    return $this->_pagination;
  }

  /**
   * Method to build the query
   **/
  function _buildQuery()
  {
    // Get the WHERE, and ORDER BY clauses for the query
    $where    = $this->_buildContentWhere();
    $orderby  = $this->_buildContentOrderBy();

    $query = '
        SELECT  *
        FROM jos_users U '. $orderby;
    return $query;
  }
  
    function _buildQuery_users($where)  {
    $query = '
        SELECT  *
        FROM jos_users U '. $orderby;
       ;//ojo dejar espacio antes del where
     // echo ''.$query;
       $this->_db->setQuery( $query );
       $detail = $this->_db->loadObjectList();
       return $detail;
    
  }


  /**
   * Method to build the orderby clause of the query
   **/
  function _buildContentOrderBy()
  {
    $mainframe = JFactory::getApplication();
    global $option;

    $filter_order    = $mainframe->getUserStateFromRequest( $option.'.componente.filter_order',   'filter_order',   'U.id', 'cmd' );
    $filter_order_Dir  = $mainframe->getUserStateFromRequest( $option.'.componente.filter_order_Dir',  'filter_order_Dir',  '', 'word' );

    //Some stupid error this fixes it
    if($filter_order == 'U.id DESC') $filter_order = 'U.id DESC';

    $orderby   = ' ORDER BY '.$filter_order.' '.$filter_order_Dir;

    return $orderby;
  }

  /** FILTROS
   * Method to build the where clause of the query
   **/
  function _buildContentWhere()
  {
    $mainframe = JFactory::getApplication();
    global $option;

    $component = JComponentHelper::getComponent( 'com_aganar' ); $params = new JParameter( $component->params );
    $search       = $mainframe->getUserStateFromRequest( $option.'componente.search',       'search',     '', 'string' );
    $search       = $this->_db->getEscaped( trim(JString::strtolower( $search ) ) );
    $name       = $this->_db->getEscaped( trim(strtolower( strtolower(JRequest::getVar("name") ) ) ) );
 
    $where = array();
    
    /*$name = trim(JRequest::getVar("name"));
    if ($name!="") {
      $where[] = " LOWER(U.name) LIKE '%".strtolower($name)."%'";
    }
    
    $id = intval(JRequest::getVar("id"));
    if ($id>0) {
      $where[] = ' U.id = '.$id;
    }
    
    $pdv = trim(JRequest::getVar("pdv"));
    if ($pdv!="") {
      $where[] = " LOWER(U.pdv) LIKE '%".strtolower($pdv)."%'";
    }
    
    $identification = intval(JRequest::getVar("identification"));
    if ($identification>0) {
      $where[] = ' U.identification = '.$identification;
    }
    
    $nit = intval(JRequest::getVar("nit"));
    if ($nit>0) {
      $where[] = ' U.nit = '.$nit;
    }  
    
    $categoria = intval(JRequest::getVar("user_category_id"));
    if ($categoria>0) {
      $where[] = ' U.user_category_id = '.$categoria;
    }
    
    $genero = intval(JRequest::getVar("gender_id"));
    if ($genero>0) {
      $where[] = ' U.gender_id = '.$genero;
    }      
    
    $mercaderista = intval(JRequest::getVar("merchandiser_id"));
    if ($mercaderista>0) {
      $where[] = ' U.merchandiser_id = '.$mercaderista;
    }  
    
    $ciudad = intval(JRequest::getVar("city_id"));
    if ($ciudad>0) {
      $where[] = ' U.city_id = '.$ciudad;   
    }   
    
    $estado = intval(JRequest::getVar("status"));
    if ($estado>0) {
      $where[] = ' U.status = '.$estado;
    } */ 
    
    $where     = ( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );

    return $where;
  }

  /**
   * Method to remove
   **/
  function delete($cids)
  {
    $cids = implode( ',', $cids );

    $query = 'DELETE FROM jos_users'.
            ' WHERE id IN ('. $cids .')';

    $this->_db->setQuery( $query );
    if(!$this->_db->query()) {
      $this->setError($this->_db->getErrorMsg());
      return false;
    }

    $total   = count( $cid );
    $msg   = $total.' '.JText::_('registro eliminado');
    return $msg;
  }

  function publicado($cid, $publicado = 1, $task = 'publicado')
  {
    JArrayHelper::toInteger($cid);
    $cids = implode(',', $cid);

    $column = 'publicado';
    if($task == 'publicado')
    {
      $column = 'publicado';
    }

    $query = 'UPDATE
                jos_users
              SET '.$column.' = '.(int) $publicado.'
              WHERE
                id IN ('.$cids.')';

    $this->_db->setQuery($query);
    if(!$this->_db->query())
    {
      return false;
    }

    return count($cid);
  }
  
  function getLists()
  {
      $this->lists['order'] = "ordering";
      return $this->lists;
  }
}
?>
