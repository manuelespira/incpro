<?php
/**
 * brinsamania 1.0 - INC Group Bogota - Manejo de socios
 * @package brinsamania 1.0
 * @copyright (C) 2009 Lyften Designs
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link http://www.kramgroup.com/ Sitio oficial
 **/
 
// Disallow direct access to this file
defined('_JEXEC') or die('Restricted access');
$mainframe = JFactory::getApplication();
?>

  <table cellspacing="0" cellpadding="0" border="0" width="100%">
    <tr>
      <td width="55%" valign="top">
        <div id="cpanel">
          <?php //echo $this->addIcon('participantes','participantes', JText::_('PARTICIPANTES'));?>
          <?php echo $this->addIcon('users','users', JText::_('USUARIOS'));?>
            <?php echo $this->addIcon('user','user', JText::_('FORMULARIO USUARIOS'));?>
       

            
            <?php /*echo $this->addIcon('carga_archivo','carga_archivo', JText::_('CARGAR USUARIOS'));?>   
            <?php echo $this->addIcon('carga_archivo_cuota','carga_archivo_cuota', JText::_('CARGAR CUOTAS'));?>   
            <?php echo $this->addIcon('importa_compra','importa_compra', JText::_('IMPORTAR COMPRAS'));*/?> 
            
            
            
          <?php //echo $this->addIcon('socios','socios', JText::_('SOCIOS'));?>
          <?php //echo $this->addIcon('beneficiarios','beneficiarios', JText::_('BENEFICIARIOS'));?>
          <?php //echo $this->addIcon('asesores','asesores', JText::_('ASESORES'));?>
          <?php //echo $this->addIcon('redenciones','redenciones', JText::_('REDENCIONES'));?>

          <?php //echo $this->addIcon('aeropuertos','aeropuertos', JText::_('AEROPUERTOS'));?>
          <?php //echo $this->addIcon('zonas','zonas', JText::_('ZONAS'));?>
          <?php //echo $this->addIcon('productos','productos', JText::_('PRODUCTOS'));?>
          <?php //echo $this->addIcon('campanias','campanias', JText::_('CAMPANIAS'));?>
          <?php //echo $this->addIcon('puntoreglas','puntoreglas', JText::_('PUNTOS'));?>
          <?php //echo $this->addIcon('iatas','iatas', JText::_('IATAS'));?>
          <?php //echo $this->addIcon('subirimagenes','subirimagenes', JText::_('SUBIRFOTOS'));?>
          <?php //echo $this->addIcon('metodos','metodos', JText::_('METODOS'));?>
          <?php //echo $this->addIcon('paises','paises', JText::_('PAISES'));?>
          <?php //echo $this->addIcon('ciudades','ciudades', JText::_('CIUDADES'));?>
          <?php //echo $this->addIcon('oficinas','oficinas', JText::_('OFICINAS'));?>
          <?php //echo $this->addIcon('tipopuntos','tipopuntos', JText::_('TIPOPUNTOS'));?>
          <?php //echo $this->addIcon('constantes','constantes', JText::_('CONSTANTES'));?>
          <?php //echo $this->addIcon('perfiles','perfiles', JText::_('PERFILES'));?>
          <?php //echo $this->addIcon('perfiles','perfiles', JText::_('PERFILES'));?>
          <?php //echo $this->addIcon('campaniasactualizaciondatos','campaniasactualizaciondatos', JText::_('CAMPANIASACTUALIZACION'));?>
        </div>
      </td>
      <td width="40%" valign="top">
        <div id="content-pane" class="pane-sliders">
<?php
// Modulo 
  $document  = &JFactory::getDocument();
  $renderer  = $document->loadRenderer('modules');
  echo $renderer->render("reportesaganar", $options, null);
?>
        </div>
      </td>
    </tr>
  </table>