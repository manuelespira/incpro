<?php
/**
 * @version $Id: view.html.php 875 2012-07-01 18:01:03Z kaktuspalme $
 * @package    aganar
 * @link http://www.kramgroup.com
 * @license    GNU/GPL
 *
 * Administraci&oacute;n aganar 
 *  Limitada, Colombia
 * @license    GNU/GPL
 * Project Page: http://www.kramgroup.com
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view' );

class aganarViewUser extends JView
{

  function display($tpl = null)
  {
        jimport('joomla.html.pane');
        global $mainframe;
       
        $user        =& $this->get('Data');
        $this->assignRef('user', $user);

        $task = JRequest::getVar("task");
        $isNew        = ($user->id < 1);
        $text = $isNew ? JText::_( 'New' ) : JText::_( 'Edit' );
        JToolBarHelper::title(   JText::_( 'Componente aganar' ).': <small><small>[ ' . $text.' ]</small></small>', 'aganar' );
        JToolBarHelper::save();
        if ($isNew) {
          JToolBarHelper::cancel();
        }
        else {
          // for existing items the button is renamed `close`
          JToolBarHelper::cancel( 'cancel', 'Close' );
        }
        $user =& $this->get( 'user');
        $this->assignRef( 'user' , $user);

        $config =& JFactory::getConfig();
        $offset = $config->getValue('config.offset');
        $this->assignRef('isNew', $isNew);
        JToolBarHelper::title( 'Administración aganar'.': <small><small>[ user ]</small></small>','user.png' );
        parent::display($tpl);
  }
}
?>
