<?php
/**
 * @version     1.0.0
 * @package     com_aganar
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Jhon Rengifo <jrengifo@grupo-inc.com> - http://
 */


// no direct access 

defined( '_JEXEC' ) or die( 'Restricted access' );
require_once( JPATH_COMPONENT.DS.'controller.php' );
JHTML::addIncludePath(JPATH_ADMINISTRATOR.DS."components".DS."com_aganar".DS.'helpers');
$controller   = new AganarController( );
$controller->execute( JRequest::getVar( 'task' ) );
$controller->redirect();
