<?php
/** 
 * @version $Id: componente.php 875 2012-07-01 18:01:03Z kaktuspalme $
 * @package    incsocios
 * @link http://www.kramgroup.com
 * @license    GNU/GPL
 *
 * Administraci&oacute;n incsocios 
 *  Limitada, Colombia
 * @license    GNU/GPL
 * Project Page: http://www.kramgroup.com
 */

// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' );

/**
 * incsocios INC Group Bogota Component Controller
 *
 * @package    incsocios
 */
class aganarControlleraprobacitas extends JController 
{
  var $_access = null;

  /**
   * constructor (registers additional tasks to methods)
   * @return void
   */
  function __construct()
  {
      parent::__construct();

      // Register Extra tasks
      $this->registerTask( 'add' , 'edit' );
      $this->registerTask('publicado','publicado');
      $this->registerTask('nopublicado','publicado');
      $this->registerTask( 'saveorder', 'saveorder' );
      $this->registerTask( 'orderup',         'orderup');
      $this->registerTask( 'orderdown',       'orderdown');
      $model = $this->getModel( 'extracto' );
  }

  function edit()
  {
      JRequest::setVar( 'view', 'aprobacitas' );
      JRequest::setVar( 'layout', 'default'  );
      JRequest::setVar('hidemainmenu', 1);
      $model = $this->getModel( 'aprobacitas' );
      parent::display();

  }


  /**
   * save a record (and redirect to main page)
   * @return void
   */
  function save()
  {
    JRequest::checkToken() or jexit( 'Invalid Token' );
    $model = $this->getModel( 'aprobacitas' );
    $component = JComponentHelper::getComponent( 'com_aganar' ); $mycom_params = new JParameter( $component->params );
    $post = $_POST;
    $post['descripcion'] = JRequest::getVar('descripcion', '', 'post', 'string', JREQUEST_ALLOWRAW);
    
    if ($model->store($post)) {
      $msg = JText::_( 'Registro almacenado' );
      $type = 'message';
      $this->setRedirect( 'index.php');
    }
    else {
      $msg = 'Error actualizando el(los) registro(s): <b>!'.$model->getError().'!</b>';
      $type = 'error';
    }
    //$this->setRedirect( 'index.php?option=com_aganar&view=extracto', $msg, $type );
  }

  /**
   * remove record
   * @return void
   */
  function remove()
  {
    JRequest::checkToken() or jexit( 'Invalid Token' );
    //Load model and delete componente - redirect afterwards
    $model = $this->getModel( 'aprobacitas' );
    if (!$model->delete($_POST["cid"])) {
      $msg = JText::_( 'Error: Registro no pudo ser eliminado' );
      $type = 'error';
    } else {
      $msg = JText::_( 'Registro eliminado' );
      $type = 'message';
    }
    $this->setRedirect( JRoute::_( 'index.php?option=com_aganar&view=extracto', false ), $msg, $type );
  }
  
  
  function cancel()
  {
      $msg = JText::_( 'Operaci&oacute;n cancelada' );
      $this->setRedirect( 'index.php?option=com_aganar&view=extracto', $msg, 'notice' );
  }
  function publicado()
  {
    // Initialize aprobacitas
    $cid      = JRequest::getVar('cid', array(), 'post', 'array');
    $task     = JRequest::getCmd('task');
    $publish  = ($task == 'publicado');

    if(empty($cid))
    {
      $this->setRedirect('index.php?option=com_aganar&view=extracto', JText::_('No hay extracto seleccionado'));
      $this->redirect();
    }

    $model = $this->getModel('aprobacitas');
    if($count = $model->publicado($cid, $publish))
    {
      if($count != 1)
      {
        $msg = JText::sprintf($publish ? 'Publicados' : 'No publicados', $count);
      }
      else
      {
        $msg = JText::_($publish ? 'Publicado' : 'No publicado');
      }
      $this->setRedirect('index.php?option=com_aganar&view=extracto', $msg);
    }
    else
    {
      $msg = JText::_('Error sacando home');
      $this->setRedirect('index.php?option=com_aganar&view=extracto', $msg, 'error');
    }
  }
  function orderup()
  {
    $model = $this->getModel('aprobacitas');
    if (!$model->changeorder(-1))
      $msg = JText::_( 'Error: Hay un error cambiando el orden' );

    $this->setRedirect( 'index.php?option=com_aganar&view=extracto', $msg );
  }

  function orderdown()
  {
    $model = $this->getModel('aprobacitas');
    if (!$model->changeorder(1))
      $msg = JText::_( 'Error: Hay un error cambiando el orden' );

    $this->setRedirect( 'index.php?option=com_aganar&view=extracto', $msg );
  }

  function saveorder()
  {
    $model = $this->getModel('aprobacitas');
    if (!$model->saveorder())
      $msg = JText::_( 'Error: Hay un error cambiando el orden' );

    $this->setRedirect( 'index.php?option=com_aganar&view=extracto', $msg );
  }
}
?>
