<?php 

/**
 * Joomla! 1.5 component Aganar
 *
 * @version $Id: Aganar.php 2013-02-08 01:06:17 svn $
 * @author Arley Romero
 * @package Joomla
 * @subpackage Aganar
 * @license GNU/GPL
 *
 * componente para controlar el programa Aganar 2013
 *
 * This component file was created using the Joomla Component Creator by Not Web Design
 * http://www.notwebdesign.com/joomla_component_creator/
 *
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

// Import Joomla! libraries
jimport('joomla.application.component.model');

class AganarModelUser extends JModel {
    var $_pagination = null;
    var $_id = null;
    var $_data = null;
    
    function __construct() {
		parent::__construct();

    $mainframe = JFactory::getApplication();
    global $option;

    $limit    = $mainframe->getUserStateFromRequest( $option.'.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
    $limitstart = $mainframe->getUserStateFromRequest( $option.'.limitstart', 'limitstart', 0, 'int' );

    $this->setState('limit', $limit);
    $this->setState('limitstart', $limitstart);

    $array = JRequest::getVar('cid',  0, '', 'array');
    $this->setId((int)$array[0]);
    }

  /**
   * Method to set the identifier
   **/
    function setId($id)
  {
    // Set id and wipe data
    $this->_id   = $id;
    $this->_data = null;
  }

  /**
   * Method to get data
   **/
  function getData()
  {
    // Load the data
    if (empty( $this->_data )) {
      $query =  " SELECT  *
                  FROM    jos_users                  
                  WHERE   id = ".$this->_id;
      
      $this->_db->setQuery( $query );
      $this->_data = $this->_db->loadObject();
    }
    if (!$this->_data) {
      $this->_data = new stdClass();
      $this->_data->id = 0;
      $this->_data->nombre = null;
    }
    return $this->_data;
  }

  /**
   * Method to get the total
   **/
  function getTotal()
  {
    // Lets load the files if it doesn't already exist
    if (empty($this->_total))
    {
      $query = $this->_buildQuery();
      $this->_total = $this->_getListCount($query);
    }

    return $this->_total;
  }

  /**
   * Method to get a pagination object
   **/
  function getPagination()
  {
    // Lets load the files if it doesn't already exist
    if (empty($this->_pagination))
    {
      jimport('joomla.html.pagination');
      $this->_pagination = new JPagination( $this->getTotal(), $this->getState('limitstart'), $this->getState('limit') );
    }
    return $this->_pagination;
  }

  /**
   * Method to build the query
   **/
  function _buildQuery()
  {
    // Get the WHERE, and ORDER BY clauses for the query
    $where    = $this->_buildContentWhere();
    $orderby  = $this->_buildContentOrderBy();

    $query = 'SELECT U.*'
          . ' FROM jos_users  AS U'
          . $where
          . ' GROUP BY U.id'
          . $orderby
          ;
    
    return $query;
  }

  /**
   * Method to build the orderby clause of the query
   **/
  function _buildContentOrderBy()
  {
    $mainframe = JFactory::getApplication();
    global $option;

    $filter_order    = $mainframe->getUserStateFromRequest( $option.'.asesor.filter_order',   'filter_order',   'U.created', 'cmd' );
    $filter_order_Dir  = $mainframe->getUserStateFromRequest( $option.'.asesor.filter_order_Dir',  'filter_order_Dir',  '', 'word' );

    //Some stupid error this fixes it
    if($filter_order == 'U.id DESC') $filter_order = 'U.id DESC';

    $orderby   = ' ORDER BY '.$filter_order.' '.$filter_order_Dir;

    return $orderby;
  }

  /**
   * Method to build the where clause of the query
   **/
  function _buildContentWhere()
  {
    $mainframe = JFactory::getApplication();
    global $option;

    $component = JComponentHelper::getComponent( 'com_aganar' ); $params = new JParameter( $component->params );
    

    $where = array();
    $name = trim(JRequest::getVar("name"));
    if ($name!="") {
      $where[] = " LOWER(U.name) LIKE '%".strtolower($name)."%'";
    }
    
    $identification = intval(JRequest::getVar("identification"));
    if ($identification>0) {
      $where[] = ' U.identification = '.$identification;
    }
    
    
    $where     = ( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );

    return $where;
  }

  /**
   * Method to remove
   **/
  function delete($cids)
  {
    // No se permite borrar asesores asi que se retorna
    $cids = implode( ',', $cids );

    // Se borran primero los elementos relacionados
    $query = 'DELETE FROM jos_users
              WHERE id IN ('. $cids .')';

			 /* echo $query;
			  exit();*/
    $this->_db->setQuery( $query );
    if(!$this->_db->query()) {
      $this->setError($this->_db->getErrorMsg());
      return false;
    }
    
    $total   = count( $cid );
    $msg   = $total.' '.JText::_('Registro(s) eliminado(s)');
    return $msg;
  }
  
  /**
   * Method to store a record
   *
   * @access  public
   * @return  boolean  True on success
   */
  function store($data)
  {
    $row =& $this->getTable();

    if(!isset($data["id"])){
      $usuario = JFactory::getUser();
      $data["id"] = $usuario->id;
    }
    if (!$row->bind($data)) {
      $this->setError($this->_db->getErrorMsg());
      return false;
    }

    if (!$row->check()) {
      $this->setError($row->getError());
      return false;
    }

    if (!$row->store()) {
      $this->setError( $this->_db->getErrorMsg() );
      return false;
    }

    return true;
  }
}



/* 
       


 */
?>