<?php
/**
 * IncSocios
 * 
 * @package    IncSocios
 * @subpackage Components
 * @link http://dev.joomla.org/component/option,com_jd-wiki/Itemid,31/id,tutorials:components/
 * @license                GNU/GPL
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die();

//require_once("base.php");

/**
 * Info Model
 *
 * @package    IncSocios
 * @subpackage IncGroup
 */
class aganarModelgeneraqueja extends JModel
{

  var $_pagination = null;
  var $_data = null;
  var $_obj = null;

function __construct()
  {
    parent::__construct();
    $mainframe = JFactory::getApplication();
    global $option;

    $limit    = $mainframe->getUserStateFromRequest( $option.'.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
    $limitstart = $mainframe->getUserStateFromRequest( $option.'.limitstart', 'limitstart', 0, 'int' );

    $this->setState('limit', $limit);
    $this->setState('limitstart', $limitstart);
  }
  /**
   * Method to get data
   **/
  function datosParti(){
      
      $sesion = JFactory::getSession();
      $userbuscado = $sesion->get('idpart');
      $db = JFactory::getDbo();
      $sql = "SELECT  P.*,U.registerDate as registro,U.estado as estuser, U.envio_sms,U.envio_email
                FROM #__users U 
                INNER JOIN #__participantes P ON U.id = P.id_user 
                WHERE U.id=" . $userbuscado;
      $db->setQuery($sql);
      return  $db->loadObjectList();
  }
  /* Registra la nueva informacion */
  function registarQueja($telConta,$tp_caso,$areainc,$comentario){
    global $mainframe;
    $session = JFactory::getSession();
    $ticket = $session->get('ticket');
    $user_consultado = $session->get('idpart');
    //ECHO
    $sql = "INSERT INTO jfb_callcenter_queja (ticket,participante,areas_inc,tp_caso,tel_contacto,comentario)
             VALUES(".$ticket.",".$user_consultado.",".$areainc.",".$tp_caso.",'".$telConta."','".$comentario."')";
    //EXIT;
   $this->_db->setquery($sql);
   //$this->_db->query();
   if($this->_db->query()){
       return TRUE;
   }else{
       return FALSE;
   }
  }
}
