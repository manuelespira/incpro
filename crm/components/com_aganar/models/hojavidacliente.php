<?php
// Disallow direct access to this file
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.model');

class aganarModelhojavidacliente extends JModel
{
  var $_pagination = null; 
  var $_id = null;
  var $_data = null;
  /**
   * Constructor
   **/
  function __construct()
  {
    parent::__construct();
    $mainframe = JFactory::getApplication();
    global $option;

    $limit    = $mainframe->getUserStateFromRequest( $option.'.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
    $limitstart = $mainframe->getUserStateFromRequest( $option.'.limitstart', 'limitstart', 0, 'int' );

    $this->setState('limit', $limit);
    $this->setState('limitstart', $limitstart);

    $array = JRequest::getVar('cid',  0, '', 'array');
    $this->setId((int)$array[0]);
     
  }
 
  /**
   * Method to set the identifier
   **/
  function setId($id)
  {
    // Set id and wipe data
    $this->_id   = $id;
    $this->_data = null;
  }

  /**
   * Method to get data
   **/
  function getData()
  {
    $mainframe = JFactory::getApplication();
    // Lets load the files if it doesn't already exist
    if (empty($this->_data))
    {
      $query = $this->_buildQuery();
      //$limit    = $mainframe->getUserStateFromRequest( $option.'.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
      //$limitstart = $mainframe->getUserStateFromRequest( $option.'.limitstart', 'limitstart', 0, 'int' );
      $this->_data = $this->_getList($query);
    }
    return $this->_data;
  }

  /**
   * Method to get the total
   **/
  function getTotal()
  {
    // Lets load the files if it doesn't already exist
    if (empty($this->_total))
    {
      $query = $this->_buildQuery();
      $this->_total = $this->_getListCount($query);
    }
    return $this->_total;
  }

  /**
   * Method to get a pagination object
   **/
  function getPagination()
  {
    $mainframe = JFactory::getApplication();
    
    // Lets load the files if it doesn't already exist
    if (empty($this->_pagination))
    {
      jimport('joomla.html.pagination');
      $limit    = $mainframe->getUserStateFromRequest( $option.'.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
      $limitstart = $mainframe->getUserStateFromRequest( $option.'.limitstart', 'limitstart', 0, 'int' );

      $this->_pagination = new JPagination( $this->getTotal(), $limitstart, $limit );
    }
    return $this->_pagination;
  }

  /**
   * Method to build the query
   **/
  function _buildQuery()
  {
    $db = JFactory::getDbo();
    $sesion = JFactory::getSession();
    $clientebuscado = JRequest::getVar('clientebuscado');
    $band =  JRequest::getVar('band');
    $user = JFactory::getUser();
    //echo $band;//exit;
   
    if(!$clientebuscado){
      $query = 'select id from #__clientes where creadopor = '.$user->id.'  order by nombre
         limit 1';
     $db->setQuery($query);
     $clientebuscado = $db->loadResult();
     $sesion->set('clientebuscado',$clientebuscado);
    }else{
        $sesion->set('clientebuscado',$clientebuscado);
    }
    
    if(isset($band) && $band == 2):
        $sesion->clear('Oculseleclien'); 
        $app = JFactory::getApplication();
        $app->redirect( 'index.php');
    endif;
        
  
    // Get the WHERE, and ORDER BY clauses for the query
    $where    = $this->_buildContentWhere();
    $orderby  = $this->_buildContentOrderBy();

        $query = "SELECT a.*,b.id as idseg , b.descripcion AS segmento, c.id as idsec, c.descripcion AS sector,d.id as idreg, d.descripcion AS regional,e.id as idtpneg, e.descripcion AS tpnego,f.descripcion as tp_id
                FROM #__clientes AS a
                INNER JOIN #__segmentos b ON b.id = a.segmento
                INNER JOIN #__sectores c ON a.sector = c.id
                INNER JOIN #__regionales d ON a.regional = d.id
                INNER JOIN #__tiponegocio e ON a.tiponegocio = e.id
                INNER JOIN #__tipodoc f ON f.id = a.tipo_id
                WHERE a.id = ".$clientebuscado;
         //echo $query;
    
    return $query;
  }

  /**
   * Method to build the orderby clause of the query
   **/
  function _buildContentOrderBy()
  {
    $mainframe = JFactory::getApplication();
    global $option;

    $filter_order    = $mainframe->getUserStateFromRequest( $option.'.componente.filter_order',   'filter_order',   'U.id', 'cmd' );
    $filter_order_Dir  = $mainframe->getUserStateFromRequest( $option.'.componente.filter_order_Dir',  'filter_order_Dir',  '', 'word' );

    //Some stupid error this fixes it
    if($filter_order == 'U.id DESC') $filter_order = 'U.id DESC';

    $orderby   = ' ORDER BY '.$filter_order.' '.$filter_order_Dir;

    return $orderby;
  }

  /** FILTROS
   * Method to build the where clause of the query
   **/
  function _buildContentWhere()
  {
    $mainframe = JFactory::getApplication();
    global $option;

//    $component = JComponentHelper::getComponent( 'com_aganar' ); $params = new JParameter( $component->params );
    $search       = $mainframe->getUserStateFromRequest( $option.'componente.search',       'search',     '', 'string' );
    $search       = $this->_db->getEscaped( trim(JString::strtolower( $search ) ) );
    $name       = $this->_db->getEscaped( trim(strtolower( strtolower(JRequest::getVar("name") ) ) ) );
 
    $where = array();
    
    $where     = ( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );

    return $where;
  }


  function getLists()
  {
      $this->lists['order'] = "ordering";
      return $this->lists;
  }
  
   function getCliente()
  { //obtenemos los clientes asociados a al usuario registrado
      $sesion = JFactory::getSession();
      $user = JFactory::getUser();
      $option = "";
      $x = "";
   
     $query= "SELECT a.id,a.nombre
               FROM  #__clientes AS a INNER JOIN  #__user_cliente AS b ON a.id=b.cliente
               WHERE b.user = ".$user->id."
               ORDER BY nombre ASC";
     $this->_db->setQuery($query);
     $result = $this->_db->loadobjectList();
     
     for ($i=0, $n=count( $result ); $i < $n; $i++) {
         if($sesion->get('clientebuscado') == $result[$i]->id){
             $x='selected';
         } else{
             $x = "";
         }
          $pull_value_short =  substr($result[$i]->nombre, 0, 30);
         if($pull_value_short == $result[$i]->nombre) :
           $nombrecliente = $result[$i]->nombre;
         else:
           $nombrecliente = $pull_value_short."[...]";
         endif;
         $option.="<option value='".$result[$i]->id."'".$x.">".$nombrecliente."</option>";
     }
      return $option;
  } 
    function getBancos()
  { //obtenemos los clientes asociados a al usuario registrado
      $sesion = JFactory::getSession();
      $cliente = $sesion->get('clientebuscado');
     
     $query= "SELECT DISTINCT a.id,a.descripcion AS banco, b.descripcion AS producto
                FROM  `#__cli_ban_pro` AS c
                INNER JOIN #__banco AS a ON c.banco = a.id
                INNER JOIN #__banco_prod AS b ON c.producto = b.id
                WHERE c.cliente =".$cliente." ";
     $this->_db->setQuery($query);
     $result = $this->_db->loadobjectList();
     return $result;
  } 
  
   function getFiduciaria()
  { //obtenemos los clientes asociados a al usuario registrado
      $sesion = JFactory::getSession();
      $cliente = $sesion->get('clientebuscado');
   
     $query= "SELECT DISTINCT a.id,a.descripcion AS fidu, b.descripcion AS producto
                FROM  #__cli_fidu_pro AS c
                INNER JOIN #__fiduciaria AS a ON c.fiduciaria = a.id
                INNER JOIN #__fiduciaria_prod AS b ON c.producto = b.id
                WHERE c.cliente =".$cliente." ";
     $this->_db->setQuery($query);
     $result = $this->_db->loadobjectList();
     return $result;
  } 
  function getComisionista()
  { //obtenemos los clientes asociados a al usuario registrado
      $sesion = JFactory::getSession();
      $cliente = $sesion->get('clientebuscado');
     
     $query= "SELECT DISTINCT a.id,a.descripcion AS comi, b.descripcion AS producto
                FROM  #__cli_comi_pro AS c
                INNER JOIN #__comisionista AS a ON c.comisionista = a.id
                INNER JOIN #__comisionista_prod AS b ON c.producto = b.id
                WHERE c.cliente =".$cliente." ";
     $this->_db->setQuery($query);
     $result = $this->_db->loadobjectList();
     return $result;
  } 
    function getContactos()
  { //obtenemos Contactos asociados a al cliente consultado
      $sesion = JFactory::getSession();
      $cliente = $sesion->get('clientebuscado');
     
      $x = "";
     $query= "SELECT id, nombre, cargo
                FROM #__cliente_contactos
                WHERE cliente =".$cliente." ";
     $this->_db->setQuery($query);
     $result = $this->_db->loadobjectList();
     return $result;
  } 
  
  
 function getSituacionact()
  { //obtenemos Situacionact asociados a al cliente consultado
      $sesion = JFactory::getSession();
      $cliente = $sesion->get('clientebuscado');
      $x = "";
     $query= "SELECT a.ult_visita, d.descripcion AS result_visita,
        b.descripcion as compromiso,
        c.descripcion as producto, 
        a.antiguedad,
        a.saldo_prom_mes,
        a.aport_prom_mes,
        a.n_pagosprom_mes,
        a.fid_adm_tpneg,
        a.recursos_adm,
        a.dir_asig_vic_gest,
        a.ejec_vic_gest,
        a.vence_contrato
        FROM #__cliente_situacion_actual AS a INNER JOIN #__cliente_compromiso AS b ON a.compromiso = b.id
        INNER JOIN #__cliente_produc AS c ON a.producto = c.id
        INNER JOIN #__cliente_result_visit AS d ON a.result_visita = d.id 
        WHERE a.cliente = ".$cliente." ";
     $this->_db->setQuery($query);
     $result = $this->_db->loadobjectList();
     return $result;
     
  }
  function getInformacionFinanciera()
  { //obtenemos toda la info financiera asociados a al cliente consultado
      $sesion = JFactory::getSession();
      $cliente = $sesion->get('clientebuscado');
     $query= "SELECT *
         FROM jfb_cliente_info_finan
        WHERE cliente = ".$cliente." ";
     $this->_db->setQuery($query);
     $result = $this->_db->loadobjectList();
     return $result;
  } 
  
    function getSeguimiento()
  { //obtenemos toda la info seguimiento asociados a al cliente consultado
      $sesion = JFactory::getSession();
      $cliente = $sesion->get('clientebuscado');
      $query= "SELECT fecha_creacion as fecha,descripcion as segui
                FROM jfb_cliente_seguimiento
                WHERE cliente = ".$cliente." ";
     $this->_db->setQuery($query);
     $result = $this->_db->loadobjectList();
     return $result;
  } 
  
     function getNoticia()
  { //obtenemos toda la info seguimiento asociados a al cliente consultado
      $sesion = JFactory::getSession();
      $cliente = $sesion->get('clientebuscado');
      $query= "SELECT fecha_creacion as fecha,descripcion as noti
                FROM jfb_cliente_noticias
                WHERE cliente = ".$cliente." ";
     $this->_db->setQuery($query);
     $result = $this->_db->loadobjectList();
     return $result;
  } 
  
 function getInfoadicional()
  { //obtenemos toda la info seguimiento asociados a al cliente consultado
      $sesion = JFactory::getSession();
      $cliente = $sesion->get('clientebuscado');
      $query= "SELECT * 
                FROM jfb_cliente_inf_adic
                WHERE cliente = ".$cliente." ";
     //echo $query;
     $this->_db->setQuery($query);
     $result = $this->_db->loadobjectList();
     return $result;
  } 
}
?>
