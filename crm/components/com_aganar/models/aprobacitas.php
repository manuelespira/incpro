<?php 
/**
 * @version $Id: view.html.php 875 2012-07-01 18:01:03Z kaktuspalme $
 * @package    aganar
 * @link http://www.kramgroup.com
 * @license    GNU/GPL
 *
 * Administraci&oacute;n aganar 
 *  Limitada, Colombia
 * @license    GNU/GPL
 * Project Page: http://www.kramgroup.com
 */

// Disallow direct access to this file
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.model');

/**
 * @package Joomla 
 * @subpackage Inc Group Socios
 * @since 1.0
 */
class aganarModelaprobacitas extends JModel
{
  
  var $_pagination = null; 
  var $_id = null;
  var $_data = null;
  
  /**
   * Constructor
   **/
  function __construct()
  {
    parent::__construct();
    $mainframe = JFactory::getApplication();
    global $option;
    $limit    = $mainframe->getUserStateFromRequest( $option.'.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
    $limitstart = $mainframe->getUserStateFromRequest( $option.'.limitstart', 'limitstart', 0, 'int' );
    $this->setState('limit', $limit);
    $this->setState('limitstart', $limitstart);
    $array = JRequest::getVar('cid',  0, '', 'array');
    $this->setId((int)$array[0]);
  }

  /**
   * Method to set the identifier
   **/
  function setId($id)
  {
    // Set id and wipe data
    $this->_id   = $id;
    $this->_data = null;
  }

  /**
   * Method to get data
   **/
  function getData()
  {
    $mainframe = JFactory::getApplication();
    // Lets load the files if it doesn't already exist
    if (empty($this->_data))
    {
      $query = $this->_buildQuery();
      //$limit    = $mainframe->getUserStateFromRequest( $option.'.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
      //$limitstart = $mainframe->getUserStateFromRequest( $option.'.limitstart', 'limitstart', 0, 'int' );
      $this->_data = $this->_getList($query);
    }
    //print_r($this->_data);
    return $this->_data;
  }
  /**
   * Method to get the total
   **/
  function getTotal()
  {
    // Lets load the files if it doesn't already exist
    if (empty($this->_total))
    {
      $query = $this->_buildQuery();
      $this->_total = $this->_getListCount($query);
    }
    return $this->_total;
  }
  /**
   * Method to get a pagination object
   **/
  function getPagination()
  {
    $mainframe = JFactory::getApplication();
    // Lets load the files if it doesn't already exist
    if (empty($this->_pagination))
    {
      jimport('joomla.html.pagination');
      $limit    = $mainframe->getUserStateFromRequest( $option.'.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
      $limitstart = $mainframe->getUserStateFromRequest( $option.'.limitstart', 'limitstart', 0, 'int' );

      $this->_pagination = new JPagination( $this->getTotal(), $limitstart, $limit );
    }
    return $this->_pagination;
  }

  /**
   * Method to build the query
   **/
  function _buildQuery()
  {
    $user = JFactory::getUser();
    $sesion = JFactory::getSession();
    $dbo = JFactory::getDbo();
    $where    = $this->_buildContentWhere();
    $orderby  = $this->_buildContentOrderBy();
    $estado = 0;
    if(JRequest::getVar('Autorizar')){
        $task = JRequest::getVar('Autorizar');
        $estado = 2;
        $array = JRequest::getVar('visitas');
    }elseif(JRequest::getVar('Rechazar')){
        $task = JRequest::getVar('Rechazar');
        $estado = 3;
        $array = JRequest::getVar('visitas');
    }
      
    IF($task && isset($estado)&& isset($array)){
       foreach($array as $value){
           $sql = "UPDATE jfb_jevents_vevent SET estado = ".$estado.",fecha_reg_act = NOW(),aprobo_rechazo = '".$user->id.",".$estado."' 
                   WHERE ev_id = ".$value."  ";
           $dbo->setQuery($sql);
           $dbo->query();
       }
    }
      
    $comercial = JRequest::getVar('comercial');
    
    if(isset($comercial)&& $comercial != -1){
        $sesion->set('comercial',$comercial);
        $comerciales =  "AND ev.created_by =".$comercial;
        
    }else{
         $sesion->clear('comercial');
        /*consultamos si el usuario es un director*/
      $query = "SELECT id
                FROM #__regionales
                WHERE id_user_director =".$user->id;
     //echo $query;
      $dbo->setQuery($query);
      $regional = $dbo->loadResult();
    //echo $regional;
      /*listando los comerciales que pertenecen a una regional*/
      if(isset($regional)){
         $query= "SELECT id_user 
                  FROM  jfb_participantes
                  WHERE regional = ".$regional."
                  AND id_user != ".$user->id."";
//echo $query;
        $dbo->setQuery($query);
        $result = $dbo->loadResultArray();
        $regional_comerciales = implode(",",$result);
        $comerciales = "AND ev.created_by in (".$regional_comerciales.")";
        }
    }
    $desde = JRequest::getVar('desde');
    if($desde)
         $sesion->set('desde',$desde);
    
    $hasta = JRequest::getVar('hasta');
    if($hasta)
         $sesion->set('hasta',$hasta);
    
    //echo 
    $query = "SELECT det.extra_info AS obser,ev.estado AS est,ev.created_by,ev.ev_id, cli.nombre AS nomcliente, rpt.startrepeat AS inicio, rpt.endrepeat AS fin, rr.byday, det.description, det.location,det.summary, det.contact, evis.descripcion as estado
            FROM (
            jfb_jevents_vevent AS ev
            )
            LEFT JOIN jfb_jevents_repetition AS rpt ON rpt.eventid = ev.ev_id
            LEFT JOIN jfb_jevents_vevdetail AS det ON det.evdet_id = rpt.eventdetail_id
            LEFT JOIN jfb_jevents_rrule AS rr ON rr.eventid = ev.ev_id
            LEFT JOIN jfb_jevents_icsfile AS icsf ON icsf.ics_id = ev.icsid
            LEFT JOIN jfb_clientes AS cli ON det.cliente = cli.id
            LEFT JOIN jfb_estado_visita as evis ON evis.id = ev.estado
            WHERE rpt.startrepeat BETWEEN  '".$desde."' AND  '".$hasta."' + INTERVAL 1 DAY  
            ".$comerciales."
            AND ev.estado != 6
            order by inicio,created_by
            ";
      //echo $query;
    return $query;
  }

  /**
   * Method to build the orderby clause of the query
   **/
  function _buildContentOrderBy()
  {
    $mainframe = JFactory::getApplication();
    global $option;

    $filter_order    = $mainframe->getUserStateFromRequest( $option.'.componente.filter_order',   'filter_order',   'U.id', 'cmd' );
    $filter_order_Dir  = $mainframe->getUserStateFromRequest( $option.'.componente.filter_order_Dir',  'filter_order_Dir',  '', 'word' );

    //Some stupid error this fixes it
    if($filter_order == 'U.id DESC') $filter_order = 'U.id DESC';

    $orderby   = ' ORDER BY '.$filter_order.' '.$filter_order_Dir;

    return $orderby;
  }

  /** FILTROS
   * Method to build the where clause of the query
   **/
  function _buildContentWhere()
  {
    $mainframe = JFactory::getApplication();
    global $option;

//    $component = JComponentHelper::getComponent( 'com_aganar' ); $params = new JParameter( $component->params );
    $search       = $mainframe->getUserStateFromRequest( $option.'componente.search',       'search',     '', 'string' );
    $search       = $this->_db->getEscaped( trim(JString::strtolower( $search ) ) );
    $name       = $this->_db->getEscaped( trim(strtolower( strtolower(JRequest::getVar("name") ) ) ) );
 
    $where = array();
    
    $where     = ( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );

    return $where;
  }

  /**
   * Method to remove
   **/
  function delete($cids)
  {
    $cids = implode( ',', $cids );

    $query = 'DELETE FROM jfb_extracto'.
            ' WHERE id IN ('. $cids .')';

    $this->_db->setQuery( $query );
    if(!$this->_db->query()) {
      $this->setError($this->_db->getErrorMsg());
      return false;
    }

    $total   = count( $cid );
    $msg   = $total.' '.JText::_('registro eliminado');
    return $msg;
  }

  function publicado($cid, $publicado = 1, $task = 'publicado')
  {
    JArrayHelper::toInteger($cid);
    $cids = implode(',', $cid);

    $column = 'publicado';
    if($task == 'publicado')
    {
      $column = 'publicado';
    }

    $query = 'UPDATE
                jos_extracto
              SET '.$column.' = '.(int) $publicado.'
              WHERE
                id IN ('.$cids.')';

    $this->_db->setQuery($query);
    if(!$this->_db->query())
    {
      return false;
    }

    return count($cid);
  }
  
  function getLists()
  {
      $this->lists['order'] = "ordering";
      return $this->lists;
  }
  
   function getComercialesaCargo(){ //obtenemos los departamentos y los listamos para mostarlos en el select de la vista
      
      $user = JFactory::getUser();
      $sesion = JFactory::getSession();
      $db = JFactory::getDbo();
      $option = "";
      $x = "";

      /*consultamos si el usuario es un director*/
      $query = "SELECT id
                FROM #__regionales
                WHERE id_user_director =".$user->id;
     //echo $query;
      $db->setQuery($query);
      $regional = $db->loadResult();
    
      /*listando los comerciales que pertenecen a una regional*/
      if(isset($regional)){
         $query= "SELECT id_user, CONCAT( nombres,  ' ', apellidos ) as comercial 
                  FROM  jfb_participantes
                  WHERE regional = ".$regional."
                  AND id_user != ".$user->id." AND cargo IN (4,7)
                  ORDER BY comercial ASC ";  
//echo $query;
        $db->setQuery($query);
        $result = $db->loadobjectList();
         for ($i=0,$n=count($result); $i < $n; $i++) {
            if($sesion->get('comercial') == $result[$i]->id_user){
                 $x='selected';
             } else{
                 $x = "";
             }
             $option.="<option value='".$result[$i]->id_user."'".$x.">".$result[$i]->comercial."</option>";
            }
         }
      return $option;
  }
  function getComerciales(){ //obtenemos los departamentos y los listamos para mostarlos en el select de la vista
      $user = JFactory::getUser();
      $sesion = JFactory::getSession();
      $db = JFactory::getDbo();
      /*consultamos si el usuario es un director*/
      $query = "SELECT id
                FROM #__regionales
                WHERE id_user_director =".$user->id;
     //echo $query;
      $db->setQuery($query);
      $regional = $db->loadResult();
    
      /*listando los comerciales que pertenecen a una regional*/
      if(isset($regional)){
         $query= "SELECT id_user,  nombres as comercial 
                  FROM  jfb_participantes
                  WHERE regional = ".$regional."
                  AND id_user != ".$user->id."";
//echo $query;
        $db->setQuery($query);
        $result = $db->loadAssocList();
        }
         $respuesta = array();
         foreach ($result as $value) {
          $respuesta[$value['id_user']] = $value['comercial'];    
         }
      return $respuesta;
  }

}
?>
