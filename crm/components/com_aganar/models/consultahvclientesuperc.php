<?php
//error_reporting(E_ALL);
// Disallow direct access to this file
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.model');

class aganarModelConsultaHvclienteSuperc extends JModel {

    var $_pagination = null;
    var $_id = null;
    var $_data = null;

    /**
     * Constructor
     * */
    
    function __construct() {
        parent::__construct();
        $mainframe = JFactory::getApplication();
        global $option;

        $limit = $mainframe->getUserStateFromRequest($option . '.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
        $limitstart = $mainframe->getUserStateFromRequest($option . '.limitstart', 'limitstart', 0, 'int');
        $this->setState('limit', $limit);
        $this->setState('limitstart', $limitstart);

        $array = JRequest::getVar('cid', 0, '', 'array');
        $this->setId((int) $array[0]);
        //echo 'consturc';exit;
    }

    /**
     * Method to set the identifier
     * */
    function setId($id) {
        // Set id and wipe data
        $this->_id = $id;
        $this->_data = null;
    }

  
    
    
    function getTotal() {
        // Lets load the files if it doesn't already exist
        if (empty($this->_total)) {
            $query = $this->_buildQuery();
            $this->_total = $this->_getListCount($query);
        }
        return $this->_total;
    }
    /**
     * Method to build the query
     * */
    
    
    
    
    function _buildQuery() {
        
        
        $session = JFactory::getSession();
        $bandera = JRequest::getVar('bandera');
        $user = JFactory::getUser();
        
        if (isset($bandera)&& $bandera==1){
             $where = $this->_buildContentWhere();
        }elseif($session->get('where')){
            $where = $session->get('where');
        }else{ 
            $where =  ""; 
        }

      $query = "SELECT a.cliente AS id_cli,UPPER(b.nombre) AS cliente, p.id_user, CONCAT( p.nombres,  ' ', p.apellidos ) AS user,b.creado
                    FROM jfb_user_cliente AS a
                    INNER JOIN jfb_clientes AS b ON a.cliente = b.id
                    INNER JOIN jfb_participantes AS p ON a.user = p.id_user
                    ".$where."
                    ORDER BY user ASC,cliente ASC";
       //exit;
        return $query;
    }

    /**
     * Method to build the orderby clause of the query
     * */
  /** FILTROS
     * Method to build the where clause of the query
     * */
    function _buildContentWhere() {
        
        $mainframe = JFactory::getApplication();
        global $option;
        
         
        $session = JFactory::getSession();
        $comercial = JRequest::getVar('comerciales');
        $docCliente = JRequest::getVar('docCliente');
        $user_cliente = JRequest::getVar('user_clientes2');
        $desde = JRequest::getVar('desde');
        $hasta = JRequest::getVar('hasta');
        $buscar = JRequest::getVar('Buscar');
        $db = JFactory::getDbo();
        $user = JFactory::getUser();
       // $where2 = $session->get('where');
       
        //echo "<pre>".print_r($user,1);
        
        $query = "SELECT id 
                        FROM jfb_regionales 
                        WHERE id_user_director = ".$user->id;
            $db->setQuery($query);
            $id_regional = $db->loadResult();
        
        $where_resul = "";
        //echo "<br>comer<".$comercial."><br>doccli<".$docCliente."><br>desde<".$desde."><br>hasta<".$hasta."><br>user_clie<".$user_cliente."><br><br>";
        
        $where = array();

        if($comercial > 0 ){
            $session->set('comercial',$comercial);
            $where[] = " p.id_user = ".$comercial;
        }else{
            $session->clear('comercial');
        } 
        
        if($docCliente){
            $where[] = " b.documento like '%".trim($docCliente)."%'";
        }else{
            if($user_cliente){
                $where[] = "b.id = ".$user_cliente;
            }
        }
        if($desde){
            $where[] = " b.creado >= '".$desde."'";
        } 
        if($hasta){
            $where[] = " b.creado <= '".$hasta."'";
        }
        
        //$where[] = " p.regional = ".$user->id_re;
        
        $where_resul = ( count($where) ? ' WHERE ' . implode(' AND ', $where) : '' );
        $session->set('where',$where_resul);
        
        return $where_resul;
    }

      function _buildContentOrderBy() {
        $mainframe = JFactory::getApplication();
        global $option;

        $filter_order = $mainframe->getUserStateFromRequest($option . '.componente.filter_order', 'filter_order', 'U.id', 'cmd');
        $filter_order_Dir = $mainframe->getUserStateFromRequest($option . '.componente.filter_order_Dir', 'filter_order_Dir', '', 'word');

        //Some stupid error this fixes it
        if ($filter_order == 'U.id DESC')
            $filter_order = 'U.id DESC';

        $orderby = ' ORDER BY ' . $filter_order . ' ' . $filter_order_Dir;

        return $orderby;
    }

 function getData($cliente)
  {
    $mainframe = JFactory::getApplication();
    if (empty($this->_data))
    {
      $query = $this->_buildQueryhv($cliente);
      $this->_data = $this->_getList($query);
    }
    return $this->_data;
  }
    
  function _buildQueryhv($cliente)
  {

        $query = "SELECT a.*,b.id as idseg , b.descripcion AS segmento, c.id as idsec, c.descripcion AS sector,d.id as idreg, d.descripcion AS regional,e.id as idtpneg, e.descripcion AS tpnego,f.descripcion as tp_id
                FROM #__clientes AS a
                INNER JOIN #__segmentos b ON b.id = a.segmento
                INNER JOIN #__sectores c ON a.sector = c.id
                INNER JOIN #__regionales d ON a.regional = d.id
                INNER JOIN #__tiponegocio e ON a.tiponegocio = e.id
                INNER JOIN #__tipodoc f ON f.id = a.tipo_id
                WHERE a.id = ".$cliente;
         //echo $query;
    
    return $query;
  }

   function getBancos($cliente)
  { //obtenemos los clientes asociados a al usuario registrado
     
     $query= "SELECT DISTINCT a.id,a.descripcion AS banco, b.descripcion AS producto
                FROM  jfb_cli_ban_pro AS c
                INNER JOIN jfb_banco AS a ON c.banco = a.id
                INNER JOIN jfb_banco_prod AS b ON c.producto = b.id
                WHERE c.cliente =".$cliente." ";
     $this->_db->setQuery($query);
     $result = $this->_db->loadobjectList();
     return $result;
  } 
  
     function getFiduciaria($cliente)
  { //obtenemos los clientes asociados a al usuario registrado
     $query= "SELECT DISTINCT a.id,a.descripcion AS fidu, b.descripcion AS producto
                FROM  #__cli_fidu_pro AS c
                INNER JOIN #__fiduciaria AS a ON c.fiduciaria = a.id
                INNER JOIN #__fiduciaria_prod AS b ON c.producto = b.id
                WHERE c.cliente =".$cliente." ";
     $this->_db->setQuery($query);
     $result = $this->_db->loadobjectList();
     return $result;
  } 
    
  function getComisionista($cliente)
  { //obtenemos los clientes asociados a al usuario registrado
     $query= "SELECT DISTINCT a.id,a.descripcion AS comi, b.descripcion AS producto
                FROM  #__cli_comi_pro AS c
                INNER JOIN #__comisionista AS a ON c.comisionista = a.id
                INNER JOIN #__comisionista_prod AS b ON c.producto = b.id
                WHERE c.cliente =".$cliente." ";
     $this->_db->setQuery($query);
     $result = $this->_db->loadobjectList();
     return $result;
  }  
    
    function getContactos($cliente)
  { //obtenemos Contactos asociados a al cliente consultado
        $query= "SELECT id, nombre, cargo
                FROM #__cliente_contactos
                WHERE cliente =".$cliente." ";
     $this->_db->setQuery($query);
     $result = $this->_db->loadobjectList();
     return $result;
  } 
  
  
 function getSituacionact($cliente)
  { //obtenemos Situacionact asociados a al cliente consultado
   
     $query= "SELECT a.ult_visita, d.descripcion AS result_visita,
        b.descripcion as compromiso,
        c.descripcion as producto, 
        a.antiguedad,
        a.saldo_prom_mes,
        a.aport_prom_mes,
        a.n_pagosprom_mes,
        a.fid_adm_tpneg,
        a.recursos_adm,
        a.dir_asig_vic_gest,
        a.ejec_vic_gest,
        a.vence_contrato
        FROM #__cliente_situacion_actual AS a INNER JOIN #__cliente_compromiso AS b ON a.compromiso = b.id
        INNER JOIN #__cliente_produc AS c ON a.producto = c.id
        INNER JOIN #__cliente_result_visit AS d ON a.result_visita = d.id 
        WHERE a.cliente = ".$cliente." ";
     $this->_db->setQuery($query);
     $result = $this->_db->loadobjectList();
     return $result;
     
  }
  function getInformacionFinanciera($cliente)
  { //obtenemos toda la info financiera asociados a al cliente consultado
      
     $query= "SELECT *
         FROM jfb_cliente_info_finan
        WHERE cliente = ".$cliente." ";
     $this->_db->setQuery($query);
     $result = $this->_db->loadobjectList();
     return $result;
  } 
  
    function getSeguimiento($cliente)
  { //obtenemos toda la info seguimiento asociados a al cliente consultado
     
      $query= "SELECT fecha_creacion as fecha,descripcion as segui
                FROM jfb_cliente_seguimiento
                WHERE cliente = ".$cliente." ";
     $this->_db->setQuery($query);
     $result = $this->_db->loadobjectList();
     return $result;
  } 
  
     function getNoticia($cliente)
  { //obtenemos toda la info seguimiento asociados a al cliente consultado
    
      $query= "SELECT fecha_creacion as fecha,descripcion as noti
                FROM jfb_cliente_noticias
                WHERE cliente = ".$cliente." ";
     $this->_db->setQuery($query);
     $result = $this->_db->loadobjectList();
     return $result;
  } 
  
 function getInfoadicional($cliente)
  { //obtenemos toda la info seguimiento asociados a al cliente consultado
     
      $query= "SELECT * 
                FROM jfb_cliente_inf_adic
                WHERE cliente = ".$cliente." ";
     //echo $query;
     $this->_db->setQuery($query);
     $result = $this->_db->loadobjectList();
     return $result;
  } 
    /**
     * Method to remove
     * */

}

?>
