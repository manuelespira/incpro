<?php
//error_reporting(E_ALL);
/**
 * @version $Id: view.html.php 875 2012-07-01 18:01:03Z kaktuspalme $
 * @package    incsocios
 * @link http://www.kramgroup.com
 * @license    GNU/GPL
 * Administraci&oacute;n incsocios 
 *  Limitada, Colombia
 * @license    GNU/GPL
 * Project Page: http://www.kramgroup.com
 */
// no direct access
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');
//echo JURI::root().'components/com_aganar/libs/Zebra_Pagination.php';
//include_once 'components/com_aganar/libs/Zebra_Pagination.php';
//$require = $_SERVER['DOCUMENT_ROOT'];
//require_once ($require . '/modules/mod_miniextracto/helper.php');

//require_once ($require.'modules/mod_miniextracto/helper.php'); en local se usa este require  
//include_once '../../libs/Zebra_Pagination.php';
/**
 * incsocios INC Group Bogota View 
 * @package    incsocios
 */
class aganarViewSabanaLiq extends JView {

    var $_pagination = null;
    var $data = null;
    var $total = null;
    var $dataextra = null;
   
    /**
     * incsocios INC Group Bogota view display method
     * @return void
     * */
    function display($tpl = null) {

        // print_r($_POST);

        $mainframe = JFactory::getApplication();
        $session = JFactory::getSession();
        $db = JFactory::getDbo();
        $model = $this->getModel();
        $periodo = intval(JRequest::getVar('periodo'));
        $var = intval(JRequest::getVar('variable'));
        
        $layout = JRequest::getVar('layout');
       

         if ($layout == "") {
                $query = $model->_buildQuery();
                $db->setQuery($query);
                $this->data = $db->loadObjectList();

            } else if ($layout == 'xls') {
                $query = $model->_buildQuery();
                $db->setQuery($query);
                $data = $db->loadObjectList();
                if($periodo > 0 && $var > 0 ){
                   $this->data = $model->getDatosAdic($data,$periodo,$var); 
                }else{
                     $this->data = $data ;
                }
                //echo "<pre>".print_r($this->data,1); exit;
                ob_end_clean();
                header("Content-type: application/octet-stream; charset=utf-8");
                header("Content-Disposition: attachment; filename=Liquidaciones_".date("Y-m-d").".xls");
                header("Pragma: no-cache");
                header("Expires: 0");
                echo "\xEF\xBB\xBF"; // UTF-8 BOM
            }
            parent::display($tpl);

        //$this->assignRef( 'items', $items );
    }

}
?>

