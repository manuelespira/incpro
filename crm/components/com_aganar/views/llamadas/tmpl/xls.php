<?php defined('_JEXEC') or die('Restricted access');?>
          <blockquote>
            <h1><?php echo JText::_( 'TITULOPRODUCTOSREDIMIDOS' ); ?></h1>
          </blockquote>
          <table border="0" cellpadding="4" cellspacing="1" class="contetabother">
          <tr class="titnumbertab">
            <td align="center"><?php echo JText::_( 'LBL_VALOR_CANAL' ); ?></td>
            <td align="center"><?php echo JText::_( 'LBL_ANIO' ); ?></td>
            <td align="center"><?php echo JText::_( 'LBL_MES' ); ?></td>
            <td align="center"><?php echo JText::_( 'LBL_DIA' ); ?></td>
            <td align="center"><?php echo JText::_( 'LBL_ASESORIATA' ); ?></td>
            <td align="center"><?php echo JText::_( 'IATA' ); ?></td>
            <td align="center"><?php echo JText::_( 'LBL_CODIGOIATA' ); ?></td>
            <td align="center"><?php echo JText::_( 'LBL_NOMBRESOCIO' ); ?></td>
            <td align="center"><?php echo JText::_( 'LBL_IDENTIFICACION' ); ?></td>
            <td align="center"><?php echo JText::_( 'LBL_NOMBREUSUARIO' ); ?></td>
            <td align="center"><?php echo JText::_( 'LBL_CIUDADIATA' ); ?></td>
            <td align="center">Barrio</td>
            <td align="center"><?php echo JText::_( 'LBL_DIRECCION' ); ?></td>
            <td align="center"><?php echo JText::_( 'LBL_TELEFONO' ); ?></td>
            <td align="center"><?php echo JText::_( 'LBL_CELULAR' ); ?></td>
            <td align="center"><?php echo JText::_( 'LBL_CODIGOSOLICITUD' ); ?></td>
            <td align="center"><?php echo JText::_( 'LBL_ESTADOREDENCION' ); ?></td>
            <td align="center"><?php echo JText::_( 'LBL_FECHASOLICITUD' ); ?></td>
            <td align="center"><?php echo JText::_( 'LBL_FECHAVERIFICACION' ); ?></td>
            <td align="center"><?php echo JText::_( 'LBL_NOMBREPRODUCTO' ); ?></td>
            <td align="center"><?php echo JText::_( 'LBL_CANTIDAD' ); ?></td>
            <td align="center"><?php echo JText::_( 'LBL_PUNTOS' ); ?></td>
          </tr>
    <?php
    for ($i=0, $n=count( $this->items ); $i < $n; $i++) {
        $row =& $this->items[$i];
    ?>
          <tr>
            <td class="tabform"><?php echo $row->nombrecanal; ?></td>
            <td class="tabform"><?php echo $row->anio; ?></td>
            <td class="tabform"><?php echo $row->mes; ?></td>
            <td class="tabform"><?php echo $row->dia; ?></td>
            <td class="tabform"><?php echo $row->nombreasesor; ?></td>
            <td class="tabform"><?php echo $row->nombreiata; ?></td>
            <td class="tabform"><?php echo $row->codigoiata; ?></td>
            <td class="tabform"><?php echo $row->nombresocio; ?></td>
            <td class="tabform"><?php echo $row->numero_identificacion; ?></td>
            <td class="tabform"><?php echo $row->username; ?></td>
            <td class="tabform"><?php echo $row->nombreciudad; ?></td>
            <td class="tabform"><?php echo $row->nombrebarrio; ?></td>
            <td class="tabform"><?php echo $row->direccionentrega; ?></td>
            <td class="tabform"><?php echo $row->telefonofijo; ?></td>
            <td class="tabform"><?php echo $row->celular; ?></td>
            
            <td class="tabform"><?php
              $idRedencion = $row->id;
              while(strlen($idRedencion)<8){
                $idRedencion = "0".$idRedencion;
              }
              echo $idRedencion; ?></td>
            <td class="tabform"><?php
                      if($row->estado==SOLICITADA){
                        echo JText::_('REDENCION_SOLICITADA');
                      }
                      else if($row->estado==APROBADA){
                        echo JText::_('REDENCION_APROBADA');
                      }
                      else if($row->estado==RECHAZADA){
                        echo JText::_('REDENCION_RECHAZADA');
                      }
                      ?></td>

            <td class="tabform"><?php echo $row->fecha_solicitud; ?></td>
            <td class="tabform"><?php echo $row->fecha_verificacion; ?></td>
            <td class="tabform"><?php echo $row->nombreproducto; ?></td>
            <td class="tabform"><?php echo $row->cantidad_producto; ?></td>
            <td class="tabform"><?php echo $row->puntos; ?></td>
          </tr>
    <?php
    }
    ?>
          </table>
