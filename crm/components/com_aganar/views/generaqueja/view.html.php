<?php
//error_reporting(E_ALL);
/**
 * Componente IncSocios
 * 
 * @package    IncSocios
 * @subpackage IncSocios
 * @link http://dev.joomla.org/component/option,com_jd-wiki/Itemid,31/id,tutorials:components/
 * @license                GNU/GPL
 */

jimport( 'joomla.application.component.view');
require_once (JPATH_BASE.DS.'components'.DS.'com_aganar'.DS.'models'.DS.'misdatos.php');
/**
 * HTML View
 *
 * @package           IncSocios
 * @subpackage        Components
 */
class aganarViewGeneraQueja extends JView
{
        function display($tpl = null)
        {
                global $mainframe;
                $model = $this->getModel();
                $item = $model->datosParti();
                $this->assignRef( 'item', $item );
                parent::display($tpl);
                $this->setDocument();
        }
        protected function setDocument()
        {
                JFactory::getDocument()->setTitle(JText::_( 'TITULOPAGINAASESORES' ));
                JFactory::getDocument()->setDescription(JText::_( 'DESCRIPCIONPAGINAASESORES' ));
        }
}
?>
