<?php defined('_JEXEC') or die('Restricted access');
$pane =& JPane::getInstance('sliders');
$conf = new JConfig();
$url_base = $conf->live_site;
?>
<script language="javascript" type="text/javascript">
<!--
 function submitbutton(pressbutton) {
  var form = document.adminForm;
  if (pressbutton == 'cancel') {
  submitform( pressbutton );
  return;
  }

  
 // do field validation
  if (form.pdv.value == ""){
    alert( "Digite el nombre del Punto de venta" );
  }
  else if (form.identification.value == ""){
    alert( "Digite la cedula del Usuario" );
  }
  else if (form.nit.value == ""){
    alert( "Digite el nit del Usuario" );
  }
  else if (form.status.value == "0"){
    alert( "Digite el Estado del Usuario" );
  }
  else{
    submitform( pressbutton );
  }
  

}
-->
</script>
<?php JHTML::_('behavior.calendar'); ?>
<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
<div class="col100">
    <fieldset class="adminform">
        <legend>Detalles</legend>
        <table class="admintable">
        <tr>
            <td width="100" align="right" class="key">
                <label for="id">Id:            </label>
            </td>
            <td>
                <?php echo $this->user->id;?>
            </td>
        </tr>
        <tr>
            <td width="100" align="right" class="key">
                <label for="pdv">Pdv:            </label>
            </td>
            <td>
                <input class="text_area" type="text" name="pdv" id="pdv" size="32" value="<?php echo $this->user->pdv;?>" />
            </td>
        </tr>
         <tr>
            <td width="100" align="right" class="key">
                <label for="identification">Cedula:</label>
            </td>
            <td>  
                <input class="text_area" type="text" name="identification" id="identification" size="32" value="<?php echo $this->user->identification;?>" />
            </td>
        </tr>
         <tr>
            <td width="100" align="right" class="key">
                <label for="nit">Nit:</label>
            </td>
            <td>  
                <input class="text_area" type="text" name="nit" id="nit" size="32" value="<?php echo $this->user->nit;?>" />
            </td>
        </tr>
        <tr>
            <td width="100" align="right" class="key">
                <label for="name">Nombre:            </label>
            </td>
            <td>
                <input class="text_area" type="text" name="name" id="name" size="32" value="<?php echo $this->user->name;?>" />
            </td>
        </tr>
        <tr>
            <td width="100" align="right" class="key">
                <label for="address">Dirección:            </label>
            </td>
            <td>
                <input class="text_area" type="text" name="address" id="address" size="32" value="<?php echo $this->user->address;?>" />
            </td>
        </tr>
        <tr>
            <td width="100" align="right" class="key">
                <label for="city_id">Ciudad:            </label>
            </td>
            <td>
                  <?php // echo JHTML::_('brinsamania.fl_ciudad', 'city_id', $this->user->city_id );?>  
            </td>
        </tr>
        <tr>
            <td width="100" align="right" class="key">
                <label for="phone">Teléfono:            </label>
            </td>
            <td>
                <input class="text_area" type="text" name="phone" id="phone" size="32" value="<?php echo $this->user->phone;?>" />
            </td>
        </tr>
        <tr>
            <td width="100" align="right" class="key">
                <label for="cellphone">Celular:            </label>
            </td>
            <td>
                <input class="text_area" type="text" name="cellphone" id="phone" size="32" value="<?php echo $this->user->cellphone;?>" />
            </td>
        </tr>
        <tr>
            <td width="100" align="right" class="key">
                <label for="email">Email:            </label>
            </td>
            <td>
                <input class="text_area" type="text" name="email" id="phone" size="32" value="<?php echo $this->user->email;?>" />
            </td>
        </tr> 
        <tr>
            <td width="100" align="right" class="key">
                <label for="debit_card">Tarjeta Debito:            </label>
            </td>
            <td>
                <input class="text_area" type="text" name="debit_card" id="phone" size="32" value="<?php echo $this->user->debit_card;?>" />
            </td>
        </tr>
        <tr>
            <td width="100" align="right" class="key">
                <label for="gender_id">Genero:            </label>
            </td>
            <td>
                 <?php // echo JHTML::_('brinsamania.fl_genero', 'gender_id', $this->user->gender_id );?>  
            </td>
        </tr>
        <tr>
            <td width="100" align="right" class="key">
                <label for="user_category_id">Categoria:            </label>
            </td>
            <td>
               <?php // echo JHTML::_('brinsamania.fl_categoria', 'user_category_id', $this->user->user_category_id );?>  
            </td>
        </tr>
        <tr>
            <td width="100" align="right" class="key">
                <label for="merchandiser_id">Mercaderista:            </label>
            </td>
            <td>
                  <?php // echo JHTML::_('brinsamania.fl_mercaderista', 'merchandiser_id', $this->user->merchandiser_id );?>  
            </td> 
        </tr>
         <tr>
            <td width="100" align="right" class="key">
                <label for="birthdate">Fecha de cumpleaños:            </label>
            </td>
            <td>
                
                <input class="inputbox" type="text" name="birthdate" id="birthdate" size="25" maxlength="25" value="<?php echo $this->user->birthdate;?>" />
                <input type="reset" class="button" value="..." onclick="return showCalendar('birthdate','%Y-%m-%d');" />
                
            </td> 
        </tr>
        <tr>
            <td width="100" align="right" class="key">
                <label for="status">Estado:            </label>
            </td>
            <td>
                <?php // echo JHTML::_('brinsamania.fl_estado', 'status', $this->user->status );?>  
            </td> 
        </tr>
        </table>
    </fieldset>
</div>
<div class="clr"></div>
<input type="hidden" name="option" value="com_aganar" />
<input type="hidden" name="id" value="<?php echo $this->user->id; ?>" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="controller" value="user" />
<?php // echo JHTML::_( 'form.token' ); ?>

</form>
