<?php
//error_reporting(E_ALL);
/** 
 * @version $Id: view.html.php 875 2012-07-01 18:01:03Z kaktuspalme $
 * @package    incsocios
 * @link http://www.kramgroup.com
 * @license    GNU/GPL
 * Administraci&oacute;n incsocios 
 *  Limitada, Colombia
 * @license    GNU/GPL
 * Project Page: http://www.kramgroup.com
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.view' ); 
//echo JURI::root().'components/com_aganar/libs/Zebra_Pagination.php';
include_once 'components/com_aganar/libs/Zebra_Pagination.php';
//include_once '../../libs/Zebra_Pagination.php';
/**
 * incsocios INC Group Bogota View
 * @package    incsocios
 */
class aganarViewConsultaHvclienteSuperc extends JView{
 
    var $_pagination = null;
    var $data = null;
    var $total = null;
    
    /**
   * incsocios INC Group Bogota view display method
   * @return void
   **/

  function display($tpl = null)
    {
      
      
     $mainframe = JFactory::getApplication();
      $db = JFactory::getDbo();  
      $model = $this->getModel();
      $layout = JRequest::getVar('layout');
       if($layout == ""){

            $regXpag = 20;//definimos cuantos registro se veran en la vista
            $numReg = $model->getTotal();
            $this->_pagination = new Zebra_Pagination();
            $data = array(); 
            $this->_pagination->records($numReg);
            $this->total = $numReg;
            $this->_pagination->records_per_page($regXpag);
            // Quitar ceros en numeros con 1 digito en paginacion
            $this->_pagination->padding(false);
            $limitIni = (($this->_pagination->get_page() - 1) * $regXpag );
            $query = $model->_buildQuery();
            $query .= " LIMIT $limitIni ,$regXpag ";

            $db->setQuery($query);
            $this->data = $db->loadObjectList();
      }
      
     if($layout="hojavida"){
         $cliente = JRequest::getVar('cliente');
         $items = $model->getData($cliente);
         $bancos = $model->getBancos($cliente);
         $fiduciaria = $model->getFiduciaria($cliente);
         $comisionista = $model->getComisionista($cliente);
         $contactos = $model->getContactos($cliente);
         $situacionact = $model->getSituacionact($cliente);
         $infoFianciera =$model->getInformacionFinanciera($cliente);
         $Seguimiento = $model->getSeguimiento($cliente);
         $Noticia = $model->getNoticia($cliente);
         $Infoadicional = $model->getInfoadicional($cliente);
                 
                 
     }
      $this->assignRef( 'items', $items );
      $this->assignRef('bancos', $bancos);
      $this->assignRef('fidu', $fiduciaria);
      $this->assignRef('comi',$comisionista);
      $this->assignRef('contactos',$contactos);
      $this->assignRef('situacionact',$situacionact);
      $this->assignRef('infofinan',$infoFianciera);
      $this->assignRef('seguimientos',$Seguimiento);
      $this->assignRef('noticias',$Noticia);
      $this->assignRef('Infoadicional',$Infoadicional);
     
     
      parent::display($tpl);

    }

}

?>

