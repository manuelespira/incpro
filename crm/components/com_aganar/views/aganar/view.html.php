<?php
/**
 * aganar 1.0 - INC Group Bogota - Manejo de socios
 * @package aganar 1.0
 * @copyright (C) 2009 Lyften Designs
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link http://www.kramgroup.com/ Sitio oficial
 **/
 
// Disallow direct access to this file
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view');

/**
 * @package Joomla
 * @subpackage aganar
 * @since 1.0
 */
class aganarViewAganar extends JView
{
  /**
   * Creates the Entrypage
   *
   * @since 1.0
   */
  function display( $tpl = null )
  {
      $mainframe = JFactory::getApplication();
    
    //Load pane behavior
    jimport('joomla.html.pane');

    //initialise variables
    $document  = & JFactory::getDocument();
    $pane     = & JPane::getInstance('sliders');
    $template  = $mainframe->getTemplate();
    $params   = & JComponentHelper::getParams('com_aganar');
    $update   = 0;

    $this->assignRef('pane'      , $pane);
    $this->assignRef('update'    , $update);
    $this->assignRef('template'    , $template);
    JToolBarHelper::preferences( 'com_aganar', 500, 750 );

    parent::display($tpl);

  }
  
  /**
   * Creates the buttons view
   **/
  function addIcon( $image , $view, $text )
  {
    $lang    =& JFactory::getLanguage();
    $link    = 'index.php?option=com_aganar&view=' . $view; 
?>
    <div style="float:<?php echo ($lang->isRTL()) ? 'right' : 'left'; ?>;">
      <div class="icon">
        <a href="<?php echo $link; ?>">
          <?php echo JHTML::_('image', 'administrator/components/com_aganar/images/iconos/icon-48-'.$image.'.png' , NULL, NULL, $text ); ?>
          <span><?php echo $text; ?></span></a>
      </div>
    </div>
<?php
  }
  /**
   * Creates the Maintenance buttons view
   **/
  function addMaintIcon( $image , $view, $text )
  {
    $lang    =& JFactory::getLanguage();
    $link    = 'index.php?option=com_aganar&controller=aganar&task=' . $view;
?>
    <div style="float:<?php echo ($lang->isRTL()) ? 'right' : 'left'; ?>;">
      <div class="icon">
        <a href="<?php echo $link; ?>">
          <?php echo JHTML::_('image', 'administrator/components/com_aganar/assets/images/icon-48-'.$image.'.png' , NULL, NULL, $text ); ?>
          <span><?php echo $text; ?></span></a>
      </div>
    </div>
<?php
  }  
}
?>
