<?php
/**
 * @version $Id: view.html.php 875 2012-07-01 18:01:03Z kaktuspalme $
 * @package    incsocios
 * @link http://www.kramgroup.com
 * @license    GNU/GPL
 *
 * Administraci&oacute;n incsocios 
 *  Limitada, Colombia
 * @license    GNU/GPL
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.view' ); 
class aganarViewActdatos extends JView
{
  /**
   * incsocios INC Group Bogota view display method
   * @return void
   **/
  function display($tpl = null)

    {
        $mainframe = JFactory::getApplication();
        //JToolBarHelper::custom( 'cargarusuarios', 'cargarusuarios', 'cargarusuarios', 'Cargar usuarios', false, false );
        // Get data from the model
        $items =& $this->get('Data');
        $depts = $this->get('Departamento'); 
        $ciudad = $this->get('Ciudad');
        $pagination = $this->get('Pagination');
        $lists =  $this->get('Lists');
        $lists['search']= $search;

        $usuario =& $this->get( 'users');
        $this->assignRef( 'users' , $usuario);
        $this->assignRef( 'pagination' , $pagination);
        $this->assignRef( 'items', $items );
        $this->assignRef('lists', $lists);
        $this->assignRef('depts', $depts);
        $this->assignRef('ciudad', $ciudad);



        parent::display($tpl);

    }

}

?>

