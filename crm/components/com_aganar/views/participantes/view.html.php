<?php

//error_reporting(E_ALL);
/**
 * @version $Id: view.html.php 875 2012-07-01 18:01:03Z kaktuspalme $
 * @package    incsocios
 * @link http://www.kramgroup.com
 * @license    GNU/GPL
 * Administraci&oacute;n incsocios 
 *  Limitada, Colombia
 * @license    GNU/GPL
 * Project Page: http://www.kramgroup.com
 */
// no direct access
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');
//echo JURI::root().'components/com_aganar/libs/Zebra_Pagination.php';
include_once 'components/com_aganar/libs/Zebra_Pagination.php';
$require = $_SERVER['DOCUMENT_ROOT'];
require_once ($require . '/modules/mod_miniextracto/helper.php');

//require_once ($require.'modules/mod_miniextracto/helper.php'); en local se usa este require  
//include_once '../../libs/Zebra_Pagination.php';
/**
 * incsocios INC Group Bogota View 
 * @package    incsocios
 */
class aganarViewParticipantes extends JView {

    var $_pagination = null;
    var $data = null;
    var $total = null;
    var $dataextra = null;
    var $grafica = null;

    /**
     * incsocios INC Group Bogota view display method
     * @return void
     * */
    function display($tpl = null) {

        // print_r($_POST);

        $mainframe = JFactory::getApplication();
        $session = JFactory::getSession();
        $db = JFactory::getDbo();
        $model = $this->getModel();
        $layout = JRequest::getVar('layout');
        $limpiar = JRequest::getVar('limpiar');
        $accion = JRequest::getVar('accion');

        if ($accion=="" && $layout=="xls" ) {
            $mainframe->redirect("index.php?option=com_aganar&view=participantes&Itemid=1193",'Por favor grafique antes de exportar','message' );
        } else {
            if ($limpiar) {
                $session->clear('desdelog');
                $session->clear('hastalog');
                $session->clear('regionallog');
                $session->clear('segmentolog');
            }


            if ($layout == "") {

                $regXpag = 20; //definimos cuantos registro se veran en la vista
                $numReg = $model->getTotal();
                //ECHO $numReg; 
                $this->_pagination = new Zebra_Pagination();
                //$this->dataextra = new modMiniExtractoDocumentosHelper();
                //$data = array(); 
                $this->_pagination->records($numReg);
                $this->total = $numReg;
                $this->_pagination->records_per_page($regXpag);
                // Quitar ceros en numeros con 1 digito en paginacion
                $this->_pagination->padding(false);
                $limitIni = (($this->_pagination->get_page() - 1) * $regXpag );
                $query = $model->_buildQuery();
                $query .= " LIMIT $limitIni ,$regXpag ";
                $db->setQuery($query);
                $this->data = $db->loadObjectList();
                $this->grafica = $model->getGrafico();
            } else if ($layout == 'xls') {
                $query = $model->_buildQuery();
                $db->setQuery($query);
                $this->data = $db->loadObjectList();
                
                //echo "<pre>".print_r($this->data,1); exit;
                ob_end_clean();
                header("Content-type: application/octet-stream; charset=utf-8");
                header("Content-Disposition: attachment; filename=Participantes_".$accion."_".date("Y-m-d").".xls");
                header("Pragma: no-cache");
                header("Expires: 0");
                echo "\xEF\xBB\xBF"; // UTF-8 BOM
            }

            parent::display($tpl);
        } 


        //$this->assignRef( 'items', $items );
    }

}
?>

