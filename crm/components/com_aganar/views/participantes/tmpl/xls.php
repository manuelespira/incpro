<?php
$session = JFactory::getSession();

?> 
<h2>LOG DE USUARIOS</h2>
 <table class="tablaliq" border="1" cellspacing="3" cellpadding="2" >
                    <tr> 
                        <th style="text-transform: uppercase;text-align: center;background-color: #000066;color: white;">Participante</th>
                        <th style="text-transform: uppercase;text-align: center;background-color: #000066;color: white;">User</th>
                        <th style="text-transform: uppercase;text-align: center;background-color: #000066;color: white;">Regional</th>
                        <th style="text-transform: uppercase;text-align: center;background-color: #000066;color: white;">Segmento</th>
                        <?php if ($session->get('desdelog') || $session->get('hastalog') ){?>
                        <th style="text-transform: uppercase;width: 110px;text-align: center;background-color: #000066;color: white;">fecha</th>
                        <?php }?>
                        <th style="text-transform: uppercase;text-align: center;background-color: #000066;color: white;">ingresos</th>
                    </tr>
                    <?php
                    $datos = $this->data;
                    if ($datos) {
                        $i = 0;
                        foreach ($datos as $value) {
                            ?>
                            <tr style="height: 25px;" <?= (($i % 2) != 0 ? '' : 'style="background-color: #DDDDDD;"'); ?>>
                                <td><?php echo utf8_decode($value->nombres); ?></td>
                                <td><?php echo $value->user; ?></td>
                                <td><?php echo utf8_decode($value->reg); ?></td>
                                <td><?php echo $value->seg; ?></td>
                                 <?php if ($session->get('desdelog') || $session->get('hastalog') ){?>
                                <td><?php echo $value->fecha; ?></td>
                                 <?php }?>
                                <td style="text-align: center"><?php echo $value->ingresos; ?></td>
                            </tr>
                            <?php
                            $i++;
                        }
                    }
                    ?>
                </table>
<?php exit(0); ?>
