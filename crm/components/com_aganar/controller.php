<?php
//error_reporting(E_ALL);
/**
 * @version     1.0.0
 * @package     com_aganar
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Jhon Rengifo <jrengifo@grupo-inc.com> - http://
 */
// no direct access
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controller');

class AganarController extends JController {

//    function display($cachable = false, $urlparams = false){
//        parent::display();
//    }

    function __construct() {
        //Get View
        if (JRequest::getCmd('view') == '') {
            JRequest::setVar('view', 'default');
        }
        $this->item_type = 'Default';
        parent::__construct();
    }

    function savearchivo() {

       
        $model = $this->getModel('cargaarchivos');
        $mensaje = $model->getProcessFile();
        $mensaje = implode("<br>", $mensaje); 
        $this->setRedirect('index.php?option=com_aganar&view=cargaarchivos&Itemid=1130',$mensaje,'message');
    }

    function procesaRedencion() {

        $redenciones = JRequest::getVar('redenciones');
        $autorizar = JRequest::getVar("autorizar");
        $rechazar = JRequest::getVar("rechazar");
        $model = $this->getModel('autorizaredenciones');
        $resp = 0;
        $msg = "";

        if (!$redenciones) {
            $msg = "No ha seleccionado ningun item para procesar.";
        } else {
            if (isset($autorizar) && $autorizar == "Autorizar") {
                for ($i = 0; $i < count($redenciones); $i++) {
                    $resp += $model->ProcessRedencion($redenciones[$i]);
                }
                 if ($resp == 0) {
                    $msg = 'No se realizaron Actualizaciones';
                } else {
                    $msg = "Se han Autorizado " . $resp . " registros";
                }
                
            } elseif (isset($rechazar) && $rechazar == "Rechazar") {
                for ($i = 0; $i < count($redenciones); $i++) {
                    $resp += $model->RechazaRedencion($redenciones[$i]);
                }
                if ($resp == 0) {
                    $msg = 'No se realizaron Actualizaciones';
                } else {
                    $msg = "Se rechazaron " . $resp . " registros";
                }
            }
        }
        $this->setRedirect('index.php?option=com_aganar&view=autorizaredenciones', $msg, "message");   
    }
    
    function callcenter(){
       $sesion = JFactory::getSession();
       $db = JFactory::getDbo();
       $user = JFactory::getUser();
       $id = JRequest::getVar('id');
       //primero verificamos que no exista otra llamada para el participante consultado
       $sql = "SELECT ticket
                        FROM  #__callcenter_llamada
                        WHERE participante = ".$id." AND semaforo !=3";
        $db->setQuery($sql); 
        $result_existe =  $db->loadResult(); 
        if($result_existe){
            $msg= 'Ya existe una llamada abierta para este participante, verifique el número de Ticket: '.$result_existe;
              $this->setRedirect('index.php?option=com_aganar&view=llamadas&Itemid=1187',$msg,"message");
        }else{
          $cod_red = rand(1, 1000);//generador de codigo para el tiket
          if ($cod_red < 10)
            {
                    $cod_red = "000".$cod_red;
            }
            elseif ($cod_red < 100)
            {
                         $cod_red = "00".$cod_red;
            }
            elseif ($cod_red < 1000)
            {
                        $cod_red = "0".$cod_red;
            }
            else
            {
                $cod_red = "".$cod_red;
            }
        $cod_red = "".$id.$cod_red;
        //CREANDO VARIABLE DE SESIÓN 
        $sesion->set('idpart', $id);
        $sesion->set('ticket', $cod_red);
        $callcenter = $user->id;
        //se inserta el valor un registro con valor del semaforo 1 que es en proceso como estado inicial de la llamada
        $sql = "INSERT INTO jfb_callcenter_llamada(ticket,participante,semaforo,fecha,callcenter) 
                    VALUES ('".$sesion->get('ticket')."','".$sesion->get('idpart')."','1',NOW(),'$callcenter')";
        $db->setQuery($sql);
        if($db->Query()){// SI se creo la llamada direcciona al extracto y muestra el mensaje
          $msg = "Se a creado la lamada con el ticket #: ".$sesion->get('ticket');
          $this->setRedirect('index.php?option=com_aganar&view=extracto',$msg,"message");
        }else{//no creo la llamada redirecciona a la busqueda del personaje e intenta nuevamente
          $msg = "Ocurrio un error intente nuevamente";
          $sesion->clear('idpart');
          $sesion->clear('ticket');
          $this->setRedirect('index.php?option=com_aganar&view=callcenter&Itemid=1185',$msg,"error");
        }
      }
  }
  
function callcenterCerrar(){
      $sesion = JFactory::getSession();
      $model = $this->getModel('callcentercerrar');
      $msg = $model->CallCentercerrarsesion(); 
      $sesion->clear('callExtracto');
      $sesion->clear('callEditdatos');
      $sesion->clear('callCatalogo');
      $sesion->clear('ticket'); 
      $sesion->clear('idpart'); 
      $sesion->clear('where');
      $this->setRedirect('index.php?option=com_aganar&view=callcenter&Itemid=1185',$msg,"message");
  }
  
  function callcenterpendiente(){
      $sesion = JFactory::getSession();
      $model = $this->getModel('callcentercerrar');
      $msg = $model->CallCentercerrarsesion();
      $sesion->clear('callExtracto');
      $sesion->clear('callEditdatos');
      $sesion->clear('callCatalogo');
      $sesion->clear('ticket');
      $sesion->clear('idpart');
      $sesion->clear('where');
      $this->setRedirect('index.php?option=com_aganar&view=callcenter&Itemid=1185',$msg,"message");
      
  }

  function callcentercerrarpendiente(){
      $sesion = JFactory::getSession();
      $model = $this->getModel('callcentercerrar');
      $ticket = JRequest::getVar('ticket');
      $obs = JRequest::getVar('observacion');
      $msg = $model->_callcentercerrarpendiente($ticket,$obs);
      $this->setRedirect('index.php?option=com_aganar&view=llamadas&Itemid=1187',$msg,"message");
      }
      
      function reiniciapass(){
          $session = JFactory::getSession();
          $user = $session->get('idpart');
          $model = $this->getModel('misdatos'); 
          $msg = $model->reiniciarPass($user); 
          $this->setRedirect('index.php?option=com_aganar&view=misdatos',$msg,"message");
       //echo "aqui";exit;   
      }
      
     function registraqueja(){
       $telConta = JRequest::getVar('telefono');
       $tp_caso = intval(JRequest::getVar('tp_caso'));
       $areainc = intval(JRequest::getVar('areainc'));
       $comentario = strtolower(JRequest::getVar("comentario"));
       $model = &$this->getModel('generaqueja');
       $respuesta = $model->registarQueja($telConta,$tp_caso,$areainc,$comentario);
       if($respuesta === TRUE){
           $this->setRedirect('index.php?option=com_aganar&view=extracto','La queja se ha registrado con éxito',"message");
       }ELSE{
           $this->setRedirect('index.php?option=com_aganar&view=generaqueja','A ocurrido un problema. Intente nuevamente!!!',"message");
       }
   }
 
  }
