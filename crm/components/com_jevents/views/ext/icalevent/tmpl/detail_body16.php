<?php 
defined('_JEXEC') or die('Restricted access');

$cfg	= & JEVConfig::getInstance();
$sesion = JFactory::getSession();
$link = $_SERVER['REQUEST_URI'];
$sesion->set('regresoHVcliente',$link);
//$action = JFactory::getApplication()->isAdmin()?"index.php":JURI::root()."index.php?option=".JEV_COM_COMPONENT."&Itemid=".JEVHelper::getItemid();
if( 0 == $this->evid) {
	$Itemid = JRequest::getInt("Itemid");
	JFactory::getApplication()->redirect( JRoute::_('index.php?option=' . JEV_COM_COMPONENT. "&task=day.listevents&year=$this->year&month=$this->month&day=$this->day&Itemid=$Itemid",false));
	return;
}

if (is_null($this->data)){
  //  echo "<pre>".print_r($this,1);
  //  echo "aqui";exit;
	JFactory::getApplication()->redirect(JRoute::_("index.php?option=".JEV_COM_COMPONENT."&Itemid=$this->Itemid",false), JText::_("JEV_SORRY_UPDATED"));
}

if( array_key_exists('row',$this->data) ){
   
	$row=$this->data['row'];
        
	// Dynamic Page Title
	JFactory::getDocument()->SetTitle( $row->title() );

	$mask = $this->data['mask'];
	$page = 0;

	
	$cfg	 = & JEVConfig::getInstance();	

	$dispatcher	=& JDispatcher::getInstance();
	$params =new JParameter(null);

	if (isset($row)) {
        $customresults = $dispatcher->trigger( 'onDisplayCustomFields', array( &$row) );
		if (!$this->loadedFromTemplate('icalevent.detail_body', $row, $mask)){
            ?>
            <!-- <div name="events">  -->
            <table class="contentpaneopen" border="1">
                <tr class="headingrow">
                    <td  width="100%" class="contentheading"><h2 class="contentheading"><?php echo $row->title(); ?></h2></td>
	                <?php
	                $jevparams = JComponentHelper::getParams(JEV_COM_COMPONENT);
	                if ($jevparams->get("showicalicon",0) &&  !$jevparams->get("disableicalexport",0) ){
	                ?>
	                <td  width="20" class="buttonheading" align="right">
						<?php
						JEVHelper::script( 'view_detail.js', 'components/'.JEV_COM_COMPONENT."/assets/js/" );
						?>
					<a href="javascript:void(0)" onclick='clickIcalButton()' title="<?php echo JText::_('JEV_SAVEICAL');?>">
							<img src="<?php echo JURI::root().'administrator/components/'.JEV_COM_COMPONENT.'/assets/images/jevents_event_sml.png'?>" align="middle" name="image"  alt="<?php echo JText::_('JEV_SAVEICAL');?>" style="height:24px;"/>
						</a>
					</td>
					<?php
	                }

					if( $row->canUserEdit() && !( $mask & MASK_POPUP )) {
							JEVHelper::script( 'view_detail.js', 'components/'.JEV_COM_COMPONENT."/assets/js/" );
							//Esta linea representa el script de edicion de las citas
                        	?>
                            <td  width="20" class="buttonheading" align="right">
                                                            
                                  <?php if($row->_estado == 1){?>  
                                <a href="javascript:void(0)" onclick='clickEditButton()' title="<?php echo JText::_('JEV_E_EDIT');?>">
                            	<?php echo JEVHelper::imagesite( 'edit.png',JText::_('JEV_E_EDIT'));?>
                                </a>
                                  <?php }?>
                            </td>
                            <?php
					}
						?>
                </tr>
                <tr class="dialogs">
                    <td align="left" valign="top" colspan="3">
                    <div style="position:relative;">
                    <?php
						$this->eventIcalDialog($row, $mask);
                    ?>
                    </div>
                    </td>
                    <td align="left" valign="top">
                    <div style="position:relative;">
                    <?php
                    $this->eventManagementDialog($row, $mask);
                    ?>
                    </div>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="4">
                        <table width="100%" border="1">
                            <tr >
	                            <?php
	                            $hastd = false;
	                            if( $cfg->get('com_repeatview') == '1' ){ 
	                                echo '<td style="width: 336px" >';
	                                echo $row->repeatSummary();
	                                echo $row->previousnextLinks();
	                                echo "</td>";
	                                $hastd = true;
	                            } 
								
	                            if( $cfg->get('com_byview') == '1' ){
	                                echo '<td class="ev_detail contact" >';
									echo "Autor" . '&nbsp;: ' . $row->contactlink();
	                                echo "</td>";
	                                $hastd = true;
	                            } 
	                            if( $cfg->get('com_hitsview') == '1' ){
	                            	echo '<td class="ev_detail hits" >';
	                                echo JText::_('JEV_EVENT_HITS') . ' : ' . $row->hits();
	                                echo "</td>";
	                                $hastd = true;
	                            } 
	                            if (!$hastd){
	                            	echo "<td/>";
	                            }
	                            ?>
                                
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr align="left" valign="top">
                    <td>
                        <table width="100%" border="1">
                            <tr>
                                <td><?php echo "<b>".JText::_('JEV_EVENT_CLIENTE')." : </b>". $row->nombrecliente(); ?></td>
                                <td>Estado Visita : &nbsp; <strong style="background: #666666;color: white;"><?php echo $row->stdo();?></strong> </td>
                                <td>
                                    <?php
                                    //echo $row->_estado;
                                    //echo "<pre>".print_r($row,1);
                                    //este fragmento de código actualiza el estado de la visita
                                    if($row->_estado == 1 || $row->_estado == 2){
                                        $newEstVisi = JRequest::getVar('estadoVisita');
                                        if(isset($newEstVisi) && $newEstVisi != -1){
                                            if( $row->_estado == 1 && $newEstVisi == 5 ){
                                                echo '<script>alert ("No puede cambiar el estado a visitada, hasta ser aprobada por el Director Regional.")</script>'; 
                                            }else{
                                                $db = JFactory::getDbo();
                                                $user = JFactory::getUser();
                                                $sql = 'UPDATE jfb_jevents_vevent SET estado = '.$newEstVisi.",fecha_ult_act = NOW(),cancel_visit = '".$user->id.",".$newEstVisi."'  WHERE ev_id = ".$row->_ev_id;
                                                $db->setQuery($sql);
                                                $db->query();
                                                $sesion->clear('regresoHVcliente'); 
                                                JFactory::getApplication()->redirect( JRoute::_($link,false));
                                            }
                                    }
                                    ?>
                                    <form name="Form1" action="" method="POST">
                                        <strong>Cambiar estado:</strong>
                                        <select name="estadoVisita" onchange="submit()">
                                            <option value='-1'>Seleccione...</option> 
                                              <option value='4'>Cancelada</option>
                                              <option value='5'>Visitada</option>
                                          </select>
                                    </form>
                                    <?php }?>
                                </td>
                            </tr> 
                        </table> 
                    </td>
                  </tr>
                  <tr>  
                    <td colspan="">
                        <?php echo '<b>Actividad:</b>'.$row->content(); ?> 
                    </td>
                </tr>
                <?php
                if ($row->hasLocation() || $row->hasContactInfo()) { ?>
                    <tr>
                        <td class="ev_detail" align="left" valign="top" colspan="4">
                            <?php
                            if( $row->hasLocation() ){
                            	echo "<b>".JText::_('JEV_EVENT_ADRESSE')." : </b>". $row->location();
                            }

                            if( $row->hasContactInfo()){
                            	if(  $row->hasLocation()){
                            		echo "<br/>";
                            	}
                            	echo "<b>".JText::_('JEV_EVENT_CONTACT')." : </b>". $row->contact_info();
                            } ?>
                        </td>
                    </tr>
                    <?php
                }

                if( $row->hasExtraInfo()){ ?>
                    <tr>
                        <td class="ev_detail" align="left" valign="top" colspan="4" style="color: #0000FF"><?php echo JText::_('JEV_EVENT_EXTRA'); ?> </td>
                    </tr>
                    <tr>
                        <td><?php echo $row->extra_info(); ?></td>
                    </tr>
                    <?php
                } ?>
	            <?php
	            if (count($customresults)>0){
	            	foreach ($customresults as $result) {
	            		if (is_string($result) && strlen($result)>0){
	            			echo "<tr><td>".$result."</td></tr>";
	            		}	            		
	            	}
	            }
				?>
                
            </table>
            <!--  </div>  -->
            <?php
		} // end if not loaded from template
            $results = $dispatcher->trigger( 'onAfterDisplayContent', array( &$row, &$params, $page ) );
            echo trim( implode( "\n", $results ) );

        } else { ?>
            <table width="100%" border="1" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="contentheading"  align="left" valign="top"><?php echo JText::_('JEV_REP_NOEVENTSELECTED'); ?></td>
                </tr>
            </table>
            <?php
        }

		if(!($mask & MASK_BACKTOLIST)) { 
                   ?>
            <?php if( $row->_estado == 1 || $row->_estado == 2){ ?>
                <p align="center">
                    <a href="index.php?option=com_aganar&view=hojavidacliente&bandera=100&clientebuscado=<?php echo $row->idcliente();?>" title="Redirección a la HV del cliente">*****Hoja de vida del cliente*****</a>
    		</p>
                <?php }?>
            
<!--    		<p align="center">
    			<a href="javascript:window.history.go(-1);" title="<?php echo JText::_('JEV_BACK'); ?>"><?php echo JText::_('JEV_BACK'); ?></a>
    		</p>
               -->
    		<?php  
		}

}
